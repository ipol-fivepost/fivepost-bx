<?
$MESS['IPOL_FIVEPOST_ADMIN_SYNC_DATA_TITLE']              = 'Загрузка и синхронизация внешних данных';

$MESS['IPOL_FIVEPOST_SYNC_START_TITLE']                   = 'Внимание!';
$MESS['IPOL_FIVEPOST_SYNC_START_DESCR']                   = 'Для правильной работы модуля необходимо произвести загрузку и синхронизацию внешних данных по точкам самовывоза, тарифам и городам.<br><br>Рекомендуется запускать синхронизацию в период минимальной активности на сайте.';
$MESS['IPOL_FIVEPOST_SYNC_START_BTN']                     = 'Запустить синхронизацию';

$MESS['IPOL_FIVEPOST_SYNC_ERRORS']                        = 'Ошибки при синхронизации';
$MESS['IPOL_FIVEPOST_SYNC_CONTINUE_AFTER_ERRORS']         = '<hr>В процессе синхронизации <b>возможны</b> ошибки, как правило из-за отсутствия данных по обязательным полям у некоторых точек, загруженных с сервера 5Post. <br>В общем случае это <b>не является значимой проблемой</b> (просто в базу модуля не будет добавлена соответствующая точка), загрузку данных можно выполнять дальше. <br><br>Для продолжения синхронизации нажмите<br><br><input type="submit" name="Apply" value="Продолжить" class="adm-btn-save">';
$MESS['IPOL_FIVEPOST_SYNC_CONTINUE']                      = 'Страница обновится автоматически. Если этого не произошло, нажмите<br><br><input type="submit" name="Apply" value="Продолжить" class="adm-btn-save">';

$MESS['IPOL_FIVEPOST_SYNC_REFRESH_DATA_TITLE']            = 'Шаг 1 из 3. Загрузка данных о точках самовывоза и тарифах';
$MESS['IPOL_FIVEPOST_SYNC_TOGGLE_INACTIVE_POINTS_TITLE']  = 'Шаг 2 из 3. Деактивация не работающих на данный момент точек самовывоза';
$MESS['IPOL_FIVEPOST_SYNC_REFRESH_LOCATIONS_TITLE']       = 'Шаг 3 из 3. Загрузка данных о городах';
$MESS['IPOL_FIVEPOST_SYNC_FINISH_TITLE']                  = 'Синхронизация завершена';
$MESS['IPOL_FIVEPOST_SYNC_FINISH_DESCR']                  = "<br>Если вы настраиваете модуль, чтобы вернуться на его страницу настроек, <a href='/bitrix/admin/settings.php?lang=ru&mid=ipol.fivepost&mid_menu=1' target='_blank'>перейдите по ссылке</a>";
