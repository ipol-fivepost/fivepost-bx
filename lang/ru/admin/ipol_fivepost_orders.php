<?
$MESS['IPOL_FIVEPOST_ADMIN_ORDERS_TITLE'] = 'Заявки';

// Buttons
$MESS['IPOL_FIVEPOST_TABLE_ORDERS_BTN_GET_STATUSES'] = 'Проверить статусы';

// Grid and filter
$MESS['IPOL_FIVEPOST_TABLE_ORDERS_ID']                        = 'ID';
$MESS['IPOL_FIVEPOST_TABLE_ORDERS_BITRIX_ID']                 = 'Bitrix ID';
$MESS['IPOL_FIVEPOST_TABLE_ORDERS_FIVEPOST_ID']               = '5Post ID';
$MESS['IPOL_FIVEPOST_TABLE_ORDERS_STATUS']                    = 'Статус';
$MESS['IPOL_FIVEPOST_TABLE_ORDERS_FIVEPOST_STATUS']           = 'Статус заказа';
$MESS['IPOL_FIVEPOST_TABLE_ORDERS_FIVEPOST_EXECUTION_STATUS'] = 'Статус исполнения заказа';
$MESS['IPOL_FIVEPOST_TABLE_ORDERS_BRAND_NAME']                = 'Бренд отправителя';
$MESS['IPOL_FIVEPOST_TABLE_ORDERS_CLIENT_NAME']               = 'ФИО получателя';
$MESS['IPOL_FIVEPOST_TABLE_ORDERS_CLIENT_EMAIL']              = 'Email получателя';
$MESS['IPOL_FIVEPOST_TABLE_ORDERS_CLIENT_PHONE']              = 'Телефон получателя';
$MESS['IPOL_FIVEPOST_TABLE_ORDERS_PLANNED_RECEIVE_DATE']      = 'Плановая дата передачи заказа';
$MESS['IPOL_FIVEPOST_TABLE_ORDERS_SHIPMENT_DATE']             = 'Плановая дата отгрузки заказа';
$MESS['IPOL_FIVEPOST_TABLE_ORDERS_RECEIVER_LOCATION']         = 'Точка выдачи';
$MESS['IPOL_FIVEPOST_TABLE_ORDERS_SENDER_LOCATION']           = 'Номер склада партнера';
$MESS['IPOL_FIVEPOST_TABLE_ORDERS_UNDELIVERABLE_OPTION']      = 'Способ обработки невостребованных заказов';
$MESS['IPOL_FIVEPOST_TABLE_ORDERS_CARGOES']                   = 'Грузы';
$MESS['IPOL_FIVEPOST_TABLE_ORDERS_CURRENCY']                  = 'Код валюты';
$MESS['IPOL_FIVEPOST_TABLE_ORDERS_DELIVERY_COST']             = 'Стоимость доставки с НДС';
$MESS['IPOL_FIVEPOST_TABLE_ORDERS_DELIVERY_COST_CURRENCY']    = 'Код валюты стоимости доставки';
$MESS['IPOL_FIVEPOST_TABLE_ORDERS_PAYMENT_VALUE']             = 'Сумма к оплате';
$MESS['IPOL_FIVEPOST_TABLE_ORDERS_PAYMENT_TYPE']              = 'Способ оплаты';
$MESS['IPOL_FIVEPOST_TABLE_ORDERS_PAYMENT_CURRENCY']          = 'Код валюты суммы к оплате';
$MESS['IPOL_FIVEPOST_TABLE_ORDERS_PRICE']                     = 'Оценочная стоимость заказа с НДС';
$MESS['IPOL_FIVEPOST_TABLE_ORDERS_PRICE_CURRENCY']            = 'Код валюты оценочной стоимости';
$MESS['IPOL_FIVEPOST_TABLE_ORDERS_MESSAGE']                   = 'Комментарий';
$MESS['IPOL_FIVEPOST_TABLE_ORDERS_OK']                        = 'Флаг успешной отправки';
$MESS['IPOL_FIVEPOST_TABLE_ORDERS_UPTIME']                    = 'Дата изменения';

// Grid row actions
$MESS['IPOL_FIVEPOST_TABLE_ORDERS_ROW_VIEW_BITRIX_ORDER']     = 'К заказу';
$MESS['IPOL_FIVEPOST_TABLE_ORDERS_ROW_GET_ORDER_STATUS']      = 'Проверить статус';
$MESS['IPOL_FIVEPOST_TABLE_ORDERS_ROW_PRINT_STICKER']         = 'Печать наклейки';
$MESS['IPOL_FIVEPOST_TABLE_ORDERS_ROW_DELETE_ORDER']          = 'Отозвать заявку';