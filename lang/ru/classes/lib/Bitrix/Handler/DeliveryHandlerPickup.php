<?
$MESS ['IPOL_FIVEPOST_DELIVERY_PROFILE_PICKUP_NAME'] = "Самовывоз";
$MESS ['IPOL_FIVEPOST_DELIVERY_PROFILE_PICKUP_DESCRIPTION'] = "Доставка до точки самовывоза в магазин сети \"Пятерочка\".";

$MESS ['IPOL_FIVEPOST_DELIVERY_HANDLER_PICKUP_MAIN_TAB_TITLE'] = "Дополнительные настройки";
$MESS ['IPOL_FIVEPOST_DELIVERY_HANDLER_PICKUP_MAIN_TAB_DESCR'] = "Дополнительные настройки профиля службы доставки";

$MESS ['IPOL_FIVEPOST_DELIVERY_HANDLER_PICKUP_MAIN_TAB_RATE_TYPE'] = "Тип тарифного плана, которым будет производиться расчет стоимости доставки";
$MESS ['IPOL_FIVEPOST_DELIVERY_HANDLER_PICKUP_MAIN_TAB_RATE_TYPE_MIN_PRICE'] = "Автовыбор (минимальная стоимость)";

$MESS ['IPOL_FIVEPOST_DELIVERY_CALC_ERROR_NO_AUTH'] = "Не удалось рассчитать доставку: не выполнена авторизация в настройках модуля";
$MESS ['IPOL_FIVEPOST_DELIVERY_CALC_ERROR_NO_PROPS'] = "Не удалось рассчитать доставку: нет объекта свойств заказа";
$MESS ['IPOL_FIVEPOST_DELIVERY_CALC_ERROR_NO_LOCATION_PROP'] = "Не удалось рассчитать доставку: нет свойства заказа с типом местоположение (LOCATION)";
$MESS ['IPOL_FIVEPOST_DELIVERY_CALC_ERROR_NO_LOCATION_CODE'] = "Не удалось рассчитать доставку: не задан код местоположения для расчета доставки";