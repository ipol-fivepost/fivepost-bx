<?
$MESS ['IPOL_FIVEPOST_ERR_UNADLEUPDATE']  = "Ошибка обновления информации в базе данных";
$MESS ['IPOL_FIVEPOST_LBL_STATUS']  = "статус модуля";
$MESS ['IPOL_FIVEPOST_LBL_FIVEPOST_STATUS']  = "статус Fivepost";
$MESS ['IPOL_FIVEPOST_LBL_FIVEPOST_EXECUTION_STATUS']  = "статус исполнения заказа Fivepost";
$MESS ['IPOL_FIVEPOST_LBL_MESSAGE']  = "комментарий";

$MESS ['IPOL_FIVEPOST_ERR_NOUPDATE']  = "Ошибка обновления статуса заказа в Битриксе";
$MESS ['IPOL_FIVEPOST_LBL_bitrixStatus']  = "Ожидаемый статус";

$MESS ['IPOL_FIVEPOST_LBL_ORDER']  = "Заказ";
$MESS ['IPOL_FIVEPOST_ERR_NO_STATUS_INFO']  = "Ошибка получения информации о статусах";
