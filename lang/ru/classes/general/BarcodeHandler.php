<?
$MESS ['IPOL_FIVEPOST_BK_orderId']   = 'Номер заказа';
$MESS ['IPOL_FIVEPOST_BK_pointId']   = 'Пункт выдачи №';
$MESS ['IPOL_FIVEPOST_BK_pointAddr'] = 'Адрес пункта';
$MESS ['IPOL_FIVEPOST_BK_seller']    = 'Продавец';
$MESS ['IPOL_FIVEPOST_BK_company']   = 'Перевозчик';
$MESS ['IPOL_FIVEPOST_BK_receiver']  = 'Получатель';
$MESS ['IPOL_FIVEPOST_BK_phone']     = 'Телефон';
$MESS ['IPOL_FIVEPOST_BK_hotline']   = 'Горячая линия';