<?
$MESS ['IPOL_FIVEPOST_INSTALL_NAME'] = "Интеграция с 5Post";
$MESS ['IPOL_FIVEPOST_INSTALL_DESCRIPTION'] = "Модуль \"Интеграция с 5Post\" - автоматизированные службы доставки и выгрузка заявок на доставку в 5Post.";

$MESS ['IPOL_FIVEPOST_INSTALL'] = "Установка модуля \"Интеграция с 5Post\".";
$MESS ['IPOL_FIVEPOST_DEL']     = "Удаление модуля \"Интеграция с 5Post\"...";

$MESS ['IPOL_FIVEPOST_INSTALL_TEXT'] = "<span style='color:green'>Модуль \"Интеграция с 5Post\" установлен.</span></br>
Подробную информацию о подключении и работе модуля можно найти на <a href='/bitrix/admin/settings.php?lang=ru&mid=ipol.fivepost&mid_menu=1' target='_blank'>странице настроек модуля</a>.<br><br>
<span style='color:red'>Внимательно ознакомьтесь с разделом FAQ!</span> В особенности - с пунктом Включение функционала. Без авторизации и синхронизации модуль работать не будет!<br><br>
Модуль разработан компанией, НЕ являющейся представителем 5Post, мы не рассматриваем вопросы, касающиеся работы сервиса 5Post.<br><br>
Если Вы являетесь компанией, настраивающей модуль 5Post на сайте клиентов - не забудьте рассказать им про документацию и прочие пункты, освещенные на этой странице.";

$MESS ['IPOL_FIVEPOST_DEL_TEXT'] = "<span style='color:red'>Модуль \"Интеграция с 5Post\" удален.</span><br>";

$MESS ['IPOL_FIVEPOST_BADSALEVERSION'] = "Модуль предназначен для работы с Интернет-магазином версии 16 и выше. Версия модуля \"Интернет-магазин\" сайта: ";
$MESS ['IPOL_FIVEPOST_NOCONVERTATION'] = "На данном сайте не была проведена Конвертация Интернет-магазина. Модуль предназначен для работы только с конвертированными Интернет-магазинами.";
$MESS ['IPOL_FIVEPOST_NOCURL']         = "Для работы модуля необходима php-библиотека для работы через (<a href='https://www.php.net/manual/ru/curl.installation.php' target='_blank'>cUrl</a>).";
$MESS ['IPOL_FIVEPOST_INSTALL_ERROR_TITLE']  = "Ошибка установки модуля \"Интеграция с 5Post\".";

$MESS ['IPOL_FIVEPOST_PRESERVE_TABLES']  = "Вы можете сохранить информацию по отосланным заявкам, сохранив таблицы модуля.";

?>