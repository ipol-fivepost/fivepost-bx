<?
// COMMON
$MESS ['IPOL_FIVEPOST_RUSSIA']   = "Россия";
$MESS ['IPOL_FIVEPOST_RUBSHORT'] = "руб.";
$MESS ['IPOL_FIVEPOST_SHTUK']    = "шт.";

$MESS ['IPOL_FIVEPOST_DAY']      = "дн.";
$MESS ['IPOL_FIVEPOST_DELIV_DAYS'] = "дней";
$MESS ['IPOL_FIVEPOST_DELIV_DAY']  = "день";
$MESS ['IPOL_FIVEPOST_DELIV_DAYA'] = "дня";

$MESS ['IPOL_FIVEPOST_SIGN_YO']  = 'ё';
$MESS ['IPOL_FIVEPOST_SIGN_YE']  = 'е';

$MESS ['IPOL_FIVEPOST_SIGN_CHOOSEPOINTDEF']  = 'Выберите пункт выдачи';

// For ProfileHandler
$MESS['IPOL_FIVEPOST_DELIVERY_METHOD_PICKUP'] = "pickup";

// 5post-specific enumerations
$MESS['IPOL_FIVEPOST_VARIANT_UNDELIVERABLEOPTION_RETURN']      = "Возврат на склад партнера";
$MESS['IPOL_FIVEPOST_VARIANT_UNDELIVERABLEOPTION_UTILIZATION'] = "Утилизация";

$MESS['IPOL_FIVEPOST_VARIANT_PAYMENTTYPE_CASH']       = "Оплата наличными";
$MESS['IPOL_FIVEPOST_VARIANT_PAYMENTTYPE_CASHLESS']   = "Оплата картой";
$MESS['IPOL_FIVEPOST_VARIANT_PAYMENTTYPE_PREPAYMENT'] = "Предоплата";

// прочее
$MESS ['IPOL_FIVEPOST_LBL_CLEAR']        = "Очистить";

$MESS ['IPOL_FIVEPOST_MESS_DOCANCEL']  = "Отменить заказ? Отмена заказа не означает его удаление из системы.";
$MESS ['IPOL_FIVEPOST_MESS_NOTCANCELED']  = "Ошибка отмены заказа.";
$MESS ['IPOL_FIVEPOST_MESS_CANCELED']  = "Заказ отменен.";
$MESS ['IPOL_FIVEPOST_TABLE_ORDER_PRINT']  = "Печать наклеек";


$MESS ['IPOL_FIVEPOST_STATUS_new']         = "Заказ еще не отправлялся";
$MESS ['IPOL_FIVEPOST_STATUS_ok']          = "Заказ отправлен, ожидает подтверждения";
$MESS ['IPOL_FIVEPOST_STATUS_sended']      = "Заказ отправлена";
$MESS ['IPOL_FIVEPOST_STATUS_valid']       = "Заявка одобрена";
$MESS ['IPOL_FIVEPOST_STATUS_rejected']    = "Ошибка в обработке заказа (отклонен)";
$MESS ['IPOL_FIVEPOST_STATUS_warehouse']   = "Заказ на складе 5Post";
$MESS ['IPOL_FIVEPOST_STATUS_inpostamat']  = "Заказ в ячейке постамата";
$MESS ['IPOL_FIVEPOST_STATUS_interrupted'] = "Ошибка в обработке заказа (исполнение прервано)";
$MESS ['IPOL_FIVEPOST_STATUS_lost']        = "Посылка утеряна";
$MESS ['IPOL_FIVEPOST_STATUS_reclaim']     = "Заказ в ячейке, срок истек, получение возможно";
$MESS ['IPOL_FIVEPOST_STATUS_repickup']    = "Заказ готов к повторной выдаче";
$MESS ['IPOL_FIVEPOST_STATUS_unclaimed']   = "Заказ не был востребован";
$MESS ['IPOL_FIVEPOST_STATUS_canceled']    = "Заказ отменен";
$MESS ['IPOL_FIVEPOST_STATUS_done']        = "Заказ получен покупателем";

