<?
namespace Ipol\Fivepost;

use \Ipol\Fivepost\Bitrix\Tools;

IncludeModuleLangFile(__FILE__);

/**
 * Class ProfileHandler
 * @package Ipol\Fivepost
 */
class ProfileHandler extends AbstractGeneral
{
    /**
     * Get delivery profiles common data
     *
     * @return array
     */
    public static function getProfiles()
    {
        $arProfiles = array(
            'pickup' => array(
                'DeliveryMethod' => Tools::getMessage('DELIVERY_METHOD_PICKUP'),
                'Class'          => '\Ipol\Fivepost\Bitrix\Handler\DeliveryHandlerPickup',
                //'NAME'           => Tools::getMessage('PROFILE_PICKUP_TITLE'),
                //'DESCRIPTION'    => Tools::getMessage('PROFILE_PICKUP_DESCRIPTION')
            ),
        );

        return $arProfiles;
    }

    /**
     * Get class names for module delivery handler profiles
     *
     * @return array
     */
    public static function getProfileClasses()
    {
        $result = array();
        foreach (self::getProfiles() as $code => $data)
        {
            $result[$code] = $data['Class'];
        }
        return $result;
    }

    /**
     * Get delivery profiles data for bitrix delivery service
     *
     * @return array
     */
    public static function profilesToBitrix()
    {
        /*$_arProfiles = self::getProfiles();

        $arProfiles = array();

        foreach ($_arProfiles as $profileId => $arProfile){
            $arProfiles[$profileId] = array(
                "TITLE"       => $arProfile['TITLE'],
                "DESCRIPTION" => $arProfile['DESCRIPTION'],

                "RESTRICTIONS_WEIGHT" => array(0),
                "RESTRICTIONS_SUM"    => array(0)
            );
        }

        return $arProfiles;
		*/
    }

    /**
     * Get delivery profiles data for calculator class
     *
     * @return array of profileCode => profileLabel
     */
    public static function profilesToCalculator()
    {
        $_arProfiles = self::getProfiles();
        $arProfiles  = array();

        foreach ($_arProfiles as $profileId => $arProfile)
        {
            $arProfiles [$profileId] = $arProfile['DeliveryMethod'];
        }

        return $arProfiles;
    }
}