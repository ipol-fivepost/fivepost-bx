<?php
namespace Ipol\Fivepost;

IncludeModuleLangFile(__FILE__);

use Ipol\Fivepost\Bitrix\Handler\Order;
use Ipol\Fivepost\Bitrix\Tools;
use Ipol\Fivepost\Fivepost\Handler\BarcodeGeneratorCode128;
use Ipol\Fivepost\Fivepost\Handler\BarcodeGeneratorEAN;

class BarcodeHandler extends AbstractGeneral
{
    public static function printBKsRequestById(){
        $dbOrders = OrdersTable::getList(array('filter'=>array('ID'=>$_REQUEST['ids'])));
        $arOrders = array();

        while($arOrder = $dbOrders->Fetch()){
            if($arOrder['FIVEPOST_ID']) {
                $arOrders [] = $arOrder['BITRIX_ID'];
            }
        }

        if(!empty($arOrders)){
            self::printBKs($arOrders);
        }
    }

    public static function printBKsRequest(){
        $arr = $_REQUEST['bitrixId'];
        if(is_string($arr) && strpos($arr,',')){
            $arr = explode(',',$_REQUEST['bitrixId']);
        }
        self::printBKs($arr);
    }

    public static function printBKs($orders,$template=''){
        if(!is_array($orders) || !array_key_exists('orders',$orders))
            $arBKs = self::getBK($orders);
        else
            $arBKs = $orders;

        if(!$template)
            $template = $_SERVER['DOCUMENT_ROOT'].Tools::getToolsPath().'bkTemplate.php';

        if(!empty($arBKs['orders']))
            include $template;
    }

    public static function getBK($arOrders){
        if(!is_array($arOrders)){
            $arOrders = array($arOrders);
        }
        $arResult = array(
            'shopName'    => Option::get('barkCompany'),
            'path'        => Tools::getJSPath().'ajax.php',
            'actionName'  => self::$MODULE_LBL.'action',
            'orders'      => array(),
            'logo5post'   => Tools::getImagePath().'5postlogo.png',
            'logoCompany' => false
        );
        if(Option::get('barkLogo')){
            $arResult['logoCompany'] = \CFile::GetPath(Option::get('barkLogo'));
        }

        foreach ($arOrders as $bitrixId) {
            $arOrder = OrdersTable::getByBitrixId($bitrixId);
            if($arOrder && !empty($arOrder)) {
                $arOrder['NUMBER'] = Order::getOrderNumber($bitrixId);
                $orderPVZ = PointsTable::getByPointGuid($arOrder['RECEIVER_LOCATION']);
                if($orderPVZ){
                    $arOrder['POINT_NAME']    = $orderPVZ['NAME'];
                    $arOrder['POINT_ADDRESS'] = $orderPVZ['FULL_ADDRESS'];
                }
                $arResult ['orders'][] = $arOrder;
            }
        }

        foreach (array('orderId','pointId','pointAddr','seller','company','receiver','phone','hotline') as $lang){
            $arResult['lang'][$lang] = Tools::getMessage('BK_'.$lang);
        }

        return $arResult;
    }

    public static function getBarcode($request=false){ // получает картинку штрихкода
        if(empty($request)){
            $request = $_REQUEST;
        }
        if(array_key_exists('oldformat',$_REQUEST) && $_REQUEST['oldformat']){
            new BarcodeGeneratorEAN($request['barcode']);
        } else {
            new BarcodeGeneratorCode128($request['barcode']);
        }
    }
}