<?
namespace Ipol\Fivepost;


use Ipol\Fivepost\Bitrix\Adapter\Cargo;
use Ipol\Fivepost\Bitrix\Adapter\Order;
use Ipol\Fivepost\Bitrix\Entity\BasicResponse;
use Ipol\Fivepost\Bitrix\Entity\DefaultGabarites;
use Ipol\Fivepost\Bitrix\Entity\Options;
use Ipol\Fivepost\Bitrix\Handler\GoodsPicker;
use Ipol\Fivepost\Bitrix\Tools;
use Ipol\Fivepost\Fivepost\FivepostApplication;

IncludeModuleLangFile(__FILE__);

class OrderHandler extends AbstractGeneral
{
    /**
     * @var \Ipol\Fivepost\Bitrix\Adapter\order
     */
    public static $order;

    /**
     * @return \Ipol\Fivepost\Bitrix\Adapter\Order
     */
    public static function getOrder()
    {
        return self::$order;
    }

    protected static function initOrder()
    {
        $options = new Options();
        self::$order = new Order($options);
    }

    public static function loadCMSOrder($bitrixId)
    {
        self::initOrder();
        return self::getOrder()->newOrder($bitrixId)->getBaseOrder();
    }

    public static function loadUploadOrder($bitrixId,$mode=1)
    {
        self::initOrder();
        return self::getOrder()->uploadedOrder($bitrixId)->getBaseOrder();
    }

    public static function loadUploadOrderBy5I($fivePostId)
    {
        self::initOrder();
        return self::getOrder()->uploadedOrderBy5I($fivePostId)->getBaseOrder();
    }

    public static function calculateOrder()
    {

    }

    public static function sendOrder()
    {
        $obReturn = new BasicResponse();
        self::initOrder();
        $obOrder = self::getOrder()->requestOrder()->getBaseOrder();

        $resultAdd = self::saveOrder($obOrder);

        if($resultAdd[0]->isSuccess()){
            if($resultAdd[1] === 'new') {
                $oldVal = Option::get('barkCounter');
                Option::set('barkCounter', $oldVal + 1);
            }
            $obOrder->setField('createDate',$obOrder->getField('createDateFP'));
            if($obOrder->getField('plannedReceiveDateFP')){
                $obOrder->setField('plannedReceiveDate',$obOrder->getField('plannedReceiveDateFP'));
            }
            if($obOrder->getField('shipmentDateFP')){
                $obOrder->setField('shipmentDate',$obOrder->getField('shipmentDateFP'));
            }
            $controller = new \Ipol\Fivepost\Bitrix\Controller\Order(self::$MODULE_ID,self::$MODULE_LBL,$obOrder);
            $obResponse = $controller->send();
            if($obResponse->isSuccess()){
                $obReturn = $obResponse;
                $resultAdd = self::markOrderSended($obOrder->getField('orderId'));
            } else {
                $obReturn = $obResponse;
            }
        } else {
            $arErrors = $resultAdd[0]->getErrors();
            $arReturnErrors = array();
            /** @var \Bitrix\Main\ORM\Fields\FieldError $arError */
            foreach ($arErrors as $arError){
                $arReturnErrors []= $arError->getMessage();
            }
            $obReturn->setSuccess(false)
                     ->setErrorText(implode(',',$arReturnErrors));
        }
        if(array_key_exists(self::$MODULE_LBL.'action',$_REQUEST) && $_REQUEST[self::$MODULE_LBL.'action']){
            echo json_encode(array(
                'success' => $obReturn->isSuccess(),
                'error'   => $obReturn->getErrorText()
            ));
        } else {
            return $obReturn;
        }
    }

    public static function getOrderBarcode()
    {
        $controller = new \Ipol\Fivepost\Bitrix\Controller\Order(self::$MODULE_ID,self::$MODULE_LBL);

        if(array_key_exists(self::$MODULE_LBL.'action',$_REQUEST) && $_REQUEST[self::$MODULE_LBL.'action'] === __METHOD__){
            echo $controller->generateBarcode();
        } else {
            return $controller->generateBarcode();
        }

        return false;
    }

    /**
     * @param $bitrixId
     * @return BasicResponse
     */
    public static function deleteOrder($bitrixId)
    {
        if(is_array($bitrixId)){
            $bitrixId = $bitrixId['bitrixId'];
        }

        $obReturn = new BasicResponse();
        $obOrder = self::loadUploadOrder($bitrixId);

        $controller = new \Ipol\Fivepost\Bitrix\Controller\Order(self::$MODULE_ID,self::$MODULE_LBL,$obOrder);

        $obResult = $controller->delete();

        if($obResult->isSuccess()){
            $obReturn->setSuccess(true);

            StatusHandler::checkStatusByBI($bitrixId);
        } else {
            $obReturn->setSuccess(false)
                     ->setErrorText($obResult->getErrorText());
        }

        if(array_key_exists(self::$MODULE_LBL.'action',$_REQUEST) && $_REQUEST[self::$MODULE_LBL.'action']){
            echo json_encode(array(
                'success' => $obReturn->isSuccess(),
                'error'   => $obReturn->getErrorText()
            ));
        } else {
            return $obReturn;
        }
    }


    /**
     * @param \Ipol\Fivepost\Core\Order\Order $obOrder
     * @return array (\Bitrix\Main\ORM\Data\AddResult,typeUpdate)'
     * Полностью сохраняет имеющийся заказ (базу) в БД.
     */
    public static function saveOrder($obOrder)
    {
        $arAdd = array(
            'BITRIX_ID'   => $obOrder->getField('orderId'),
            'FIVEPOST_ID' => $obOrder->getField('barcode'),
            'STATUS' => 'NEW',
            'FIVEPOST_STATUS' => '',
            'FIVEPOST_EXECUTION_STATUS' => '',
            'BRAND_NAME'   => $obOrder->getField('brandName'),
            'CLIENT_NAME'  => $obOrder->getReceivers()->getFirst()->getName(),
            'CLIENT_EMAIL' => $obOrder->getReceivers()->getFirst()->getEmail(),
            'CLIENT_PHONE' => $obOrder->getReceivers()->getFirst()->getPhone(),
            'SHIPMENT_DATE' => '',
            'RECEIVER_LOCATION' => $obOrder->getField('receiverLocation'),
            'SENDER_LOCATION' => $obOrder->getField('senderLocation'),
            'UNDELIVERABLE_OPTION' => $obOrder->getField('undeliverableOption'),
            'CARGOES' => serialize(array('items'=>$obOrder->getItems(),'goods'=>$obOrder->getGoods())),
            'DELIVERY_COST' => $obOrder->getPayment()->getDelivery()->getAmount(),
            'DELIVERY_COST_CURRENCY' => $obOrder->getField('deliveryCostCurrency'),
            'PAYMENT_VALUE' => $obOrder->getPayment()->getGoods()->getAmount(),
            'PAYMENT_TYPE' =>  $obOrder->getPayment()->getType(),
            'PAYMENT_CURRENCY' => $obOrder->getField('currency'),
            'PRICE' => $obOrder->getPayment()->getEstimated()->getAmount(),
            'PRICE_CURRENCY' => $obOrder->getField('priceCurrency'),
            'UPTIME' => mktime()
        );
        if($obOrder->getField('plannedReceiveDateDB')){
            $arAdd['PLANNED_RECEIVE_DATE'] = $obOrder->getField('plannedReceiveDateDB');
        }
        if($obOrder->getField('shipmentDateDB')){
            $arAdd['SHIPMENT_DATE'] = $obOrder->getField('shipmentDateDB');
        }

        $check = OrdersTable::getByBitrixId($obOrder->getField('orderId'));
        if($check) {
            $obAddDB = OrdersTable::update($check['ID'],$arAdd);
            $type='update';
        } else {//update
            $obAddDB = OrdersTable::add($arAdd);
            $type='new';
        }

        return array($obAddDB,$type);
    }

    public static function markOrderSended($bitrixId){
        $tableId = OrdersTable::getByBitrixId($bitrixId);
        if($tableId) {
            OrdersTable::update($tableId['ID'], array(
                'OK'     => '1',
                'STATUS' => 'ok'
            ));
        }
    }

    // other
    public static function countCargoGabs($params){
        $answer = array('success' => false);
        if(!$params['orderId']){
            $answer['error'] = 'No order Id';
        }elseif(!count($params['items'])){
            $answer['error'] = 'No items';
        }else{
            $arItems = GoodsPicker::fromArray($params['items'],$params['orderId']);
            $obCargo = new Cargo(new DefaultGabarites());
            $obCargo->set($arItems);

            $answer = array(
                'success'    => true,
                'weight'     => $obCargo->getCargo()->getWeight(),
                'dimensions' => $obCargo->getCargo()->getDimensions(),
                'cargo'      => $params['cargo']
            );
        }

        if($params[self::$MODULE_LBL.'action'])
            echo Tools::jsonEncode($answer);

        return $answer;
    }

    /**
     * @param $orderId
     * @return bool|mixed
     * С каким тарифом был оформлен заказ
     */
    public static function getOrderTarif($orderId)
    {
//        $order = orderHandler::loadCMSOrder($orderId);
//        return $order->getField('deliveryMode');
    }

    // PVZ
    public static function getCityPVZ($city,$default = false){
//        if(is_array($city)){
//            $default = $city['default'];
//            $city    = $city['city'];
//        }
//
//        $arReturn = self::_getCityPVZ($city,$default);
//
//        if(!$arReturn['success'] && empty($arReturn['result']) && strpos($city,Tools::getMessage('SIGN_YO'))){
//            $arReturn = self::_getCityPVZ(str_replace(Tools::getMessage('SIGN_YO'),Tools::getMessage('SIGN_YE'),$city),$default);
//        }
//
//        if($_REQUEST[self::$MODULE_LBL.'action']) {
//            echo Tools::jsonEncode($arReturn);
//        }
//
//        return $arReturn;
    }

    public static function getSavedPVZ($code){
//        if(is_array($code)){
//            $code = $code['code'];
//        }
//
//        $arReturn = array('success'=>false,'result'=>false);
//
//        $pickupPointController = new pickupPoints();
//        if($code) {
//            $obPVZ = $pickupPointController->getById($code);
//            if($obPVZ){
//                $arReturn = array('success' => true,  'result' => array(
//                    'city'    => $obPVZ->getCity(),
//                    'address' => $obPVZ->getAddress(),
//                    'id'      => $obPVZ->getId()
//                ));
//            } else {
//                $arReturn = array('success' => false, 'result' => 'Not found');
//            }
//        }
//
//        if($_REQUEST[self::$MODULE_LBL.'action']) {
//            echo Tools::jsonEncode($arReturn);
//        }
//
//        return $arReturn;
    }

}