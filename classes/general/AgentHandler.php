<?
namespace Ipol\Fivepost;

/**
 * Class agentHandler
 * @package Ipol\Fivepost\
 * Предназначен для работы с агентами. Через addAgent добавляется агент. Все функции агентов обращаются на этот класс, где уже запускаются нужные обработчики.
 *
 */
class AgentHandler extends AbstractGeneral
{
    public static function addAgent($agent,$interval = 1800)
    {
        $result = null;
        if(
            method_exists('Ipol\Fivepost\AgentHandler',$agent) &&
            $agent !== 'addAgent'
        ){
            $result = \CAgent::AddAgent('\Ipol\Fivepost\agentHandler::'.$agent.'();',self::$MODULE_ID,"N",$interval);
        }

        return $result;
    }

    /**
     * @return string
     * Пример обработчика агента. Не забываем возвращать строку с путем агента, чтобы он вызывался не один раз.
     */
    public static function refreshStatuses()
    {
        StatusHandler::refreshOrderStates();
        return '\Ipol\Fivepost\agentHandler::refreshStatuses();';
    }

    /**
     * Sync points, rates, locations data
     * @return string
     */
    public static function syncServiceData()
    {
        SyncHandler::syncServiceData();
        return '\Ipol\Fivepost\AgentHandler::syncServiceData();';
    }
}