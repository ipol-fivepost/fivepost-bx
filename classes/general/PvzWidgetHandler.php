<?
namespace Ipol\Fivepost;

IncludeModuleLangFile(__FILE__);

use Bitrix\Sale\Order;
use Bitrix\Sale\Payment;
use Bitrix\Sale\Shipment;
use Ipol\Fivepost\Bitrix\Adapter\Cargo;
use Ipol\Fivepost\Bitrix\Entity\DefaultGabarites;
use Ipol\Fivepost\Bitrix\Handler\GoodsPicker;
use Ipol\Fivepost\Bitrix\Handler\LocationsDelivery;
use Ipolh\Pony\Bitrix\Controller\pickupPoints;
use Ipolh\Pony\Bitrix\Tools;
use Ipolh\Pony\Bitrix\Adapter;
use Ipolh\Pony\Bitrix\Entity\options;
use Ipolh\Pony\Bitrix\Handler\deliveries;

class PvzWidgetHandler extends abstractGeneral
{
    protected static $postField = 'PVZ';

    public static $city;
    public static $PAY_SYSTEM_ID  = false;
    public static $PERSON_TYPE_ID = false;
    public static $DELIVERY_ID     = false;

    public static $selDeliv;
    public static $widjetLoaded = false;
    public static $options      = false;

    public static function loadWidjet(){
        if(\Ipol\Fivepost\Bitrix\Handler\Deliveries::isActive() && $_REQUEST['is_ajax_post'] != 'Y' && $_REQUEST["AJAX_CALL"] != 'Y' && !$_REQUEST["ORDER_AJAX"]) {
            \CJSCore::Init(array('jquery'));
            $pathToController = \Ipol\Fivepost\Bitrix\Tools::getJSPath().'pvzWidjet.js';
            $pathToWidjet     = \Ipol\Fivepost\Bitrix\Tools::getJSPath().'widjet/widjet.js';

$GOODS = GoodsPicker::fromBasket();
$obDefGabs = new DefaultGabarites();
$obCargo = new Cargo($obDefGabs);
$cargos = $obCargo->set($GOODS)->getCargo();
$dims   = $cargos->getDimensions();

            if(
                file_exists($_SERVER['DOCUMENT_ROOT'].$pathToWidjet) &&
                file_exists($_SERVER['DOCUMENT_ROOT'].$pathToController)
            ){
                $GLOBALS['APPLICATION']->AddHeadScript($pathToWidjet);
                $GLOBALS['APPLICATION']->AddHeadScript($pathToController);
                ?>
                <script type="text/javascript">
                    var <?=self::getMODULELBL()?>PVZWidjet = new ipol_fivepost_pvzWidjet(
                        '<?=self::$city?>',
                        <?=\CUtil::PhpToJSObject(self::getProfileLink())?>,
                        '<?=self::getSavingLink()?>', // where pvz saves via request
                        '<?=self::getPostField()?>', // contents result of calculations?,
                        <?=\CUtil::PhpToJSObject(self::getAddressInput())?>, // id props where ID is saved
                        '<?=(self::$options->fetchPvzLabel()) ? self::$options->fetchPvzLabel() : \Ipol\Fivepost\Bitrix\Tools::getMessage('WIDJET_CHOOSE')?>',
                        '<?=self::$MODULE_LBL?>',
                        {
                            WRONG_PAY: '<?=\Ipol\Fivepost\Bitrix\Tools::getMessage('WIDJET_ERROR_WRONGPAY')?>',
                            PVZTYPE_POSTAMAT : '<?=\Ipol\Fivepost\Bitrix\Tools::getMessage('WIDJET_PVZTYPE_POSTAMAT')?>',
                            PVZTYPE_ISSUE_POINT : '<?=\Ipol\Fivepost\Bitrix\Tools::getMessage('WIDJET_PVZTYPE_ISSUE_POINT')?>',
                            PVZTYPE_TOBACCO : '<?=\Ipol\Fivepost\Bitrix\Tools::getMessage('WIDJET_PVZTYPE_TOBACCO')?>',
                        },
                        <?=\CUtil::PhpToJSObject(array(
                            'length'     => $dims['L'],
                            'width'      => $dims['W'],
                            'height'     => $dims['H'],
                            'price'      => $cargos->getTotalPrice(),
                            'weight'     => $cargos->getWeight()
                        ))?>
                    );
                    <?=self::getMODULELBL()?>PVZWidjet.setObRequestConcat({
                        getBasket : true,
                    });
                </script>
                <?
                \Ipol\Fivepost\Bitrix\Tools::getCommonCss();
            }
        }
    }

    // WORKOUT

    /**
     * @return array of places for linking with widjet
     */
    public static function getProfileLink()
    {
        $arProfiles = \Ipol\Fivepost\Bitrix\Handler\Deliveries::getActualProfiles(true);
        $objProfiles = array();
        if(Option::get('pvzID')) {
            $objProfiles = array(
                option::get('pvzID') => array(
                    'tag'   => false,
                    'price' => false,
                    'self'  => true,
                    'link'  => array_pop(array_pop($arProfiles))
                )
            );
        } else {
            foreach($arProfiles as $id => $arProfile) {
                $objProfiles[$id] = array(
                    'tag'   => false,
                    'price' => false,
                    'self'  => false,
                );
            }
        }

        return $objProfiles;
    }

    /**
     * In which key of ajax answer put the widjet data
     * @return string
     */
    public static function getPostField()
    {
        return self::$MODULE_LBL.self::$postField;
    }

    /**
     * In what key of request will be the id of chosen PVZ
     * @return string
     */
    public static function getSavingLink(){
//        return self::$MODULE_LBL.'pickup_pvz';
        return 'POINT_GUID';
    }

    public static function getAddressInput(){
        $arInputs = array();
        if(\cmodule::includeModule('sale')){
            $orderProp = self::getOptions()->fetchPvzPicker();
            $dbProps = \CSaleOrderProps::GetList(array(),array('CODE' => $orderProp));

            while($arProp=$dbProps->Fetch())
                $arInputs []= $arProp['ID'];
        }

        return $arInputs;
    }

    /**
     * @return \Ipol\Fivepost\Bitrix\Entity\Options
     */
    protected static function getOptions(){
        if(!self::$options){
            self::$options = new \Ipol\Fivepost\Bitrix\Entity\Options();
        }

        return self::$options;
    }

    /**
     * @param $content
     * добавляем данные по услугам в тело страницы
     */
    public static function addWidjetData(&$content){
        if(\Ipol\Fivepost\Bitrix\Handler\Deliveries::isActive()) {
            $noJson = self::no_json($content);
            if (($_REQUEST['is_ajax_post'] == 'Y' || $_REQUEST["AJAX_CALL"] == 'Y' || $_REQUEST["ORDER_AJAX"]) && $noJson) {
                $content
                    .= '<input type="hidden"
                                id="' . self::getPostField() . '"
                                name="' . self::getPostField() . '"
                                value=\'' . \Ipol\Fivepost\Bitrix\Tools::jsonEncode(array(
                        'city'   => self::$city,
                        'paysys' => DeliveryHandler::definePaysystem(),
                        'PAY_SYSTEM_ID' => self::$PAY_SYSTEM_ID,
                        'PERSON_TYPE_ID' => self::$PERSON_TYPE_ID,
                        'DELIVERY_ID'    => self::$DELIVERY_ID
                    )) . '\' />
                        ';
            } elseif (
                (
                    $_REQUEST['action'] == 'refreshOrderAjax' ||
                    $_REQUEST['soa-action'] == 'refreshOrderAjax'
                ) &&
                !$noJson
            ) {
                $content = json_decode($content,true);
                $content [self::getPostField()] =  array(
                    'city'   => self::$city,
                    'paysys' => DeliveryHandler::definePaysystem(),
                    'PAY_SYSTEM_ID' => self::$PAY_SYSTEM_ID,
                    'PERSON_TYPE_ID' => self::$PERSON_TYPE_ID,
                    'DELIVERY_ID'     => self::$DELIVERY_ID
                );
                $content = json_encode($content);

            }
        }
    }

    public static function no_json($wat){
        return is_null(json_decode($wat,true));
    }

    public static function prepareData($arResult,$arUserResult){
        if(!\Ipol\Fivepost\Bitrix\Handler\Deliveries::isActive())
        {
            return false;
        }
        if($arUserResult['DELIVERY_LOCATION'])
        {
            $location = $arUserResult['DELIVERY_LOCATION'];
        }else
        {
            $locationProp = \CSaleOrderProps::GetList(array(),array(
                'PERSON_TYPE_ID' => $arUserResult['PERSON_TYPE_ID'],
                'ACTIVE'         => 'Y',
                'IS_LOCATION'    => 'Y'
            ))->Fetch();
            if($arUserResult['ORDER_PROP'][$locationProp['ID']])
            {
                $location = $arUserResult['ORDER_PROP'][$locationProp['ID']];
            }
            else
            {
                $location = $_REQUEST['order']['ORDER_PROP_'.$locationProp['ID']];
            }
        }

        if($location) {
            $location = \Ipol\Fivepost\Bitrix\Adapter::locationById($location);
            if($location->getLocationLink()) {
                self::$city   = $location->getLocationLink()->getApi()->getCode();
            }
        }

        if($arUserResult['PAY_SYSTEM_ID']){
            self::$PAY_SYSTEM_ID = $arUserResult['PAY_SYSTEM_ID'];
        }
        if($arUserResult['PERSON_TYPE_ID']){
            self::$PERSON_TYPE_ID = $arUserResult['PERSON_TYPE_ID'];
        }
        if($arUserResult['DELIVERY_ID']){
            self::$DELIVERY_ID = $arUserResult['DELIVERY_ID'];
        }

        return true;
    }


    /**
     * Gets PVZ code from request after making order
     * @return bool|string of id
     */
    protected static function getRequestPVZ()
    {
        $check = (
            !array_key_exists(PvzWidgetHandler::getSavingLink(), $_REQUEST) ||
            !$_REQUEST[PvzWidgetHandler::getSavingLink()] ||
            $_REQUEST[PvzWidgetHandler::getSavingLink()] == 'false'
        ) ? false : $_REQUEST[PvzWidgetHandler::getSavingLink()];

        return $check;
    }

    /**
     * @param Order $entity
     * @param $values
     * @return \Bitrix\Main\EventResult|bool
     */
    public static function checkPVZProp($entity,$values)
    {
        $options = new \Ipol\Fivepost\Bitrix\Entity\Options();
        if(
            !\Ipol\Fivepost\Bitrix\Tools::isAdminSection() &&
            \Ipol\Fivepost\Bitrix\Handler\Deliveries::isActive() &&
            $options->fetchNoPVZnoOrder() == 'Y'
        ) {
            $shipmentCollection = $entity->getShipmentCollection();
            $shipmentCollection->rewind();
            /** @var Shipment $obShipment */
            while($obShipment = $shipmentCollection->next()){
                if ($obShipment->isSystem()) {
                    continue;
                }

                $delivery = \Ipol\Fivepost\Bitrix\Handler\Deliveries::defineDelivery($obShipment->getField('DELIVERY_ID'));
                if ($delivery === 'pickup' && !self::getRequestPVZ())
                {
                    return new \Bitrix\Main\EventResult(\Bitrix\Main\EventResult::ERROR, new \Bitrix\Sale\ResultError(\Ipol\Fivepost\Bitrix\Tools::getMessage('ERROR_NOPVZ'), 'code'), 'sale');
                }
            }
            $shipmentCollection->rewind();
        }
        return true;
    }

    /**
     * @param Order $entity
     * @param $values
     * @return \Bitrix\Main\EventResult|bool
     */
    public static function checkPVZPaysys($entity, $values)
    {
        $options = new \Ipol\Fivepost\Bitrix\Entity\Options();
        if(
            !\Ipol\Fivepost\Bitrix\Tools::isAdminSection() &&
            \Ipol\Fivepost\Bitrix\Handler\Deliveries::isActive()
        ){
            $shipmentCollection = $entity->getShipmentCollection();

            $shipmentCollection->rewind();
            /** @var Shipment $obShipment */
            while($obShipment = $shipmentCollection->next()){
                if($obShipment->isSystem()){
                    continue;
                }

                $delivery = \Ipol\Fivepost\Bitrix\Handler\Deliveries::defineDelivery($obShipment->getField('DELIVERY_ID'));

                if($delivery === 'pickup')
                {
                    $checkMode   = false;
                    $nalPayment  = $options->fetchPayNal();
                    $cardPayment = $options->fetchPayCard();
                    $paymentCollection = $entity->getPaymentCollection();
                    $paymentCollection->rewind();
                    /** @var Payment $obPayment */
                    // while($obPayment = $paymentCollection->next()){ // not working, löl
                    foreach($paymentCollection as $index => $obPayment){
                        if(in_array($obPayment->getField('PAY_SYSTEM_ID'),$nalPayment)){
                            $checkMode = 'CASH';
                        } elseif(in_array($obPayment->getField('PAY_SYSTEM_ID'),$cardPayment)){
                            $checkMode = 'CARD';
                        }
                        if($checkMode){
                            break;
                        }
                    }
                    $paymentCollection->rewind();

                    $allow      = true;
                    $arPoints   = false;
                    if($checkMode) {
                        if ($chosenPVZ = self::getRequestPVZ()) {
                            $arPoints = PointsTable::getByPointGuid($chosenPVZ);
                            if($arPoints){
                                $allow = ($arPoints[$checkMode.'_ALLOWED'] === 'Y');
                            }
                        }
                    }

                    if(!$allow && $arPoints){
                        $errorMess = \Ipol\Fivepost\Bitrix\Tools::getMessage('ERROR_NOPAY_'.$checkMode);
                        return new \Bitrix\Main\EventResult(\Bitrix\Main\EventResult::ERROR, new \Bitrix\Sale\ResultError($errorMess, 'code'), 'sale');
                    }
                }
            }
            $shipmentCollection->rewind();
        }

        return true;
    }

    protected static $answer = false;

    // widjet stuff

    public static function getPVZ()
    {
        $obPoints = PointsHandler::getPoints();
        $arCities = LocationsHandler::getCities();
        $arPoints = array(
            'POINTS' => array(),
            'CITY' => array(),
            'CITYREG' => array(),
            'REGIONSMAP' => array(),
            'CITYFULL' => array(),
            'REGIONS' => array(),
        );

        if($obPoints->isSuccess()){
            $curPoints = $obPoints->getData();
            $arPoints['POINTS'] = $curPoints['POINTS'];
        }

        foreach ($arCities as $arCity){
            $arPoints['CITY'][$arCity['CODE']]    = $arCity['NAME'];
            $arPoints['CITYREG'][$arCity['CODE']] = $arCity['REGION'];
            if(!array_key_exists($arCity['REGION'],$arPoints['REGIONSMAP'])){
                $arPoints['REGIONSMAP'][$arCity['REGION']] = array();
            }
            $arPoints['REGIONSMAP'][$arCity['REGION']] = $arCity['CODE'];
            $arPoints['CITYFULL'][$arCity['CODE']] = $arCity['COUNTRY'].' '.$arCity['REGION'].' '.$arCity['NAME'];
            $arPoints['REGIONS'][$arCity['CODE']] = implode(', ', array_filter(array($arCity['REGION'],$arCity['COUNTRY'])));
        }

        self::toAnswer(array('pvz' => $arPoints));
        self::printAnswer();
    }

    public static function getLang(){
        self::toAnswer(array('LANG' => self::getLangArray()));
        self::printAnswer();
    }

    public static function getLangArray()
    {
        $tanslate = array(
            'rus' => array(
                'YOURCITY'   => \Ipol\Fivepost\Bitrix\Tools::getMessage('WIDGET_YOURCITY'),
                'COURIER'    => \Ipol\Fivepost\Bitrix\Tools::getMessage('WIDGET_COURIER'),
                'PICKUP'     => \Ipol\Fivepost\Bitrix\Tools::getMessage('WIDGET_PICKUP'),
                'TERM'       => \Ipol\Fivepost\Bitrix\Tools::getMessage('WIDGET_TERM'),
                'PRICE'      => \Ipol\Fivepost\Bitrix\Tools::getMessage('WIDGET_PRICE'),
                'DAY'        => \Ipol\Fivepost\Bitrix\Tools::getMessage('WIDGET_DAY'),
                'RUB'        => \Ipol\Fivepost\Bitrix\Tools::getMessage('WIDGET_RUB'),
                'NODELIV'    => \Ipol\Fivepost\Bitrix\Tools::getMessage('WIDGET_NODELIV'),
                'CITYSEARCH' => \Ipol\Fivepost\Bitrix\Tools::getMessage('WIDGET_CITYSEARCH'),
                'ALL'        => \Ipol\Fivepost\Bitrix\Tools::getMessage('WIDGET_ALL'),
                'PVZ'        => \Ipol\Fivepost\Bitrix\Tools::getMessage('WIDGET_PVZ'),
                'MOSCOW'     => \Ipol\Fivepost\Bitrix\Tools::getMessage('WIDGET_MOSCOW'),
                'RUSSIA'     => \Ipol\Fivepost\Bitrix\Tools::getMessage('WIDGET_RUSSIA'),
                'COUNTING'   => \Ipol\Fivepost\Bitrix\Tools::getMessage('WIDGET_COUNTING'),
                'NO_AVAIL'          => \Ipol\Fivepost\Bitrix\Tools::getMessage('WIDGET_NO_AVAIL'),
                'NO_PAY'            => \Ipol\Fivepost\Bitrix\Tools::getMessage('WIDGET_NO_PAY'),
                'CHOOSE_TYPE_AVAIL' => \Ipol\Fivepost\Bitrix\Tools::getMessage('WIDGET_CHOOSE_TYPE_AVAIL'),
                'CHOOSE_OTHER_CITY' => \Ipol\Fivepost\Bitrix\Tools::getMessage('WIDGET_CHOOSE_OTHER_CITY'),
                'TYPE_ADDRESS'      => \Ipol\Fivepost\Bitrix\Tools::getMessage('WIDGET_TYPE_ADDRESS'),
                'TYPE_ADDRESS_HERE' => \Ipol\Fivepost\Bitrix\Tools::getMessage('WIDGET_TYPE_ADDRESS_HERE'),
                'L_ADDRESS' => \Ipol\Fivepost\Bitrix\Tools::getMessage('WIDGET_L_ADDRESS'),
                'L_TIME'    => \Ipol\Fivepost\Bitrix\Tools::getMessage('WIDGET_L_TIME'),
                'L_WAY'     => \Ipol\Fivepost\Bitrix\Tools::getMessage('WIDGET_L_WAY'),
                'L_DESCR'   => \Ipol\Fivepost\Bitrix\Tools::getMessage('WIDGET_L_DESCR'),
                'L_PRICE'     => \Ipol\Fivepost\Bitrix\Tools::getMessage('WIDGET_L_PRICE'),
                'L_CHOOSE'  => \Ipol\Fivepost\Bitrix\Tools::getMessage('WIDGET_L_CHOOSE'),
                'H_LIST'    => \Ipol\Fivepost\Bitrix\Tools::getMessage('WIDGET_H_LIST'),
                'H_PROFILE' => \Ipol\Fivepost\Bitrix\Tools::getMessage('WIDGET_H_PROFILE'),
                'H_CASH'    => \Ipol\Fivepost\Bitrix\Tools::getMessage('WIDGET_H_CASH'),
                'H_CARD'   => \Ipol\Fivepost\Bitrix\Tools::getMessage('WIDGET_H_CARD'),
                'H_SUPPORT' => \Ipol\Fivepost\Bitrix\Tools::getMessage('WIDGET_H_SUPPORT'),
                'H_QUESTIONS' => \Ipol\Fivepost\Bitrix\Tools::getMessage('WIDGET_H_QUESTIONS'),
                'DAY0' => \Ipol\Fivepost\Bitrix\Tools::getMessage('WIDGET_DAY0'),
                'DAY1' => \Ipol\Fivepost\Bitrix\Tools::getMessage('WIDGET_DAY1'),
                'DAY2' => \Ipol\Fivepost\Bitrix\Tools::getMessage('WIDGET_DAY2'),
                'DAY3' => \Ipol\Fivepost\Bitrix\Tools::getMessage('WIDGET_DAY3'),
                'DAY4' => \Ipol\Fivepost\Bitrix\Tools::getMessage('WIDGET_DAY4'),
                'DAY5' => \Ipol\Fivepost\Bitrix\Tools::getMessage('WIDGET_DAY5'),
                'DAY6' => \Ipol\Fivepost\Bitrix\Tools::getMessage('WIDGET_DAY6'),
                'L_PAYMENT' => \Ipol\Fivepost\Bitrix\Tools::getMessage('WIDGET_L_PAYMENT'),
                'ERROR_WRONGPAY' => \Ipol\Fivepost\Bitrix\Tools::getMessage('WIDJET_ERROR_WRONGPAY'),
            )
        );

        if (isset($_REQUEST['lang']) && isset($tanslate[$_REQUEST['lang']]) ) return $tanslate[$_REQUEST['lang']];
        else return $tanslate['ru'];
    }

    public static function calcPVZ(){
        $blockedByFilter = false;
        if($_REQUEST['filters']){
            $obPoint = PointsTable::getByPointGuid($_REQUEST['shipment']['pointId']);
            if($obPoint){
                foreach($_REQUEST['filters'] as $filter => $val){
                    $blockedByFilter = (!array_key_exists($filter,$obPoint) || $obPoint[$filter] !== $val);
                }
            }
        }

        $arCalc = array();

        $arResponse = array(
            'result' => array(
                'pointId' => $_REQUEST['shipment']['pointId']
            ),
        );

        switch(true){
            case (array_key_exists('getBasket',$_REQUEST) && $_REQUEST['getBasket']) : $setter = array('basket' => true);break;
            case (array_key_exists('getOrder', $_REQUEST) && $_REQUEST['getOrder']) :  $setter = array('order'  => $_REQUEST['getOrder']);break;
            default : $setter = false; break;
        }

        if(!$blockedByFilter) {
            $obCity = LocationsDelivery::getByFiasGuid($_REQUEST['shipment']['city']);
            if ($obCity) {
                $arOrder = array(
                    'WEIGHT' => $_REQUEST['shipment']['goods'][0]['weight'],
                    'DIMENSIONS' => array(
                        'L' => $_REQUEST['shipment']['goods'][0]['length'],
                        'W' => $_REQUEST['shipment']['goods'][0]['width'],
                        'H' => $_REQUEST['shipment']['goods'][0]['height'],
                    ),
                    'LOCATION' => $obCity['BITRIX_CODE'],
                    'POINT_GUID' => $_REQUEST['shipment']['pointId']
                );
                if(array_key_exists('PAY_SYSTEM_ID',$_REQUEST) && $_REQUEST['PAY_SYSTEM_ID']){
                    $arOrder['PAY_SYSTEM_ID'] = $_REQUEST['PAY_SYSTEM_ID'];
                }
                if(array_key_exists('PERSON_TYPE_ID',$_REQUEST) && $_REQUEST['PERSON_TYPE_ID']){
                    $arOrder['PERSON_TYPE_ID'] = $_REQUEST['PERSON_TYPE_ID'];
                }

                $arCalc  = DeliveryHandler::calculateDelivery($arOrder,$setter);
            }
        }

        if(empty($arCalc)){
            $arResponse['error'] = 'No calculation';
        } else {
            foreach ($arCalc as $arProfiles){
                foreach ($arProfiles as $arResult){
                    $arResponse['result']['price'] = $arResult['PRICE'];
                    $arResponse['result']['term']  = $arResult['PERIOD'];

                    if($arResult['ERROR']){
                        $arResponse['error'] = $arResult['ERROR'];
                    }

                    break;
                }
                break;
            }
        }


        self::toAnswer($arResponse);
        self::printAnswer();
    }

    protected static function toAnswer($wat)
    {
        $stucked = array('error');
        if (!is_array($wat)) {
            $wat = array('info' => $wat);
        }
        if (!is_array(self::$answer)) {
            self::$answer = array();
        }
        foreach ($wat as $key => $sign) {
            if (in_array($key, $stucked)) {
                if (!array_key_exists($key, self::$answer)) {
                    self::$answer[$key] = array();
                }
                self::$answer[$key] [] = $sign;
            } else {
                self::$answer[$key] = $sign;
            }
        }
    }

    protected static function printAnswer()
    {
        echo json_encode(self::$answer);
    }
}