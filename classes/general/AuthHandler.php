<?php

namespace Ipol\Fivepost;


use Ipol\Fivepost\Bitrix\Entity\Encoder;
use Ipol\Fivepost\Bitrix\Entity\Options;
use Ipol\Fivepost\Bitrix\Tools;
use Ipol\Fivepost\Fivepost\FivepostApplication;

class AuthHandler extends AbstractGeneral
{
    public static function auth($params)
    {
        $workTry = self::authRequest($params,false);

        if($workTry === true){
            echo '%Y%';
        }else {
            $workTry = self::authRequest($params,true);
            if($workTry === true){
                echo '%T%';
            } else {
                echo Tools::getMessage('AUTH_ERROR');
            }
        }
    }

    public static function authRequest($params,$testMode = false){
        $encoder = new Encoder();
        $application = new FivepostApplication(
            $params['apiKey'],
            $testMode,
            40,
            $encoder
        );

        if($application->getLastError()){
            return false;
        } else {
            self::login($params['apiKey']);
            Option::set('isTest',($testMode) ? 'Y' : 'N');

            return true;
        }
    }

    public static function login($key)
    {
        SubscribeHandler::register();

        Option::set('apiKey',$key);

        AgentHandler::addAgent('refreshStatuses',1800);
        AgentHandler::addAgent('syncServiceData',1800);
    }

    public static function delogin()
    {
        Option::set('apiKey',false);
        Option::set('isTest','N');

        SubscribeHandler::unRegister();

        \CAgent::RemoveModuleAgents(self::$MODULE_ID);

        if(array_key_exists(self::$MODULE_LBL.'action',$_REQUEST))
            echo 'Y';
    }

    public static function isAuthorized()
    {
        $options = new Options();
        return (bool) $options->fetchApiKey();
    }
}