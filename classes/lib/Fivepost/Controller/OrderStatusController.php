<?php


namespace Ipol\Fivepost\Fivepost\Controller;

use Ipol\Fivepost\Api\BadResponseException;
use Ipol\Fivepost\Api\Entity\Request\GetOrderStatus;
use Ipol\Fivepost\Api\Entity\Request\Part\GetOrderStatus\OrderStatus;
use Ipol\Fivepost\Api\Entity\Request\Part\GetOrderStatus\OrderStatusList;
use Ipol\Fivepost\Api\Entity\Response\ErrorResponse;
use Ipol\Fivepost\Fivepost\AppLevelException;
use Ipol\Fivepost\Fivepost\Entity\OrderStatusResult;
use Ipol\Fivepost\Fivepost\ErrorResponseException;


class OrderStatusController extends RequestController
{
    /**
     * OrderStatusController constructor.
     * @param array $arNumbers
     * @param string $type
     */
    public function __construct(array $arNumbers, string $type)
    {
        $data = new GetOrderStatus();
        $orderCollection = new OrderStatusList();
        switch ($type)
        {
            case "uuid":
                foreach($arNumbers as $uuid)
                {
                    $order = new OrderStatus();
                    $order->setOrderId($uuid);
                    $orderCollection->add($order);
                }
                break;
            case "senderOrderId":
                foreach($arNumbers as $number)
                {
                    $order = new OrderStatus();
                    $order->setSenderOrderId($number);
                    $orderCollection->add($order);
                }
                break;
        }
        $data->setOrderStatuses($orderCollection);
        $this->setRequestObj($data);
    }

    public function execute(): OrderStatusResult
    {
        $result = new OrderStatusResult();

        try{
            $requestProcess = $this->getSDK()
                ->getOrderStatus($this->getRequestObj());

            $result->setSuccess($requestProcess->getResponse()->isRequestSuccess())
                ->setOrderStatus($requestProcess->getResponse());

            if(!$result->isSuccess() && is_a($requestProcess->getResponse(), ErrorResponse::class))
            {
                $result->setError(new ErrorResponseException($requestProcess->getResponse()));
            }
        }catch(BadResponseException | AppLevelException $e){
            $result->setSuccess(false)
                ->setError($e);
        }

        return $result;
    }
}