<?php


namespace Ipol\Fivepost\Fivepost\Controller;

use Ipol\Fivepost\Api\BadResponseException;
use Ipol\Fivepost\Fivepost\AppLevelException;
use Ipol\Fivepost\Fivepost\Entity\CancelOrderByNumberResult;
use Ipol\Fivepost\Api\Entity\Request\CancelOrderByNumber as CancelRequest;


/**
 * Class CancelOrderByNumber
 * @package Ipol\Fivepost\Fivepost\Controller
 */
class CancelOrderByNumber extends RequestController
{
    /**
     * CancelOrderByNumber constructor.
     * @param string $number
     */
    public function __construct(string $number)
    {
        $data = new CancelRequest($number);
        $this->setRequestObj($data);
    }

    /**
     * @return CancelOrderByNumberResult
     */
    public function execute(): CancelOrderByNumberResult
    {
        $result = new CancelOrderByNumberResult();

        try{
            $requestProcess = $this->getSDK()
                ->cancelOrderByNumber($this->getRequestObj());

            if($requestProcess->getResponse()->isRequestSuccess())
            {
                $result->setSuccess(true)
                    ->setCancelOrderByNumber($requestProcess->getResponse());
            }
            else
            {
                $result->setSuccess(false)
                    ->setError($requestProcess->getResponse());
            }

        }catch(BadResponseException | AppLevelException $e){
            $result->setSuccess(false)
                ->setError($e);
        }

        return $result;
    }
}