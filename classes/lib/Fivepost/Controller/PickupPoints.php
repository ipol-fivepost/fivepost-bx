<?php


namespace Ipol\Fivepost\Fivepost\Controller;

use Ipol\Fivepost\Api\BadResponseException;
use \Ipol\Fivepost\Api\Entity\Request\PickupPoints as PointsRequest;
use Ipol\Fivepost\Api\Entity\Response\ErrorResponse;
use Ipol\Fivepost\Fivepost\AppLevelException;
use Ipol\Fivepost\Fivepost\Entity\PickupPointsResult;
use Ipol\Fivepost\Fivepost\ErrorResponseException;


class PickupPoints extends RequestController
{
    /**
     * PickupPoints constructor.
     * @param int $pageNum
     * @param int $pageSize
     */
    public function __construct($pageNum = 0, $pageSize = 1000)
    {
        $data = new PointsRequest();
        $data->setPageNumber($pageNum)
            ->setPageSize($pageSize);

        $this->setRequestObj($data);
    }

    public function get(): PickupPointsResult
    {
        $result = new PickupPointsResult();

        try{
            $requestProcess = $this->getSDK()
                ->pickupPoints($this->getRequestObj());

            $result->setSuccess($requestProcess->getResponse()->isRequestSuccess())
                ->setPickupPoints($requestProcess->getResponse());

            if(!$result->isSuccess() && is_a($requestProcess->getResponse(), ErrorResponse::class))
            {
                $result->setError(new ErrorResponseException($requestProcess->getResponse()));
            }
        }catch(BadResponseException | AppLevelException $e){
            $result->setSuccess(false)
                ->setError($e);
        }
        return $result;
    }

}
