<?php


namespace Ipol\Fivepost\Fivepost\Controller;

use Ipol\Fivepost\Api\BadResponseException;
use Ipol\Fivepost\Api\Entity\Request\CreateOrder;
use Ipol\Fivepost\Api\Entity\Request\Part\CreateOrder\Barcode;
use Ipol\Fivepost\Api\Entity\Request\Part\CreateOrder\BarcodeList;
use Ipol\Fivepost\Api\Entity\Request\Part\CreateOrder\Cargo;
use Ipol\Fivepost\Api\Entity\Request\Part\CreateOrder\CargoList;
use Ipol\Fivepost\Api\Entity\Request\Part\CreateOrder\Cost;
use Ipol\Fivepost\Api\Entity\Request\Part\CreateOrder\PartnerOrder;
use Ipol\Fivepost\Api\Entity\Request\Part\CreateOrder\PartnerOrderList;
use Ipol\Fivepost\Api\Entity\Request\Part\CreateOrder\ProductValue;
use Ipol\Fivepost\Api\Entity\Request\Part\CreateOrder\ProductValueList;
use Ipol\Fivepost\Api\Entity\Response\ErrorResponse;
use Ipol\Fivepost\Core\Order\OrderCollection;
use Ipol\Fivepost\Fivepost\AppLevelException;
use Ipol\Fivepost\Fivepost\Entity\OrderResult;
use Ipol\Fivepost\Fivepost\ErrorResponseException;


/**
 * Class Order
 * @package Ipol\Fivepost\Fivepost\Controller
 */
class Order extends RequestController
{
    /**
     * @var OrderCollection
     */
    protected $orders;

    /**
     * @param OrderCollection $orders
     * @return $this
     */
    public function setOrders(OrderCollection $orders): Order
    {
        $this->orders = $orders;
        return $this;
    }

    /**
     * @return OrderCollection
     */
    public function getOrders(): ?OrderCollection
    {
        return $this->orders;
    }

    /**
     * @return $this
     */
    public function convert(): Order
    {
        $orders = $this->getOrders()->reset();
        $request = new CreateOrder();

        $obOrderCollection = new PartnerOrderList();
        while ($order = $orders->getNext())
        {
            $receiver = $order->getReceivers()->getFirst();
            $obOrder = new PartnerOrder();
            $obOrder->setClientEmail($receiver->getEmail())
                ->setSenderCreateDate($order->getField('createDate'))
                ->setSenderOrderId($order->getNumber())
                ->setBrandName(($order->getSender()) ? $order->getSender()->getCompany() : $order->getField('brandName'))
                ->setClientOrderId($order->getField('track'))
                ->setClientName($receiver->getName())
                ->setClientPhone($receiver->getPhone())
                ->setReceiverLocation($order->getField('receiverLocation'))
                ->setSenderLocation($order->getField('senderLocation'))
                ->setUndeliverableOption($order->getField('undeliverableOption'));

            if($order->getField('receiveDate')){
                $obOrder->setPlannedReceiveDate($order->getField('receiveDate'));
            }
            if($order->getField('shipmentDate')){
                $obOrder->setShipmentDate($order->getField('shipmentDate'));
            }

            $obCargoCollection = new CargoList();
            $obCargo = new Cargo();
            $obProductCollection = new ProductValueList();
            $items = $order->getItems()->reset();
            while($item = $items->getNext())
            {
                $obProduct = new ProductValue();
                $obProduct->setBarcode($item->getBarcode())
                    ->setCurrency($order->getField('currency'))
                    ->setVendorCode($item->getArticul())
                    ->setName($item->getName())
                    ->setPrice(floatval($item->getPrice()))
                    ->setValue(intval($item->getQuantity()))
                    ->setVat(intval($item->getVatRate()));

                if($item->getProperty('oc')){
                    $obProduct->setOriginCountry($item->getProperty('oc'));
                }
                if($item->getProperty('ccd')){
                    $obProduct->setCodeGTD($item->getProperty('ccd'));
                }
                if($item->getProperty('tnved')){
                    $obProduct->setCodeTNVED($item->getProperty('tnved'));
                }

                $obProductCollection->add($obProduct);
            }
            $obBarcodeCollection = new BarcodeList();
            foreach ($order->getField('barcodes') as $barcode)
            {
                $obBarcode = new Barcode();
                $obBarcode->setValue($barcode);
                $obBarcodeCollection->add($obBarcode);
            }
            $obCargo->setProductValues($obProductCollection)
                ->setBarcodes($obBarcodeCollection)
                ->setCurrency($order->getField('currency'))
                ->setPrice($order->getPayment()->getCost()->getAmount())
                ->setLength(intval($order->getGoods()->getLength()))
                ->setWidth(intval($order->getGoods()->getWidth()))
                ->setHeight(intval($order->getGoods()->getHeight()))
                ->setWeight(intval($order->getGoods()->getWeight()*1000))
                ->setSenderCargoId($order->getField('senderCargoId'));
            if($order->getPayment()->getNdsDefault()){
                $obCargo->setVat(intval($order->getPayment()->getNdsDefault()));
            }

            $obCargoCollection->add($obCargo);
            $obOrder->setCargoes($obCargoCollection);

            if($order->getPayment()->getType() == 'cash')
                $paymentType = 'CASH';
            else
                $paymentType = 'CASHLESS';
            if($order->getPayment()->getGoods()->getAmount() + $order->getPayment()->getDelivery()->getAmount() == $order->getPayment()->getPayed())
                $paymentType = 'PREPAYED';

            $obCost = new Cost();
            $obCost = new Cost();
            $obCost->setDeliveryCost($order->getPayment()->getDelivery()->getAmount())
                ->setDeliveryCostCurrency($order->getPayment()->getDelivery()->getCurrency())
                ->setPaymentCurrency($order->getPayment()->getGoods()->getCurrency())
                ->setPaymentType($paymentType)
                ->setPaymentValue($order->getPayment()->getGoods()->getAmount() + $order->getPayment()->getDelivery()->getAmount() - $order->getPayment()->getPayed()->getAmount())
                ->setPrice($order->getPayment()->getCost()->getAmount())
                ->setPriceCurrency($order->getPayment()->getCost()->getCurrency());
            $obOrder->setCost($obCost);

            $obOrderCollection->add($obOrder);
        }

        $request->setPartnerOrders($obOrderCollection);
        $this->setRequestObj($request);

        return $this;
    }

    /**
     * @return OrderResult
     */
    public function send(): OrderResult
    {
        $data = $this->getRequestObj();
        $result = new OrderResult();
        try{
            $requestProcess = $this->getSDK()
                ->createOrder($data);

            $result->setSuccess($requestProcess->getResponse()->isRequestSuccess())
                ->setOrder($requestProcess->getResponse());

            if(!$result->isSuccess() && is_a($requestProcess->getResponse(), ErrorResponse::class))
            {
                $result->setError(new ErrorResponseException($requestProcess->getResponse()));
            }
        }catch(BadResponseException | AppLevelException $e){
            $result->setSuccess(false)
                ->setError($e);
        }

        return $result;
    }
}