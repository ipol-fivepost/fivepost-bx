<?php


namespace Ipol\Fivepost\Fivepost\Controller;


use Ipol\Fivepost\Api\BadResponseException;
use Ipol\Fivepost\Api\Entity\Request\JwtGenerate;
use Ipol\Fivepost\Api\Entity\Response\ErrorResponse;
use Ipol\Fivepost\Fivepost\AppLevelException;
use Ipol\Fivepost\Fivepost\Entity\RequestJwtResult;
use Ipol\Fivepost\Fivepost\ErrorResponseException;

/**
 * Class RequestJwt
 * @package Ipol\Fivepost\Fivepost\Controller
 */
class RequestJwt extends RequestController
{
    /**
     * RequestJwt constructor.
     * @param string $apiKey
     */
    public function __construct(string $apiKey)
    {
        $data = new JwtGenerate();
        $data->setApikey($apiKey);

        $this->setRequestObj($data);
    }

    /**
     * @return RequestJwtResult
     */
    public function execute(): RequestJwtResult
    {
        $result = new RequestJwtResult();

        try{
            $requestProcess = $this->getSDK()
                ->jwtGenerate($this->getRequestObj());

            $result->setSuccess($requestProcess->getResponse()->isRequestSuccess())
                ->setResponse($requestProcess->getResponse());

            if(!$result->isSuccess() && is_a($requestProcess->getResponse(), ErrorResponse::class))
            {
                $result->setError(new ErrorResponseException($requestProcess->getResponse()));
            }
            else
            {
                $result->parseFields();
            }
        }catch(BadResponseException | AppLevelException $e){
            $result->setSuccess(false)
                ->setError($e);
        } finally {
            return $result;
        }
    }

}