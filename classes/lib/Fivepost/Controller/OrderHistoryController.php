<?php


namespace Ipol\Fivepost\Fivepost\Controller;

use Ipol\Fivepost\Api\BadResponseException;
use Ipol\Fivepost\Api\Entity\Request\GetOrderHistory;
use Ipol\Fivepost\Api\Entity\Response\ErrorResponse;
use Ipol\Fivepost\Fivepost\AppLevelException;
use Ipol\Fivepost\Fivepost\Entity\OrderHistoryResult;
use Ipol\Fivepost\Fivepost\ErrorResponseException;


/**
 * Class OrderHistoryController
 * @package Ipol\Fivepost\Fivepost\Controller
 */
class OrderHistoryController extends RequestController
{
    /**
     * OrderHistoryController constructor.
     * @param string $id
     * @param string $type
     */
    public function __construct(string $id, string $type)
    {
        $data = new GetOrderHistory();
        switch ($type)
        {
            case "uuid":
                $data->setOrderId($id);
                break;
            case "senderOrderId":
                $data->setSenderOrderId($id);
                break;
        }
        $this->setRequestObj($data);
    }

    /**
     * @return OrderHistoryResult
     */
    public function execute(): OrderHistoryResult
    {
        $result = new OrderHistoryResult();

        try{
            $requestProcess = $this->getSDK()
                ->getOrderHistory($this->getRequestObj());

            $result->setSuccess($requestProcess->getResponse()->isRequestSuccess())
                ->setOrderHistory($requestProcess->getResponse());

            if(!$result->isSuccess() && is_a($requestProcess->getResponse(), ErrorResponse::class))
            {
                $result->setError(new ErrorResponseException($requestProcess->getResponse()));
            }
        }catch(BadResponseException | AppLevelException $e){
            $result->setSuccess(false)
                ->setError($e);
        }

        return $result;
    }
}