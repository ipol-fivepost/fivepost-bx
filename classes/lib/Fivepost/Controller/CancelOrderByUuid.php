<?php


namespace Ipol\Fivepost\Fivepost\Controller;

use Ipol\Fivepost\Api\BadResponseException;
use Ipol\Fivepost\Api\Entity\Request\CancelOrderById;
use Ipol\Fivepost\Api\Entity\Response\ErrorResponse;
use Ipol\Fivepost\Fivepost\AppLevelException;
use Ipol\Fivepost\Fivepost\Entity\CancelOrderByUuidResult;
use Ipol\Fivepost\Fivepost\ErrorResponseException;


/**
 * Class CancelOrderByUuid
 * @package Ipol\Fivepost\Fivepost\Controller
 */
class CancelOrderByUuid extends RequestController
{
    /**
     * CancelOrderByUuid constructor.
     * @param string $uuid
     */
    public function __construct(string $uuid)
    {
        $data = new CancelOrderById($uuid);
        $this->setRequestObj($data);
    }

    /**
     * @return CancelOrderByUuidResult
     */
    public function execute(): CancelOrderByUuidResult
    {
        $result = new CancelOrderByUuidResult();

        try{
            $requestProcess = $this->getSDK()
                ->cancelOrderById($this->getRequestObj());

            $result->setSuccess($requestProcess->getResponse()->isRequestSuccess())
                ->setCancelOrderByUuid($requestProcess->getResponse());

            if(!$result->isSuccess() && is_a($requestProcess->getResponse(), ErrorResponse::class))
            {
                $result->setError(new ErrorResponseException($requestProcess->getResponse()));
            }
        }catch(BadResponseException | AppLevelException $e){
            $result->setSuccess(false)
                ->setError($e);
        }

        return $result;
    }
}