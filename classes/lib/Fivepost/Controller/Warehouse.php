<?php


namespace Ipol\Fivepost\Fivepost\Controller;

use Ipol\Fivepost\Api\BadResponseException;
use Ipol\Fivepost\Api\Entity\Request\Part\Warehouse\WarehouseElemList;
use \Ipol\Fivepost\Api\Entity\Request\Warehouse as WarehouseRequest;
use Ipol\Fivepost\Api\Entity\Response\ErrorResponse;
use Ipol\Fivepost\Fivepost\AppLevelException;
use Ipol\Fivepost\Fivepost\Entity\WarehouseResult;
use Ipol\Fivepost\Fivepost\ErrorResponseException;


/**
 * Class Warehouse
 * @package Ipol\Fivepost\Fivepost\Controller
 */
class Warehouse extends RequestController
{
    /**
     * Warehouse constructor.
     * @param WarehouseElemList $warehouses
     */
    public function __construct(WarehouseElemList $warehouses)
    {
        $data = new WarehouseRequest();
        $data->setWarehouses($warehouses);

        $this->setRequestObj($data);
    }

    /**
     * @return WarehouseResult
     */
    public function create(): WarehouseResult
    {
        $result = new WarehouseResult();

        try{
            $requestProcess = $this->getSDK()
                ->warehouse($this->getRequestObj());

            $result->setSuccess($requestProcess->getResponse()->isRequestSuccess())
                ->setWarehouse($requestProcess->getResponse());

            if(!$result->isSuccess() && is_a($requestProcess->getResponse(), ErrorResponse::class))
            {
                $result->setError(new ErrorResponseException($requestProcess->getResponse()));
            }
        }catch(BadResponseException | AppLevelException $e){
            $result->setSuccess(false)
                ->setError($e);
        } finally {
            return $result;
        }
    }

}