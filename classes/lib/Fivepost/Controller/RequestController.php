<?php


namespace Ipol\Fivepost\Fivepost\Controller;

use Ipol\Fivepost\Api\Entity\Response\AbstractResponse;
use Ipol\Fivepost\Api\Sdk;
use Ipol\Fivepost\Fivepost\AppLevelException;


/**
 * Class RequestController
 * @package Ipol\Fivepost\Fivepost\Controller
 */
abstract class RequestController
{
    /**
     * @var Sdk
     */
    protected $SDK = false;
    /**
     * @var false|string
     */
    protected $lastError = false;
    /**
     * @var mixed|AbstractResponse
     */
    protected $requestObj;

    /**
     * @return mixed
     */
    public function getRequestObj()
    {
        return $this->requestObj;
    }

    /**
     * @param mixed $requestObj
     * @return RequestController
     */
    public function setRequestObj($requestObj): RequestController
    {
        $this->requestObj = $requestObj;
        return $this;
    }

    /**
     * @return Sdk
     * @throws AppLevelException
     */
    public function getSDK()
    {
        if(!$this->SDK)
            throw new AppLevelException('Accessing SDK before setting and configuring it');
        return $this->SDK;
    }

    /**
     * @param Sdk $SDK
     * @return $this
     */
    public function setSDK(Sdk $SDK): RequestController
    {
        $this->SDK = $SDK;

        return $this;
    }

    /**
     * @return false|string
     */
    public function getLastError()
    {
        return $this->lastError;
    }

}