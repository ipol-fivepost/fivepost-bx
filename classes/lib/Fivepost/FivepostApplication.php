<?php


namespace Ipol\Fivepost\Fivepost;

use Error;
use Exception;
use Ipol\Fivepost\Api\Adapter\CurlAdapter;
use Ipol\Fivepost\Api\Entity\EncoderInterface;
use Ipol\Fivepost\Api\Entity\Request\Part\Warehouse\WarehouseElemList;
use Ipol\Fivepost\Api\Entity\LoggerInterface;
use Ipol\Fivepost\Api\Sdk;
use Ipol\Fivepost\Bitrix\Entity\Options;
use Ipol\Fivepost\Core\Entity\Collection;
use Ipol\Fivepost\Core\Entity\CacheInterface;
use Ipol\Fivepost\Core\Order\OrderCollection;
use Ipol\Fivepost\Fivepost\Controller\CancelOrderByNumber;
use Ipol\Fivepost\Fivepost\Controller\CancelOrderByUuid;
use Ipol\Fivepost\Fivepost\Controller\Order;
use Ipol\Fivepost\Fivepost\Controller\OrderHistoryController;
use Ipol\Fivepost\Fivepost\Controller\OrderStatusController;
use Ipol\Fivepost\Fivepost\Controller\PickupPoints;
use Ipol\Fivepost\Fivepost\Controller\RequestController;
use Ipol\Fivepost\Fivepost\Controller\RequestJwt;
use Ipol\Fivepost\Fivepost\Controller\Warehouse;
use Ipol\Fivepost\Fivepost\Handler\Tools;


/**
 * Class FivepostApplication
 * @package Ipol\Fivepost\Fivepost
 */
class FivepostApplication
{
    /**
     * @var string
     */
    protected $apiKey;
    /**
     * @var string Auth bearer token
     */
    protected $jwt = "";
    /**
     * @var bool - shows if api mode is test or productive
     */
    protected $testMode = false;
    /**
     * @var bool - true if using custom URL for requests is allowed
     */
    protected $customAllowed = false;
    /**
     * @var false|EncoderInterface
     */
    protected $encoder = false;
    /**
     * @var false|LoggerInterface
     */
    protected $logger = false;
    /**
     * @var Options
     */
    protected $option;
    /**
     * @var integer|false
     */
    protected $timeout;
    /**
     * @var false|CacheInterface
     */
    protected $cache = false;
    /**
     * @var array
     * saves results of calculation via hash
     */
    protected $abyss;
    /**
     * @var bool
     * set - data won't get into the abyss
     */
    protected $blockAbyss = true;
    /**
     * @var string
     * shows how was made last request: via cache, taken from abyss or by actual request to server
     */
    protected $lastRequestType = false;
    /**
     * @var string
     */
    protected $hash;
    /**
     * @var mixed|false false if no errors occurred, error-info otherwise
     * @deprecated
     */
    protected $lastError = false;
    /**
     * @var Collection|false false if no errors occurred, error-info otherwise
     */
    protected $errorCollection = false;
    /**
     * @var bool
     * Indicates if the method was already called (for recurrent calls for dead jwt)
     */
    protected $recursionFlag = false;

    /**
     * FivepostApplication constructor.
     * @param string $apiKey
     * @param false $isTest
     * @param false $timeout
     * @param false $encoder
     * @param false $cache
     * @param false $logger
     */
    public function __construct(string $apiKey, $isTest=false, $timeout=false, $encoder=false, $cache=false, $logger=false)
    {
        $this->setApiKey($apiKey)
            ->setTestMode($isTest)
            ->setTimeout($timeout)
            ->setEncoder($encoder)
            ->setCache($cache)
            ->setLogger($logger);

        $this->abyss = array();
        try {
            $this->getJwt();
        } catch (AppLevelException $e) {
            $this->addError($e);
            //$this->addError(new AppLevelException("No token, and failed to request new one."));
        }
    }

    /**
     * @param bool $force
     * @return false|string
     * @throws AppLevelException
     */
    public function getJwt(bool $force = false)
    {
        if($this->jwt)
            return $this->jwt;

        if(!$force && $this->getCache() && $this->getCache()->checkCache(md5($this->getApiKey().'jwt')))
        {
            $this->setJwt($this->getCache()->getCache(md5($this->getApiKey().'jwt')));
        }
        else
        {
            $newJwt = $this->requestJwt($this->getApiKey());
            if(!$newJwt->isSuccess())
            {
                if($newJwt->getError())
                    $this->addError($newJwt->getError());
                throw new AppLevelException("Fail to get jwt-token!");
            }
            else
            {
                $this->setJwt($newJwt->getJwt());
                if ($this->getCache())
                {
                    $this->getCache()->setLife(3600);
                    $this->getCache()->setCache(md5($this->getApiKey() . 'jwt'), $this->jwt);
                }
            }
        }
        return $this->jwt;
    }

    /**
     * @param string $jwt
     * @return FivepostApplication
     */
    public function setJwt(string $jwt): FivepostApplication
    {
        $this->jwt = $jwt;
        return $this;
    }

    /**
     * @param string $apiKey
     * @return Entity\RequestJwtResult
     */
    public function requestJwt(string $apiKey): Entity\RequestJwtResult
    {
        $this->lastRequestType = 'direct';
        $controller = new RequestJwt($apiKey);

        try {
            $mode = $this->testMode? 'TEST': 'API';
            $adapter = new CurlAdapter($this->getTimeout());
            if($this->getLogger())
                $adapter->setLog($this->getLogger());

            $sdk = new Sdk($adapter, false, $this->getEncoder(), $mode, $this->customAllowed);

            return $controller->setSDK($sdk)->execute();
        } catch (Exception $e) {
            $this->addError($e);
            $result = new Entity\RequestJwtResult();
            $result->setSuccess(false);
            return $result;
        }
    }

    /**
     * @param int $pageSize - amount points in one request. 1000 is recommended by documentation
     * @return Collection of \Ipol\Fivepost\Api\Entity\Response\Part\PickupPoint\Content
     */
    public function getAllPickupPoints(int $pageSize = 1000): Collection
    {
        $this->setHash(md5('allPickupPoints'));

        if($this->checkAbyss())
        {
            $this->lastRequestType = 'abyss';
            $return = $this->abyss[$this->getHash()];
        }
        else
        {
            if ($this->getCache() && $this->getCache()->checkCache($this->getHash()))
            {
                $this->lastRequestType = 'cache';
                $return = $this->getCache()->getCache($this->getHash());
            }
            else
            {
                $this->lastRequestType = 'direct';

                $pageNum = 0;
                $lastPage = 1;
                $return = new Collection('points');

                try {
                    while ($pageNum++ < $lastPage) {
                        $res = $this->getPickupPoints($pageNum, $pageSize);
                        $lastPage = $res->getPickupPoints()->getTotalPages();
                        while ($point = $res->getPickupPoints()->getContent()->getNext())
                            $return->add($point);
                    }

                    $this->toCache($return)
                        ->toAbyss($return);
                }catch(Exception $e){//TODO redo
                    $this->addError(new AppLevelException('allPoints request failed on page '.$pageNum.' because '.$e->getMessage()));
                }
            }
        }
        return $return;
    }

    /**
     * @param int $pageNum
     * @param int $pageSize
     * @return bool | Entity\PickupPointsResult
     */
    public function getPickupPoints(int $pageNum, int $pageSize)
    {
        $this->lastRequestType = 'direct';
        $controller = new PickupPoints($pageNum, $pageSize);

        try{
            $this->configureController($controller);
        } catch (Exception $e){
            $this->addError($e);
            return false;
        }
        $result = $controller->get();
        
        if($result->isSuccess())
            return $result;
        
        else if($result->getError() && $result->getError()->getCode() === 401 && !$this->recursionFlag)
        {
            $this->setJwt("");
            $this->recursionFlag = true; //blocking further recursive calls
            try {
                $this->getJwt(true); //forcing Jwt-request
            } catch (AppLevelException $e) {
                $this->addError($e);
                return false;
            }
            return $this->getPickupPoints($pageNum, $pageSize);
        }
        else
        {
            $this->addError($result->getError());
            return false;
        }
    }

    /**
     * @param WarehouseElemList $warehouse
     * @return bool|Entity\WarehouseResult
     */
    public function createWarehouse(WarehouseElemList $warehouse)
    {
        $this->lastRequestType = 'direct';
        $controller = new Warehouse($warehouse);

        try {
            $this->configureController($controller);
        } catch (Exception $e) {
            $this->addError($e);
            return false;
        }
        $result = $controller->create();

        if($result->isSuccess())
            return $result;
        else if($result->getError() && $result->getError()->getCode() === 401 && !$this->recursionFlag)
        {
            $this->setJwt("");
            $this->recursionFlag = true; //blocking further recursive calls
            try {
                $this->getJwt(true); //forcing Jwt-request
            } catch (AppLevelException $e) {
                $this->addError($e);
                return false;
            }
            return $this->createWarehouse($warehouse);
        }
        else
        {
            $this->addError($result->getError());
            return false;
        }
    }

    /**
     * @param OrderCollection $orders
     * @return bool|Entity\OrderResult
     */
    public function sendOrders(OrderCollection $orders)
    {
        $this->lastRequestType = 'direct';
        $controller = new Order();

        try {
            $this->configureController($controller);
        } catch (Exception $e) {
            $this->addError($e);
            return false;
        }
        $result =  $controller->setOrders($orders)
            ->convert()
            ->send();
        if($result->isSuccess())
            return $result;
        else if($result->getError() && $result->getError()->getCode() === 401 && !$this->recursionFlag)
        {
            $this->setJwt("");
            $this->recursionFlag = true; //blocking further recursive calls
            try {
                $this->getJwt(true); //forcing Jwt-request
            } catch (Exception $e) {
                $this->addError($e);
                return false;
            }
            return $this->sendOrders($orders);
        }
        else
        {
            $this->addError($result->getError());
            return false;
        }
    }

    /**
     * @param string $uuid - order uuid from fivepost API
     * @return bool|Entity\CancelOrderByUuidResult
     */
    public function cancelOrderByUuid(string $uuid)
    {
        $this->lastRequestType = 'direct';
        $controller = new CancelOrderByUuid($uuid);

        try {
            $this->configureController($controller);
        } catch (Exception $e) {
            $this->addError($e);
            return false;
        }
        $result =  $controller->execute();
        if($result->isSuccess())
            return $result;
        else if($result->getError() && $result->getError()->getCode() === 401 && !$this->recursionFlag)
        {
            $this->setJwt("");
            $this->recursionFlag = true; //blocking further recursive calls
            try {
                $this->getJwt(true); //forcing Jwt-request
            } catch (Exception $e) {
                $this->addError($e);
                return false;
            }
            return $this->cancelOrderByUuid($uuid);
        }
        else
        {
            $this->addError($result->getError());
            return false;
        }
    }

    /**
     * @param string $number - order number in CMS
     * @return bool|Entity\CancelOrderByNumberResult
     */
    public function cancelOrderByNumber(string $number)
    {
        $this->lastRequestType = 'direct';
        $controller = new CancelOrderByNumber($number);

        try {
            $this->configureController($controller);
        } catch (Exception $e) {
            $this->addError($e);
            return false;
        }

        $result =  $controller->execute();

        if($result->isSuccess())
            return $result;
        else if($result->getError() && $result->getError()->getCode() === 401 && !$this->recursionFlag)
        {
            $this->setJwt("");
            $this->recursionFlag = true; //blocking further recursive calls
            try {
                $this->getJwt(true); //forcing Jwt-request
            } catch (Exception $e) {
                $this->addError($e);
                return false;
            }
            return $this->cancelOrderByNumber($number);
        }
        else
        {
            $this->addError($result->getError());
            return false;
        }
    }

    /**
     * @param array $arNumbers - array with numbers of orders
     * @param string $type - "uuid" if arNumbers has fivepost api uuids of orders
     *                          or "senderOrderId" if it has CMS order numbers
     * @return bool|Entity\OrderStatusResult
     */
    public function getOrderStatus(array $arNumbers, string $type = "uuid")
    {
        if($type != "uuid" && $type != "senderOrderId")
            throw new Error("Wrong order id type (".$type.") for method ".__METHOD__."!");
        $this->lastRequestType = 'direct';
        $controller = new OrderStatusController($arNumbers, $type);
        try {
            $this->configureController($controller);
        } catch (Exception $e) {
            $this->addError($e);
            return false;
        }
        $result = $controller->execute();
        if($result->isSuccess())
            return $result;
        else if($result->getError() && $result->getError()->getCode() === 401 && !$this->recursionFlag)
        {
            $this->setJwt("");
            $this->recursionFlag = true; //blocking further recursive calls
            try {
                $this->getJwt(true); //forcing Jwt-request
            } catch (Exception $e) {
                $this->addError($e);
                return false;
            }
            return $this->getOrderStatus($arNumbers, $type);
        }
        else
        {
            $this->addError($result->getError());
            return false;
        }
    }

    /**
     * @param string $id - number of order
     * @param string $type - "uuid" if id is fivepost api uuid of order
     *                          or "senderOrderId" if it is CMS order number
     * @return false|Entity\OrderHistoryResult
     */
    public function getOrderHistory(string $id, string $type = "uuid")
    {
        if($type != "uuid" && $type != "senderOrderId")
            throw new Error("Wrong order id type (".$type.") for method ".__METHOD__."!");
        $this->lastRequestType = 'direct';
        $controller = new OrderHistoryController($id, $type);
        try {
            $this->configureController($controller);
        } catch (Exception $e) {
            $this->addError($e);
            return false;
        }
        $result = $controller->execute();
        if($result->isSuccess())
            return $result;
        else if($result->getError() && $result->getError()->getCode() === 401 && !$this->recursionFlag)
        {
            $this->setJwt("");
            $this->recursionFlag = true; //blocking further recursive calls
            try {
                $this->getJwt(true); //forcing Jwt-request
            } catch (Exception $e) {
                $this->addError($e);
                return false;
            }
            return $this->getOrderHistory($id, $type);
        }
        else
        {
            $this->addError($result->getError());
            return false;
        }
    }

    /**
     * @return false|Entity\GenerateBarcodeResult|string
     */
    public function generateBarcode()
    {
        try {
            return Tools::barcodeGenerate($this->getOption());
        } catch (Exception $e) {
            $this->addError($e);
            return false;
        }
    }

    /**
     * @return string
     */
    public function getApiKey(): string
    {
        return $this->apiKey;
    }

    /**
     * @param string $apiKey
     * @return FivepostApplication
     */
    public function setApiKey(string $apiKey): FivepostApplication
    {
        $this->apiKey = $apiKey;
        return $this;
    }

    /**
     * @return bool
     */
    public function isTestMode(): bool
    {
        return $this->testMode;
    }

    /**
     * @param bool $testMode
     * @return FivepostApplication
     */
    public function setTestMode(bool $testMode): FivepostApplication
    {
        $this->testMode = $testMode;
        return $this;
    }

    /**
     * @return $this
     */
    public function allowCustom(): FivepostApplication
    {
        $this->customAllowed = true;
        return $this;
    }

    /**
     * @return $this
     */
    public function disallowCustom(): FivepostApplication
    {
        $this->customAllowed = false;
        return $this;
    }

    /**
     * @return bool|EncoderInterface
     */
    public function getEncoder()
    {
        return $this->encoder;
    }

    /**
     * @param false|EncoderInterface $encoder
     * @return FivepostApplication
     */
    public function setEncoder($encoder): FivepostApplication
    {
        $this->encoder = $encoder;
        return $this;
    }

    /**
     * @return false|LoggerInterface
     */
    public function getLogger()
    {
        return $this->logger;
    }

    /**
     * @param false|LoggerInterface $logger
     * @return FivepostApplication
     */
    public function setLogger($logger): FivepostApplication
    {
        $this->logger = $logger;
        return $this;
    }

    /**
     * @return Options
     */
    public function getOption()
    {
        return $this->option;
    }

    /**
     * @param Options $option
     * @return FivepostApplication
     */
    public function setOption(Options $option): FivepostApplication
    {
        $this->option = $option;
        return $this;
    }

    /**
     * @return int|false
     */
    public function getTimeout()
    {
        return $this->timeout;
    }

    /**
     * @param int|false $timeout
     * @return FivepostApplication
     */
    public function setTimeout($timeout): FivepostApplication
    {
        $this->timeout = $timeout;
        return $this;
    }

    /**
     * @return bool|CacheInterface
     */
    public function getCache()
    {
        return $this->cache;
    }

    /**
     * @param bool|CacheInterface $cache
     * @return FivepostApplication
     */
    public function setCache($cache): FivepostApplication
    {
        $this->cache = $cache;
        return $this;
    }

    /**
     * @param mixed $data
     * @param string $hash
     * @param int $ttl
     * @return FivepostApplication
     */
    public function toCache($data, int $ttl = 3600, string $hash = ""): FivepostApplication
    {
        if(!$hash)
            $hash = $this->getHash();
        if(!$hash || $data === null || !$this->getCache())
            return $this;

        $this->getCache()->setLife($ttl);
        $this->getCache()->setCache($hash, $data);
        return $this;
    }

    /**
     * @return array
     */
    public function getAbyss()
    {
        return $this->abyss;
    }

    /**
     * @param array $abyss
     * @return FivepostApplication
     */
    public function setAbyss($abyss): FivepostApplication
    {
        $this->abyss = $abyss;
        return $this;
    }

    /**
     * @return bool
     */
    public function isAbyssLocked(): bool
    {
        return $this->blockAbyss;
    }

    /**
     * @param bool $blockAbyss
     * @return FivepostApplication
     */
    public function setAbyssLock(bool $blockAbyss): FivepostApplication
    {
        $this->blockAbyss = $blockAbyss;
        return $this;
    }

    /**
     * @param bool|string $hash
     * @return bool|mixed
     * checks whether same request was already done
     */
    public function checkAbyss($hash = false)
    {
        $hash = ($hash) ? $hash : $this->getHash();
        if(!$this->isAbyssLocked() &&
            $hash &&
            array_key_exists($hash, $this->abyss)
        ){
            return $this->abyss[$hash];
        }
        return false;
    }

    /**
     * @param $val
     * @param bool $hash
     * @return $this
     * returns saved request
     */
    public function toAbyss($val, $hash = false): FivepostApplication
    {
        $hash = ($hash)? $hash : $this->getHash();
        if(!$this->isAbyssLocked() && $hash)
            $this->abyss[$hash] = $val;

        return $this;
    }

    /**
     * @return string
     */
    public function getHash(): string
    {
        return $this->hash;
    }

    /**
     * @param mixed $hash
     * @return FivepostApplication
     */
    public function setHash($hash): FivepostApplication
    {
        $this->hash = $hash;
        return $this;
    }

    /**
     * @return false|Collection
     */
    public function getErrorCollection()
    {
        return $this->errorCollection;
    }

    /**
     * @param mixed $error - throwable (Exceptions)
     * @return $this
     */
    protected function addError($error): FivepostApplication
    {
        if(!$this->errorCollection)
            $this->errorCollection = new Collection('errors');

        $this->errorCollection->add($error);
        return $this;
    }

    /**
     * @return string
     * @deprecated - use getErrorCollection()->getLast() instead
     */
    public function getLastError()
    {
        if($this->getErrorCollection() && $lastError = $this->getErrorCollection()->getLast())
        {
            /**@var Exception $lastError*/
            return $lastError->getMessage();
        }
        else
            return "";
    }

    /**
     * @return string
     */
    public function getLastRequestType()
    {
        return $this->lastRequestType;
    }

    /**
     * @param RequestController $controller
     * sets sdk
     * @throws Exception
     */
    protected function configureController($controller)
    {
        $controller->setSDK($this->getSDK());
    }

    /**
     * @return Sdk
     * get the sdk-controller
     * ! timeout sets only here: later it wouldn't be changed !
     * @throws Exception
     */
    public function getSDK(): Sdk
    {
        $mode = $this->testMode? 'TEST': 'API';
        $adapter = new CurlAdapter($this->getTimeout());
        if($this->getLogger())
            $adapter->setLog($this->getLogger());

        return new Sdk($adapter, $this->getJwt(), $this->getEncoder(), $mode, $this->customAllowed);
    }

}