<?php


namespace Ipol\Fivepost\Fivepost\Entity;

use Ipol\Fivepost\Api\BadResponseException;
use Ipol\Fivepost\Core\Entity\BasicResponse;
use Ipol\Fivepost\Fivepost\AppLevelException;
use Ipol\Fivepost\Fivepost\ErrorResponseException;

/**
 * Class AbstractResult
 * @package Ipol\Fivepost\Fivepost\Entity
 */
class AbstractResult extends BasicResponse
{
    /**
     * @return false|ErrorResponseException|BadResponseException|AppLevelException
     */
    public function getError()
    {
        return parent::getError();
    }
}