<?php

namespace Ipol\Fivepost\Fivepost\Entity;

use Ipol\Fivepost\Api\Entity\Response\GetOrderHistory;

/**
 * Class OrderHistoryResult
 * @package Ipol\Fivepost\Fivepost\Entity
 */
class OrderHistoryResult extends AbstractResult
{
    /**
     * @var GetOrderHistory
     */
    protected $OrderHistory;

    /**
     * @return GetOrderHistory
     */
    public function getOrderHistory()
    {
        return $this->OrderHistory;
    }

    /**
     * @param GetOrderHistory $OrderHistory
     * @return $this
     */
    public function setOrderHistory($OrderHistory)
    {
        $this->OrderHistory = $OrderHistory;

        return $this;
    }
}