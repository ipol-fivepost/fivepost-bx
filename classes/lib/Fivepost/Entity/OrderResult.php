<?php

namespace Ipol\Fivepost\Fivepost\Entity;

use Ipol\Fivepost\Api\Entity\Response\CreateOrder;

class OrderResult extends AbstractResult
{
    protected $Order;

    /**
     * @return CreateOrder
     */
    public function getOrder()
    {
        return $this->Order;
    }

    /**
     * @param mixed $Order
     * @return $this
     */
    public function setOrder($Order)
    {
        $this->Order = $Order;

        return $this;
    }
}