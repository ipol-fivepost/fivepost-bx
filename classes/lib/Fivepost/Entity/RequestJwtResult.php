<?php


namespace Ipol\Fivepost\Fivepost\Entity;

use Ipol\Fivepost\Api\Entity\Response\ErrorResponse;
use Ipol\Fivepost\Api\Entity\Response\JwtGenerate;

/**
 * Class RequestJwtResult
 * @package Ipol\Fivepost\Fivepost\Entity
 */
class RequestJwtResult extends AbstractResult
{
    /**
     * @var string
     */
    protected $jwt;

    /**
     * @return JwtGenerate|ErrorResponse
     */
    public function getResponse()
    {
        return parent::getResponse();
    }

    /**
     * @return string
     */
    public function getJwt(): string
    {
        return $this->jwt;
    }

    /**
     * @param string $jwt
     * @return RequestJwtResult
     */
    public function setJwt(string $jwt): RequestJwtResult
    {
        $this->jwt = $jwt;
        return $this;
    }

    /**
     * @return $this
     */
    public function parseFields(): RequestJwtResult
    {
        $this->setJwt($this->getResponse()->getJwt());
        return $this;
    }
}