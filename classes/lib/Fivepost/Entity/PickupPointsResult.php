<?php


namespace Ipol\Fivepost\Fivepost\Entity;


use Ipol\Fivepost\Api\Entity\Response\PickupPoints;

class PickupPointsResult extends AbstractResult
{
    protected $PickupPoints;

    /**
     * @return PickupPoints
     */
    public function getPickupPoints()
    {
        return $this->PickupPoints;
    }

    /**
     * @param mixed $PickupPoints
     * @return $this
     */
    public function setPickupPoints($PickupPoints)
    {
        $this->PickupPoints = $PickupPoints;

        return $this;
    }
}