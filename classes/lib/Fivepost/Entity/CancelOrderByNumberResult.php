<?php


namespace Ipol\Fivepost\Fivepost\Entity;

use Ipol\Fivepost\Api\Entity\Response\CancelOrderByNumber;

class CancelOrderByNumberResult extends AbstractResult
{
    protected $CancelOrderByUuid;

    /**
     * @return CancelOrderByNumber
     */
    public function getCancelOrderByNumber()
    {
        return $this->CancelOrderByUuid;
    }

    /**
     * @param CancelOrderByNumber $response
     * @return $this
     */
    public function setCancelOrderByNumber($response)
    {
        $this->CancelOrderByUuid = $response;

        return $this;
    }
}