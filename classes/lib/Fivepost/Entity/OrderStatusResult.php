<?php

namespace Ipol\Fivepost\Fivepost\Entity;


use Ipol\Fivepost\Api\Entity\Response\GetOrderStatus;

class OrderStatusResult extends AbstractResult
{
    protected $OrderStatus;

    /**
     * @return GetOrderStatus
     */
    public function getOrderStatus()
    {
        return $this->OrderStatus;
    }

    /**
     * @param GetOrderStatus $OrderStatus
     * @return $this
     */
    public function setOrderStatus($OrderStatus)
    {
        $this->OrderStatus = $OrderStatus;

        return $this;
    }
}