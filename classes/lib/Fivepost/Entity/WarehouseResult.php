<?php


namespace Ipol\Fivepost\Fivepost\Entity;

use Ipol\Fivepost\Api\Entity\Response\Warehouse;

class WarehouseResult extends AbstractResult
{
    protected $Warehouse;

    /**
     * @return Warehouse
     */
    public function getWarehouse()
    {
        return $this->Warehouse;
    }

    /**
     * @param mixed $warehouse
     * @return $this
     */
    public function setWarehouse($warehouse)
    {
        $this->Warehouse = $warehouse;

        return $this;
    }
}