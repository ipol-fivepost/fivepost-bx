<?php


namespace Ipol\Fivepost\Fivepost\Entity;

use Ipol\Fivepost\Api\Entity\Response\CancelOrderById;

class CancelOrderByUuidResult extends AbstractResult
{
    protected $CancelOrderByUuid;

    /**
     * @return CancelOrderById
     */
    public function getCancelOrderByUuid()
    {
        return $this->CancelOrderByUuid;
    }

    /**
     * @param CancelOrderById $response
     * @return $this
     */
    public function setCancelOrderByUuid($response)
    {
        $this->CancelOrderByUuid = $response;

        return $this;
    }
}