<?
namespace Ipol\Fivepost\Core\Order;


abstract class AbstractPerson
{
    protected $firstName;
    protected $secondName;
    protected $patronymic;
    protected $email;
    protected $phone;

    public function __construct()
    {
    }

    public function __call($name, $arguments)
    {
        $parametr = lcfirst(substr($name,3));

        if($parametr && strpos($name,'get') !== false)
        {
            if(property_exists($this,$parametr))
                return $this->$parametr;
            else {
                throw new \Exception('Getting unknown field '.$parametr);
            }
        }
        elseif($parametr && strpos($name,'set') !== false)
        {
            $this->$parametr = $arguments[0];

            return $this;
        }
        else
            throw new \Exception('Call to unknown method '.$name);
    }

    /**
     * Return mix of First/Second/Patronymic names
     * @return string
     */
    public function getName()
    {
        return trim(
                    (($this->getFirstName())  ? $this->getFirstName()." " : "")  .
                    (($this->getSecondName()) ? $this->getSecondName()." " : "") .
                    (($this->getPatronymic()) ? $this->getPatronymic() : "")
                );
    }

    /**
     * @return mixed
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * @param mixed $firstName
     * @return $this
     */
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getSecondName()
    {
        return $this->secondName;
    }

    /**
     * @param mixed $secondName
     * @return $this
     */
    public function setSecondName($secondName)
    {
        $this->secondName = $secondName;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getPatronymic()
    {
        return $this->patronymic;
    }

    /**
     * @param mixed $patronymic
     * @return $this
     */
    public function setPatronymic($patronymic)
    {
        $this->patronymic = $patronymic;

        return $this;
    }

    public function setName($name)
    {
        $name = explode(' ',$name);
        $this->setFirstName($name[0])
             ->setSecondName($name[1]);
        if(count($name) > 2)
        {
            $this->setPatronymic(trim(implode(" ",array_slice($name,2))));
        }

        return $this;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param mixed $email
     * @return $this
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * @param mixed $phone
     * @return $this
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;

        return $this;
    }

}