<?

namespace Ipol\Fivepost\Core\Order;


class Sender extends AbstractPerson
{
    protected $company;

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @return mixed
     */
    public function getCompany()
    {
        return $this->company;
    }

    /**
     * @param mixed $company
     * @return $this
     */
    public function setCompany($company)
    {
        $this->company = $company;

        return $this;
    }
}