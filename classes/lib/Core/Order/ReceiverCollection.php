<?
namespace Ipol\Fivepost\Core\Order;

use Ipol\Fivepost\Core\Entity\Collection;

class ReceiverCollection extends Collection
{
    protected $receivers;

    public function __construct()
    {
        parent::__construct('receivers');
    }

    /**
     * @return bool | Receiver
     */
    public function getFirst()
    {
        return parent::getFirst();
    }
}