<?
namespace Ipol\Fivepost\Core\Order;


/**
 * Class Item
 * @package Ipol\Fivepost\Core\Order
 */
class Item
{
    /**
     * @var string
     */
    protected $name;
    /**
     * @var string
     */
    protected $description;
    /**
     * @var int
     */
    protected $weight;
    /**
     * @var int
     */
    protected $length;
    /**
     * @var int
     */
    protected $width;
    /**
     * @var int
     */
    protected $height;
    /**
     * @var float
     */
    protected $cost;
    /**
     * @var float
     */
    protected $price;
    /**
     * @var int
     */
    protected $quantity;
    /**
     * @var string
     */
    protected $barcode;
    /**
     * @var string
     */
    protected $id;
    /**
     * @var string
     */
    protected $articul;
    /**
     * @var int
     */
    protected $vatRate;
    /**
     * @var float
     */
    protected $vatSum;
    /**
     * @var array
     */
    protected $properties;

    /**
     * @return array
     */
    public function getProperties()
    {
        return (is_array($this->properties)) ? $this->properties : array();
    }

    /**
     * @param array $properties
     * @return $this
     */
    public function setProperties($properties)
    {
        $this->properties = $properties;

        return $this;
    }

    /**
     * Sets property for item
     * @param $propertyCode
     * @param $propertyVal
     * @return $this
     */
    public function setProperty($propertyCode, $propertyVal)
    {
        if(!is_array($this->properties)){
            $this->properties = array();
        }

        $this->properties[$propertyCode] = $propertyVal;

        return $this;
    }

    /**
     * Gets item property, false if not set
     * @param $propertyCode
     * @return bool | mixed
     */
    public function getProperty($propertyCode)
    {
        $arProps = $this->getProperties();
        if(array_key_exists($propertyCode,$arProps)){
            return $arProps[$propertyCode];
        } else {
            return false;
        }
    }

    /**
     * @return int
     */
    public function getVatRate()
    {
        return $this->vatRate;
    }

    /**
     * @param  $vatRate
     * @return $this
     */
    public function setVatRate($vatRate)
    {
        $this->vatRate = $vatRate;

        return $this;
    }

    /**
     * @return float
     */
    public function getVatSum()
    {
        return $this->vatSum;
    }

    /**
     * @param  $vatSum
     * @return $this
     */
    public function setVatSum($vatSum)
    {
        $this->vatSum = $vatSum;

        return $this;
    }

    /**
     * @param $name
     * @param $arguments
     * @return $this
     * @throws \Exception
     */
    public function __call($name, $arguments)
    {
        $parametr = lcfirst(substr($name,3));

        if($parametr && strpos($name,'get') !== false)
        {
            if(property_exists($this,$parametr))
                return $this->$parametr;
            else {
                throw new \Exception('Getting unknown field '.$parametr);
            }
        }
        elseif($parametr && strpos($name,'set') !== false)
        {
            $this->$parametr = $arguments[0];

            return $this;
        }
        else
            throw new \Exception('Call to unknown method '.$name);
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param  $name
     * @return $this
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param  $description
     * @return $this
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * @return int
     */
    public function getWeight()
    {
        return $this->weight;
    }

    /**
     * @param  $weight
     * @return $this
     */
    public function setWeight($weight)
    {
        $this->weight = $weight;

        return $this;
    }

    /**
     * @return int
     */
    public function getLength()
    {
        return $this->length;
    }

    /**
     * @param  $length
     * @return $this
     */
    public function setLength($length)
    {
        $this->length = $length;

        return $this;
    }

    /**
     * @return int
     */
    public function getWidth()
    {
        return $this->width;
    }

    /**
     * @param  $width
     * @return $this
     */
    public function setWidth($width)
    {
        $this->width = $width;

        return $this;
    }

    /**
     * @return int
     */
    public function getHeight()
    {
        return $this->height;
    }

    /**
     * @param  $height
     * @return $this
     */
    public function setHeight($height)
    {
        $this->height = $height;

        return $this;
    }

    /**
     * @return float
     */
    public function getCost()
    {
        return $this->cost;
    }

    /**
     * @param  $cost
     * @return $this
     */
    public function setCost($cost)
    {
        $this->cost = $cost;

        return $this;
    }

    /**
     * @return float
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @param  $price
     * @return $this
     */
    public function setPrice($price)
    {
        $this->price = $price;

        return $this;
    }

    /**
     * @return int
     */
    public function getQuantity()
    {
        return $this->quantity;
    }

    /**
     * @param  $quantity
     * @return $this
     */
    public function setQuantity($quantity)
    {
        $this->quantity = $quantity;

        return $this;
    }

    /**
     * @return string
     */
    public function getBarcode()
    {
        return $this->barcode;
    }

    /**
     * @param  $barcode
     * @return $this
     */
    public function setBarcode($barcode)
    {
        $this->barcode = $barcode;

        return $this;
    }

    /**
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param  $id
     * @return $this
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return string
     */
    public function getArticul()
    {
        return $this->articul;
    }

    /**
     * @param  $articul
     * @return $this
     */
    public function setArticul($articul)
    {
        $this->articul = $articul;

        return $this;
    }

    /**
     * @return array
     */
    public function getFields()
    {
        return get_object_vars($this);
    }


}