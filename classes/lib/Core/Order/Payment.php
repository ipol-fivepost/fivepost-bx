<?

namespace Ipol\Fivepost\Core\Order;


use Ipol\Fivepost\Core\Entity\Money;

/**
 * Class Payment
 * @package Ipol\Fivepost\Core\Order
 */
class Payment
{
    /**
     * @var Money
     * Payment for delivery
     */
    protected $delivery;
    /**
     * @var Money
     * Payment for order goods
     */
    protected $goods;
    /**
     * @var Money
     * Estimated order cost for insurance
     */
    protected $estimated;
    /**
     * @var Money
     * was order already payed?
     */
    protected $payed;

    /**
     * @var int
     * default goods VAT percentage
     */
    protected $ndsDefault;
    /**
     * @var int
     * delivery VAT percentage
     */
    protected $ndsDelivery;

    /**
     * @var string - 'Cash','Card','Bill','other', etc
     * Payment type
     */
    protected $type;

    /**
     * @var bool
     * 1 for cashless, 0 for cash
     */
    protected $isBeznal;

    /**
     * Payment constructor.
     */
    public function __construct()
    {
        return $this;
    }

    /**
     * @return int|mixed
     * Полная проверка стоимости: если безнал - то 0, иначе - стоимость доставки + товары - оплаченное
     */
    public function getPrice()
    {
        if($this->getIsBeznal())
        {
            return 0;
        }
        else
        {
            return $this->getNominalPrice();
        }
    }

    /**
     * @return mixed
     * Стоимость доставки + товары - оплаченное
     */
    public function getNominalPrice()
    {
        $return = $this->getDelivery()? $this->getDelivery()->getAmount() : 0;
        $return += $this->getGoods()? $this->getGoods()->getAmount() : 0;
        $return -= $this->getPayed()? $this->getPayed()->getAmount() : 0;
        return $return;
    }

    /**
     * @return Money|false
     * Если установлена оценочная - дает ее, иначе - стоимость товаров
     */
    public function getCost()
    {
        return ($this->getEstimated())? $this->getEstimated() : $this->getGoods();
    }

    /**
     * @return Money|null
     */
    public function getEstimated()
    {
        return $this->estimated;
    }

    /**
     * @param Money $estimated
     * @return $this
     */
    public function setEstimated($estimated)
    {
        $this->estimated = $estimated;

        return $this;
    }

    /**
     * @return bool
     */
    public function getIsBeznal()
    {
        return boolval($this->isBeznal);
    }

    /**
     * @param bool $isBeznal
     * @return $this
     */
    public function setIsBeznal($isBeznal)
    {
        $this->isBeznal = $isBeznal;

        return $this;
    }

    /**
     * @return Money|false
     */
    public function getDelivery()
    {
        return (isset($this->delivery)) ? $this->delivery : false;
    }

    /**
     * @param Money $delivery
     * @return $this
     */
    public function setDelivery($delivery)
    {
        $this->delivery = $delivery;

        return $this;
    }

    /**
     * @return Money|false
     */
    public function getGoods()
    {
        return (isset($this->goods)) ? $this->goods : false;
    }

    /**
     * @param Money $goods
     * @return $this
     */
    public function setGoods($goods)
    {
        $this->goods = $goods;

        return $this;
    }

    /**
     * @return Money
     */
    public function getPayed()
    {
        return ($this->payed) ? $this->payed : new Money(0); //TODO bad practice to return money with static currency
    }

    /**
     * @param Money $payed
     * @return $this
     */
    public function setPayed($payed)
    {
        $this->payed = $payed;

        return $this;
    }


    /**
     * @return string
     */
    public function getType()
    {
        return ($this->type) ? $this->type : 'other';
    }

    /**
     * @param string $type
     * @return $this
     */
    public function setType($type)
    {
        $this->type = (in_array($type, ['Cash','Card','Bill'])) ? $type : 'other';

        return $this;
    }

    /**
     * @return int
     */
    public function getNdsDefault()
    {
        return $this->ndsDefault;
    }

    /**
     * @param int $ndsDefault
     * @return $this
     */
    public function setNdsDefault(int $ndsDefault)
    {
        $this->ndsDefault = $ndsDefault;

        return $this;
    }

    /**
     * @return int
     */
    public function getNdsDelivery()
    {
        return $this->ndsDelivery;
    }

    /**
     * @param int $ndsDelivery
     * @return $this
     */
    public function setNdsDelivery($ndsDelivery): Payment
    {
        $this->ndsDelivery = $ndsDelivery;

        return $this;
    }
}