<?

namespace Ipol\Fivepost\Core\Order;

use Ipol\Fivepost\Core\Entity\Collection;

class ItemCollection extends Collection
{
    protected $items;

    public function __construct()
    {
        parent::__construct('items');
    }

    /**
     * @return bool | Item
     */
    public function getFirst()
    {
        return parent::getFirst();
    }

    /**
     * @return bool | Item
     */
    public function getNext()
    {
        return parent::getNext();
    }
}