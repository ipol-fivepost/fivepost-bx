<?php


namespace Ipol\Fivepost\Core\Order;


use Ipol\Fivepost\Core\Entity\Collection;

class OrderCollection extends Collection
{
    protected $orders;

    public function __construct()
    {
        parent::__construct('orders');
    }

    /**
     * @return bool | Order
     */
    public function getNext()
    {
        return parent::getNext();
    }

    /**
     * @return bool | Order
     */
    public function getFirst()
    {
        return parent::getFirst();
    }
}