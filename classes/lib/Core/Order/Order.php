<?

namespace Ipol\Fivepost\Core\Order;


/**
 * Class Order
 * @package Ipol\Fivepost\Core\Order
 */
class Order
{
    /**
     * @var string - order number in bitrix ('ACCOUNT_NUMBER')
     */
    protected $number;
    /**
     * @var string - name for status (supposedly inner name defined by module and API)
     * example: 'NEW'
     */
    protected $status;
    /**
     * @var string - order id in API
     */
    protected $link;
    /**
     * @var Sender
     */
    protected $sender;
    /**
     * @var ReceiverCollection
     */
    protected $receivers;
    /**
     * @var Address
     */
    protected $addressFrom;
    /**
     * @var Address
     */
    protected $addressTo;
    /**
     * @var Payment
     */
    protected $payment;
    /**
     * @var Goods
     */
    protected $goods;
    /**
     * @var ItemCollection
     */
    protected $items;
    /**
     * @var array
     */
    protected $fields;

    function __construct()
    {
        $this->receivers = new ReceiverCollection();
        $this->fields    = array();
        return $this;
    }

    /**
     * @return ItemCollection
     */
    public function getItems()
    {
        return $this->items;
    }

    /**
     * @param mixed $items
     * @return $this
     */
    public function setItems($items)
    {
        $this->items = $items;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getLink()
    {
        return $this->link;
    }

    /**
     * @param mixed $link
     * @return $this
     */
    public function setLink($link)
    {
        $this->link = $link;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param mixed $status
     * @return $this
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getNumber()
    {
        return $this->number;
    }

    /**
     * @param mixed $number
     * @return $this
     */
    public function setNumber($number)
    {
        $this->number = $number;

        return $this;
    }

    public function getField($code)
    {
        return (array_key_exists($code,$this->fields)) ? $this->fields[$code] : false;
    }

    public function setFields($arFields)
    {
        if(!is_array($arFields)){
            throw new \InvalidArgumentException('arFields must be an array');
        }

        foreach($arFields as $field => $val){
            $this->setField($field,$val);
        }
    }

    public function setField($code,$val)
    {
        $this->fields[$code] = $val;

        return $this;
    }

    /**
     * @return Payment
     */
    public function getPayment()
    {
        return $this->payment;
    }

    /**
     * @param mixed $payment
     * @return $this
     */
    public function setPayment(Payment $payment)
    {
        $this->payment = $payment;

        return $this;
    }

    /**
     * @return \Ipol\Fivepost\Core\Order\Sender
     */
    public function getSender()
    {
        return $this->sender;
    }

    /**
     * @param mixed $sender
     * @return $this
     */
    public function setSender(Sender $sender)
    {
        $this->sender = $sender;

        return $this;
    }

    public function addReciever(Receiver $reciever)
    {
        $this->getReceivers()->add($reciever);

        return $this;
    }

    /**
     * @return ReceiverCollection
     */
    public function getReceivers()
    {
        return $this->receivers;
    }

    /**
     * @param mixed $receivers
     * @return $this
     */
    public function setReceivers(ReceiverCollection $receivers)
    {
        $this->receivers = $receivers;

        return $this;
    }

    /**
     * @return Address
     */
    public function getAddressFrom()
    {
        return $this->addressFrom;
    }

    /**
     * @param mixed $addressFrom
     * @return $this
     */
    public function setAddressFrom(Address $addressFrom)
    {
        $this->addressFrom = $addressFrom;

        return $this;
    }

    /**
     * @return Address
     */
    public function getAddressTo()
    {
        return $this->addressTo;
    }

    /**
     * @param mixed $addressTo
     * @return $this
     */
    public function setAddressTo(Address $addressTo)
    {
        $this->addressTo = $addressTo;

        return $this;
    }

    /**
     * @return Goods
     */
    public function getGoods()
    {
        return $this->goods;
    }

    /**
     * @param Goods $goods
     * @return $this
     */
    public function setGoods($goods)
    {
        $this->goods = $goods;

        return $this;
    }
}