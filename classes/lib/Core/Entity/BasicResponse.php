<?php

namespace Ipol\Fivepost\Core\Entity;

/**
 * Class BasicResponse
 * @package Ipol\Fivepost\Core\Entity
 * Вспомогательный объект для обработки простых ответов сервера, таких как печать этикеток, итп.
 * Чтобы везде была база. База.
 */
class BasicResponse extends BasicEntity
{
    /**
     * @var bool
     * Успех/провал запроса
     */
    protected $success;
    /**
     * @var bool
     * По сути - напрямую хранит ответ API
     */
    protected $response;
    /**
     * @var mixed
     * Содержит текст ошибки
     */
    protected $error;
    /**
     * @var
     * Содержит код ошибки
     */
    protected $code;

    public function __construct()
    {
        $this->success  = false;
        $this->error    = false;
        $this->response = false;
        $this->fields   = array();
    }

    /**
     * @return bool
     */
    public function isSuccess()
    {
        return $this->success;
    }

    /**
     * @param bool $success
     * @return $this
     */
    public function setSuccess($success)
    {
        $this->success = $success;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getResponse()
    {
        return $this->response;
    }

    /**
     * @param mixed $response
     * @return $this
     */
    public function setResponse($response)
    {
        $this->response = $response;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getError()
    {
        return $this->error;
    }

    public function isError()
    {
        return (!$this->isSuccess() && $this->getError());
    }

    /**
     * @param mixed $error
     * @return $this
     */
    public function setError($error)
    {
        $this->error = $error;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * @param mixed $code
     * @return $this
     */
    public function setCode($code)
    {
        $this->code = $code;

        return $this;
    }
}