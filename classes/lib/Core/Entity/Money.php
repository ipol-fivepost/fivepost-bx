<?php

namespace Ipol\Fivepost\Core\Entity;

/**
 * Class Money
 * @package Ipol\Fivepost\Core\Entity
 */
class Money
{
    /**
     * @var int inner container for precise storing money amount. Uses minimal unit (cent, kopeyka etc)
     */
    protected $amount;
    /**
     * @var string - ISO-4217 currency code
     */
    protected $currency;
    /**
     * @var int number of decimal places for this currency (2 for RUB, for instance: 145.50 RUB - 50 - 2 decimal places)
     */
    protected static $decimal; //basically its 2, but for rare currencies with other rules you can extend this class and change it

    /**
     * Money constructor.
     * @param float $amount
     * @param string $currency
     */
    public function __construct(float $amount, string $currency = 'RUB')
    {
        self::$decimal = 2;
        $this->amount = $amount * pow(10, self::$decimal);
        $this->currency = $currency;
    }

    /**
     * @param float $amount
     * @return Money
     */
    public function setAmount(float $amount): Money
    {
        $this->amount = $amount * pow(10, self::$decimal);
        return $this;
    }

    /**
     * @return float
     */
    public function getAmount(): float
    {
        return $this->amount / pow(10, self::$decimal);
    }

    /**
     * @return string
     */
    public function getCurrency(): string
    {
        return $this->currency;
    }

}