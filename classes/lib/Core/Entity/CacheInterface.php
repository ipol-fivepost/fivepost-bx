<?php
namespace Ipol\Fivepost\Core\Entity;

interface CacheInterface
{
    /**
     * @param int $life
     * @return mixed
     * Sets duration on cache's existence
     */
    public function setLife(int $life);

    /**
     * @param string $hash
     * @return mixed
     * receives data from cache
     */
    public function getCache(string $hash);

    /**
     * @param string $hash
     * @param mixed $data
     * @return mixed
     * puts data in cache with hash-key
     */
    public function setCache(string $hash, $data);

    /**
     * @param string $hash
     * @return mixed
     * checks existence of cache
     */
    public function checkCache(string $hash);
}