<?php

namespace Ipol\Fivepost\Core\Entity;

/**
 * Class Collection
 * @package Ipol\Fivepost\Core
 * Object for storing uniform data, that replaces arrays
 * Accessors must declare property, which name will be transfer to parent constructor - that will be container
 * of array with content
 */
class Collection
{
    /**
     * @var int
     */
    protected $index;

    /**
     * @var
     */
    protected $error;

    /**
     * @var string - name of container for all collection elements
     */
    protected $field;

    /**
     * Collection constructor.
     * @param string $field
     */
    public function __construct(string $field)
    {
        $this->field = $field;

        $this->$field = array();

        $this->reset();

    }

    /**
     * @return $this
     * resets index of array
     */
    public function reset()
    {
        $this->index = 0;

        return $this;
    }

    /**
     * @return string
     */
    public function getField(): string
    {
        return $this->field;
    }

    /**
     * @return mixed
     */
    public function getError()
    {
        return $this->error;
    }

    /**
     * @param $index
     * @return bool
     * deletes element, if not exists - returns false, otherwise - resets index and returns true
     */
    public function delete($index): bool
    {
        $link = $this->field;

        if(array_key_exists($index, $this->$link))
        {
            array_splice($this->$link, $index, $index);
            sort($this->$link);
            $this->reset();
            return true;
        } else {
            return false;
        }
    }

    /**
     * @return $this
     * clears everything
     */
    public function clear()
    {
        $link = $this->field;

        if(property_exists($this,$link))
        {
            $this->$link = array();
        }

        $this->reset();

        return $this;
    }

    /**
     * @param mixed $error
     * @param bool $clear
     * @return $this
     */
    public function setError($error,$clear=false)
    {
        $this->error = ($this->error && !$clear) ? $this->error.", ".$error : $error;

        return $this;
    }

    /**
     * @param $something - new element that will be added to collection
     * @return $this
     */
    public function add($something)
    {
        $link = $this->field;

        array_push($this->$link, $something);

        return $this;
    }

    /**
     * @return mixed | bool - returns next element of collection
     */
    public function getNext()
    {
        $link = $this->field;

        if(count($this->$link) < ($this->index) +1)
            return false;

        $arValues = $this->$link;

        return $arValues[$this->index++];
    }

    /**
     * @return int
     */
    public function getIndex(): int
    {
        return $this->index - 1;
    }

    /**
     * @return mixed|false - returns first element of collection
     */
    public function getFirst()
    {
        $link = $this->field;

        if(!count($this->$link))
            return false;

        $arValues = $this->$link;

        return $arValues[0];
    }

    public function getLast()
    {
        $link = $this->field;

        if(!$counter = count($this->$link))
            return false;

        $arValues = $this->$link;

        return $arValues[$counter - 1];
    }

    /**
     * @return int
     */
    public function getQuantity(): int
    {
        $link = $this->field;
        return count($this->$link);
    }
}