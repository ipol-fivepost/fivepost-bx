<?php


namespace Ipol\Fivepost\Core\Entity\Packing;


interface DimensionsMerger
{
    public static function getSumDimensions($arGabs);

}