<?

namespace Ipol\Fivepost\Core\Delivery;

/**
 * Class LocationLink
 * @package Ipol\Fivepost\Core\Delivery
 * Связка местоположений cms и API
 */
class LocationLink
{
    protected $cms = false;
    protected $api = false;

    public function ready()
    {
        return ($this->getCms() && $this->getApi());
    }

    /**
     * @return bool|Location
     */
    public function getCms()
    {
        return $this->cms;
    }

    /**
     * @param mixed $cms
     * @return $this
     */
    public function setCms(Location $cms)
    {
        $this->cms = $cms;

        return $this;
    }

    /**
     * @return bool|Location
     */
    public function getApi()
    {
        return $this->api;
    }

    /**
     * @param mixed $api
     * @return $this
     */
    public function setApi(Location $api)
    {
        $this->api = $api;

        return $this;
    }


}