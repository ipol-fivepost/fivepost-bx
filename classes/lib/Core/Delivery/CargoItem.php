<?
namespace Ipol\Fivepost\Core\Delivery;


use Ipol\Fivepost\Core\Entity\Money;

/**
 * Class CargoItem
 * @package Ipol\Fivepost\Core\Delivery
 * Description of basic product (ware, goods) (length, width, height, quantity)
 * l,w,h - mm
 * w - g
 * v - m3
 */
class CargoItem
{
    /**
     * @var int - mm
     */
    protected $length;
    /**
     * @var int - mm
     */
    protected $width;
    /**
     * @var int - mm
     */
    protected $height;
    /**
     * @var float - m^3
     */
    protected $volume;
    /**
     * @var - int gram
     */
    protected $weight;
    /**
     * @var int
     */
    protected $quantity = 1;
    /**
     * @var Money
     * estimated item price
     */
   protected $price;
    /**
     * @var Money
     * how match buyer must pay for this item
     */
   protected $cost;
    /**
     * @var bool
     */
    protected $overSize = false;

    /**
     * @return Money|bool
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @param Money|bool $price
     * @return $this
     */
    public function setPrice($price)
    {
        $this->price = $price;

        return $this;
    }

    /**
     * @return Money|bool
     */
    public function getCost()
    {
        return $this->cost;
    }

    /**
     * @param Money|bool $cost
     * @return $this
     */
    public function setCost($cost)
    {
        $this->cost = $cost;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getQuantity()
    {
        return $this->quantity;
    }

    /**
     * @param mixed $quantity
     * @return $this
     */
    public function setQuantity($quantity)
    {
        $this->quantity = $quantity;

        return $this;
    }

   public function giveVolume()
   {
       return ($this->getVolume()) ? $this->getVolume() : ($this->getHeight() * $this->getWidth() * $this->getLength());
   }

    /**
     * @return mixed
     */
    public function getVolume()
    {
        return $this->volume;
    }

    /**
     * @param mixed $volume
     * @return $this
     */
    public function setVolume($volume)
    {
        $this->volume = $volume;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getHeight()
    {
        return $this->height;
    }

    /**
     * @param mixed $height
     * @return $this
     */
    public function setHeight($height)
    {
        $this->height = $height;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getWeight()
    {
        return $this->weight;
    }

    /**
     * @param mixed $weight
     * @return $this
     */
    public function setWeight($weight)
    {
        $this->weight = $weight;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getLength()
    {
        return $this->length;
    }

    /**
     * @param mixed $length
     * @return $this
     */
    public function setLength($length)
    {
        $this->length = $length;

        return $this;
    }

    /**
     * @return bool
     */
    public function ready(): bool
    {
       if(!$this->getWeight())
           return false;
       if(!$this->getVolume() && !$this->getWeight() && !$this->getLength() && !$this->getHeight()) //TODO: is it right?
           return false;
       return true;
   }

   public function setGabs($length, $width, $height)
   {
       $this->setLength($length);
       $this->setWidth($width);
       $this->setHeight($height);

       return $this;
   }

    /**
     * @return mixed
     */
    public function getWidth()
    {
        return $this->width;
    }

    /**
     * @param mixed $width
     * @return $this
     */
    public function setWidth($width)
    {
        $this->width = $width;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getOverSize()
    {
        return $this->overSize;
    }

    /**
     * @param mixed $overSize
     */
    public function setOverSize($overSize)
    {
        $this->overSize = $overSize;
    }

}