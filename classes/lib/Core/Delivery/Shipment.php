<?
namespace Ipol\Fivepost\Core\Delivery;

use \Ipol\Fivepost\Core\Delivery\Cargo as CoreCargo;

/**
 * Class Shipment
 * @package Ipol\Fivepost\Core\Delivery
 * Элементарная отгрузка: откуда-куда-товары-тариф-результат
 */
class Shipment
{
    /**
     * @var Location
     */
    protected $from;
    /**
     * @var Location
     */
    protected $to;
    /**
     * @var CargoCollection;
     */
    protected $cargoes;
    /**
     * @var
     * По какому "тарифу" считать отгрузку
     */
    protected $tariff;
    protected $error;
    protected $errorText;

    /**
     * @var mixed of something useful
     */
    protected $details = false;

    /**
     * @var TariffCollection
     * Ответ от СД, разбитый по вариантам для данного тарифа
     */
    protected $summary;

    public function __construct()
    {
        $this->cargoes  = new CargoCollection();
        $this->summary = new TariffCollection();

        return $this;
    }

    /**
     * @return mixed
     */
    public function getError()
    {
        return $this->error;
    }

    /**
     * @param mixed $error
     * @return $this
     */
    public function setError($error)
    {
        $this->error = $error;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getErrorText()
    {
        return $this->errorText;
    }

    /**
     * @param mixed $errorText
     * @return $this
     */
    public function setErrorText($errorText)
    {
        $this->errorText = $errorText;

        return $this;
    }

    public function addCargo(CoreCargo $obCargo)
    {
        $this->cargoes->add($obCargo);

        return $this;
    }

    /**
     * @return Location
     */
    public function getFrom()
    {
        return $this->from;
    }

    /**
     * @param Location $from
     * @return $this
     */
    public function setFrom($from)
    {
        $this->from = $from;

        return $this;
    }

    /**
     * @return Location
     */
    public function getTo()
    {
        return $this->to;
    }

    /**
     * @param Location $to
     * @return $this
     */
    public function setTo($to)
    {
        $this->to = $to;

        return $this;
    }

    /**
     * @return CargoCollection
     */
    public function getCargoes()
    {
        return $this->cargoes;
    }

    /**
     * @param CargoCollection $cargoes
     * @return $this
     */
    public function setCargoes($cargoes)
    {
        $this->cargoes = $cargoes;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getTariff()
    {
        return $this->tariff;
    }

    /**
     * @param mixed $tariff
     * @return $this
     */
    public function setTariff($tariff)
    {
        $this->tariff = $tariff;

        return $this;
    }

    /**
     * @return TariffCollection
     */
    public function getSummary()
    {
        return $this->summary;
    }

    /**
     * @param TariffCollection $summary
     * @return $this
     */
    public function setSummary($summary)
    {
        $this->summary = $summary;

        return $this;
    }

    /**
     * @param bool $full
     * @return string
     * Возвращает хэш отгрузки для кэша
     */
    public function getHash($full=false)
    {
        $hash = serialize(array($this->getTariff(),$this->getTo(),$this->getFrom(),$this->getCargoes(),$this->getDetails()));
        return ($full) ? $hash : md5($hash);
    }

    /**
     * @return mixed
     */
    public function getDetails()
    {
        return $this->details;
    }

    /**
     * @param mixed $details
     * @return $this
     */
    public function setDetails($details)
    {
        $this->details = $details;

        return $this;
    }

    /**
     * Resets summary for better calculation
     * @return $this
     */
    public function resetSummary()
    {
        $this->summary = new TariffCollection();

        return $this;
    }

}