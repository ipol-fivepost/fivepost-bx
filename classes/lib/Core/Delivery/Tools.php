<?
namespace Ipol\Fivepost\Core\Delivery;


class Tools
{
    public static function getTerm ($termMin, $termMax, $glue = '-')
    {
        if ($termMin == $termMax)
            return $termMin;
        else
            return $termMin.$glue.$termMax;
    }
}