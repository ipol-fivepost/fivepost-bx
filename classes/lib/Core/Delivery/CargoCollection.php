<?php

namespace Ipol\Fivepost\Core\Delivery;

use Ipol\Fivepost\Core\Entity\Collection;
use Ipol\Fivepost\Core\Entity\Packing\MebiysDimMerger;

/**
 * Class CargoCollection
 * @package Ipol\Fivepost\Core\Delivery
 * Сборка грузов - используется для учета разбиения на места
 */
class CargoCollection extends Collection
{
    protected $Cargoes;
    protected $packer;

    public function __construct($packer = false)
    {
        parent::__construct('Cargoes');
        $this->packer = $packer? : new MebiysDimMerger();
    }

    /**
     * @return Cargo
     */
    public function getFirst()
    {
        return parent::getFirst();
    }

    /**
     * @return Cargo
     */
    public function getNext()
    {
        return parent::getNext();
    }

    /**
     * @return int
     * Get estimated value of the goods in all cargoes
     */
    public function getTotalPrice()
    {
        $this->reset();
        $ttlPrice = 0;
        while ($obCargo = $this->getNext())
        {
            $ttlPrice += $obCargo->getTotalPrice();
        }

        return $ttlPrice;
    }

    /**
     * @return int
     * Get how much money is need to be payed
     */
    public function getTotalCost()
    {
        $this->reset();
        $ttlCost = 0;
        while ($obCargo = $this->getNext())
        {
            $ttlCost += $obCargo->getTotalCost();
        }

        return $ttlCost;
    }

    public function getTotalWeight()
    {
        $this->reset();
        $weight = 0;

        while ($obCargo = $this->getNext())
        {
            $weight += $obCargo->getWeight();
        }

        return $weight;
    }

    public function getTotalVolume()
    {
        $this->reset();
        $volume = 0;

        while ($obCargo = $this->getNext())
        {
            $volume += $obCargo->getVolume();
        }

        return $volume;
    }

    public function getTotalDimensions()
    {
        $arGabs = array();
        $this->reset();

        while($obCargo = $this->getNext())
        {
            $cargoGabarites = $obCargo->getDimensions();
            $arGabs[] = array($cargoGabarites['L'], $cargoGabarites['W'], $cargoGabarites['H'], 1);
        }

        return $this->packer::getSumDimensions($arGabs);
    }
}