<?php

namespace Ipol\Fivepost\Core\Delivery;

use Ipol\Fivepost\Core\Entity\Collection;

/**
 * Class TariffCollection
 * @package Ipol\Fivepost\Core\Delivery
 * Совокупность тарифов, если отгрузка может быть доставлена разными способами (и API позволяет считать их сразу - что важно)
 */
class TariffCollection extends Collection
{
    protected $Tariffs;

    public function __construct()
    {
        parent::__construct('Tariffs');
    }

    /**
     * @return Tariff
     */
    public function getNext()
    {
        return parent::getNext();
    }

    /**
     * @return Tariff
     */
    public function getFirst()
    {
        return parent::getFirst();
    }
}