<?
namespace Ipol\Fivepost\Core\Delivery;

use Ipol\Fivepost\Core\Entity\Collection;
use Ipol\Fivepost\Core\Entity\Packing\MebiysDimMerger;

/**
 * Class Cargo
 * @package Ipol\Fivepost\Core\Delivery
 * Описание груза, состоящего из элементарных товаров
 */
class Cargo extends Collection
{
    protected $Items;
    protected $packer;

    public function __construct($packer = false)
    {
        parent::__construct('Items');
        $this->packer = $packer? new $packer : new MebiysDimMerger();
    }

    /**
     * @return CargoItem
     */
    public function getNext()
    {
        return parent::getNext();
    }

    /**
     * @return CargoItem
     */
    public function getFirst()
    {
        return parent::getFirst();
    }

    /**
     * @param CargoItem $item
     * @return $this
     * @throws \Exception
     */
    public function add($item)
    {
        if($item->ready()) {
            parent::add($item);
        }else
            throw new \Exception('CargoItem is not ready in '.get_class());

        return $this;
    }

    /**
     * @return array (L,W,H)
     */
    public function getDimensions()
    {
        $arGabs = array();

        $this->reset();
        while($obItem = $this->getNext())
        {
            $arGabs []= array($obItem->getLength(),$obItem->getWidth(),$obItem->getHeight(),$obItem->getQuantity());
        }

        return $this->packer::getSumDimensions($arGabs);
    }

    public function getVolume()
    {
        $volume = 0;

        $this->reset();
        while($obItem = $this->getNext())
        {
            $volume += $obItem->giveVolume() * $obItem->getQuantity();
        }

        return $volume;
    }

    public function getWeight()
    {
        $weight = 0;

        $this->reset();
        while($obItem = $this->getNext())
        {
            $weight += $obItem->getWeight() * $obItem->getQuantity();
        }

        return $weight;
    }

    public function getGabs()
    {
        return array('W'=>$this->getWeight(),'V'=>$this->getVolume(),'G'=>$this->getDimensions());
    }

    /**
     * @return int (но не факт)
     * Получает оценочную стоимость товара
     */
    public function getTotalPrice()
    {
        $price = 0;

        $this->reset();
        while($obItem = $this->getNext())
        {
            $price += $obItem->getPrice() * $obItem->getQuantity();
        }

        return $price;
    }

    /**
     * @return int (но не факт)
     * Получает стоимость к оплате за товар
     */
    public function getTotalCost()
    {
        $cost = 0;

        $this->reset();
        while($obItem = $this->getNext())
        {
            $cost += $obItem->getCost() * $obItem->getQuantity();
        }

        return $cost;
    }

    public function checkOverSize()
    {
        $this->reset();
        while($obItem = $this->getNext())
        {
            if($obItem->getOverSize())
                return true;
        }

        return false;
    }
}