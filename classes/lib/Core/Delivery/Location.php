<?
namespace Ipol\Fivepost\Core\Delivery;


class Location
{
    /**
     * @var
     * Какое-то id местоположения в системе
     */
    protected $id;
    /**
     * @var
     * Дополнительный id местоположения в системе
     */
    protected $code;
    /**
     * @var
     * Страна местоположения
     */
    protected $country;
    /**
     * @var
     * Регион местоположения
     */
    protected $region;
    /**
     * @var
     * Ссылка на родителя местоположения в системе
     */
    protected $parent;
    /**
     * @var
     * Название местоположения
     */
    protected $name;
    /**
     * @var
     * Индекс местоположения
     */
    protected $zip;

    /**
     * @var
     * 'cms' - from cms, 'api' - from delivery
     */
    protected $link;

    public function __construct($link)
    {
        if($link != 'cms' && $link != 'api')
        {
            throw new \InvalidArgumentException('Illegal Link for location ("api" or "cms" is acceptable)');
        }
        $this->link = $link;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getZip()
    {
        return $this->zip;
    }

    /**
     * @param mixed $zip
     * @return $this
     */
    public function setZip($zip)
    {
        $this->zip = $zip;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     * @return $this
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     * @return $this
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * @param mixed $code
     * @return $this
     */
    public function setCode($code)
    {
        $this->code = $code;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * @param mixed $country
     * @return $this
     */
    public function setCountry($country)
    {
        $this->country = $country;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getRegion()
    {
        return $this->region;
    }

    /**
     * @param mixed $region
     * @return $this
     */
    public function setRegion($region)
    {
        $this->region = $region;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getParent()
    {
        return $this->parent;
    }

    /**
     * @param mixed $parent
     * @return $this
     */
    public function setParent($parent)
    {
        $this->parent = $parent;

        return $this;
    }
}