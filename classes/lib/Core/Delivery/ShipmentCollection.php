<?
namespace Ipol\Fivepost\Core\Delivery;

use Ipol\Fivepost\Core\Entity\Collection;

/**
 * Class ShipmentCollection
 * @package Ipol\Fivepost\Core\Delivery
 * Совокупность отгрузок для доставки. Суть: заказ может быть разбит по частям (для суммарного подсчета доставки используется merge).
 * Если заказ цельный и доставляется одной отгрузкой - значит, будет коллекция из одной отгрузки. Увы.
 */
class ShipmentCollection extends Collection
{
    /**
     * @var
     * array of \Ipol\Fivepost\Core\Delivery\Shipment
     */
    protected $shipments;

    /**
     * @var
     * Приоритетность тарифов: считается либо указанный конкретно, либо (если массив) - до первого удачного совпадения.
     * Используется в merge
     */
    protected $tariffPriority;

    /**
     * @var
     * Если у тарифа несколько вариантов - будет приниматься тот, что указан здесь.
     */
    protected $variantPriority = false;

    public function __construct()
    {
        parent::__construct('shipments');
    }

    /**
     * @return mixed
     */
    public function getTariffPriority()
    {
        return $this->tariffPriority;
    }

    /**
     * @param mixed $tariffPriority
     * @return $this
     */
    public function setTariffPriority($tariffPriority)
    {
        $this->tariffPriority = $tariffPriority;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getVariantPriority()
    {
        return $this->variantPriority;
    }

    /**
     * @param mixed $variantPriority
     * @return $this
     */
    public function setVariantPriority($variantPriority)
    {
        $this->variantPriority = $variantPriority;

        return $this;
    }

    /**
     * @param Shipment $shipment
     * @return $this
     */
    public function addShipment($shipment)
    {
        $this->add($shipment);

        return $this;
    }

    /**
     * @return Shipment
     */
    public function getFirst()
    {
        return parent::getFirst();
    }

    /**
     * @return Shipment
     */
    public function getNext()
    {
        return parent::getNext();
    }

    /**
     * @return array of tariffs in shipments
     * Используется когда мы точно не знаем имеющиеся тарифы
     */
    public function getShipmentsTariffs()
    {
        $arTariffs = array();

        $this->reset();
        while($shipment = $this->getNext())
        {
            if($shipment->getError()){
                continue;
            } else {
                $shipment->getSummary()->reset();
                while ($obTariff = $shipment->getSummary()->getNext())
                {
                    if(
                        (!$this->getVariantPriority() || $obTariff->getVariant() == $this->getVariantPriority()) &&
                        !in_array($obTariff->getId(),$arTariffs)
                    )
                    {
                        $arTariffs [] = $obTariff->getId();
                    }
                }
            }
        }

        return $arTariffs;
    }

    /**
     * @return array('price','termMin','termMax')|bool
     * @throws \Exception
     */
    public function merge()
    {
        $this->reset();
        $merger = new shipmentMerger();

        while ($shipment = $this->getNext())
        {
            if($shipment->getError()) {
                throw new \Exception($shipment->getErrorText());
            } else {
                $essence = false;
                if (isset($this->tariffPriority))
                {
                    if(is_array($this->tariffPriority))
                    {
                        foreach($this->tariffPriority as $tarifId)
                        {
                            $essence = $this->magic_parceTarif($shipment,$tarifId,$this->getVariantPriority(), true);
                            if($essence)
                                break;
                        }
                    }
                    else
                        $essence = $this->magic_parceTarif($shipment,$this->tariffPriority,$this->getVariantPriority(),true);
                }
                else
                    $essence = $this->magic_parceTarif($shipment,false,$this->getVariantPriority());
            }
            if($essence){
                $termMax = false;
                $termMin = $essence['term'];
                if(strpos($essence['term'],'-') != false)
                {
                    $essence['term'] = explode('-',$essence['term']);
                    $termMin = trim($essence['term'][0]);
                    $termMax = trim($essence['term'][1]);
                }

                $merger->addShipment($essence['price'],$termMin,$termMax,$essence['detail']);
            }
            else
                throw new \Exception(($this->getError()) ? ((is_array($this->getError()) ? implode(',',$this->getError()) : $this->getError())) : 'No tarifs in shipment found');
        }

        return $merger->getMergedArray();
    }

    /**
     * @param Shipment $shipment
     * @param bool $defId
     * @param bool $variant
     * @param bool $breakOnError
     * @return array|bool
     * Пробегает по итоговому расчету отгрузки (Тарифам), оставляет либо тариф по дефолту (defId - тот, который хотим получить),
     * возможно - с учетом варианта (variant), либо первый попавшийся
     */
    protected function magic_parceTarif($shipment, $defId = false, $variant = false, $breakOnError = false)
    {
        $essence = false;

        $shipment->getSummary()->reset();
        while($obTariff = $shipment->getSummary()->getNext())
        {
            if(
                !$defId ||
                $obTariff->getId() == $defId
            )
            {
                if ($obTariff->getError())
                {
                    $this->setError($obTariff->getErrorText());
                    if($breakOnError)
                        break;
                    else
                        continue;
                }
                else {
                    if(!$variant || $obTariff->getVariant() == $variant) {
                        $essence = array(
                            'price' => $obTariff->getPrice()->getAmount(),
                            'term' => $obTariff->getTerm(),
                            'detail' => $obTariff->getDetails()
                        );
                        break;
                    }
                }
            }
        }

        return $essence;
    }

    /**
     * @return string
     * Возвращает хэш для кэша всей отгрузки (без учета саммари)
     */
    public function getHash()
    {
        $hash = '';
        $this->reset();
        while ($obShipment = $this->getNext()){
            $hash .= $obShipment->getHash(true);
        }
        return $hash;
    }


}