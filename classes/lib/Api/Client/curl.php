<?
    namespace Ipol\Fivepost\Api\Client;

    class curl implements ClientInterface{
        private $client;
        private $answer;
        private $code;
        private $url;

        /**
         * curl constructor.
         * @param bool $url
         * @param bool $config
         * @throws \Exception
         */
        public function __construct($url=false, $config=false)
        {
            if(!function_exists('curl_init'))
                throw new \Exception('No CURL library');
            $this->client = curl_init();
            if($url)
                $this->setUrl($url);
            if($config)
                $this->config($config);
        }

        public function post($data = false)
        {
            curl_setopt($this->client,CURLOPT_POST, TRUE);
            if($data) {
                curl_setopt($this->client, CURLOPT_POSTFIELDS, $data);
            }
            $this->request(true);

            return $this;
        }

        public function get($data = false)
        {
            if($data){
                if(strpos($this->url,'?') !== false)
                    $this->url = substr($this->url,0,strpos($this->url,'?'));
                $this->url .= '?' . http_build_query($data);
            }

            $this->request();

            return $this;
        }

        public function delete($data = false)
        {
            curl_setopt($this->client, CURLOPT_CUSTOMREQUEST, "DELETE");

            $this->request(true);

            return $this;
        }

        public function config($args)
        {
            curl_setopt_array($this->client,$args);

            return $this;
        }

        public function setOpt($opt,$val)
        {
            curl_setopt($this->client,$opt,$val);

            return $this;
        }

        public function getCode()
        {
            return $this->code;
        }

        public function getAnswer()
        {
            return $this->answer;
        }

        public function getRequest()
        {
            return array('code' => $this->getCode(),'answer' => $this->getAnswer());
        }

        public function setUrl($url)
        {
            $this->url = $url;
            return $this;
        }

        public function getUrl()
        {
            return $this->url;
        }

        public function flee()
        {
            if($this->client)
                curl_close($this->client);
            return $this;
        }

        private function request($close=true)
        {
            curl_setopt($this->client,CURLOPT_URL, $this->url);
            $this->answer = curl_exec($this->client);
            $this->code   = curl_getinfo($this->client,CURLINFO_HTTP_CODE);

            if($close)
                $this->flee();
            return $this;
        }
    }
?>