<?
    namespace Ipol\Fivepost\Api;

    use Error;
    use Ipol\Fivepost\Api\Adapter\AdapterInterface;
    use Ipol\Fivepost\Api\Adapter\CurlAdapter;
    use Ipol\Fivepost\Api\Methods\CancelOrderById;
    use Ipol\Fivepost\Api\Methods\CancelOrderByNumber;
    use Ipol\Fivepost\Api\Methods\CreateOrder;
    use Ipol\Fivepost\Api\Methods\GetOrderHistory;
    use Ipol\Fivepost\Api\Methods\GetOrderStatus;
    use Ipol\Fivepost\Api\Methods\JwtGenerate;
    use Ipol\Fivepost\Api\Methods\PickupPoints;
    use Ipol\Fivepost\Api\Methods\Warehouse;

    class Sdk
    {
        /**
         * @var CurlAdapter
         */
        private $adapter;
        private $encoder;

        /**
         * @var array
         */
        protected $map;

        /**
         * Sdk constructor.
         * @param CurlAdapter $adapter
         * @param bool | string $jwt
         * @param bool | AdapterInterface $encoder
         * @param bool | string $mode
         * @param bool $custom
         */
        public function __construct(CurlAdapter $adapter, $jwt = false, $encoder = false, $mode = 'API', $custom = false)
        {

            $this->adapter = $adapter;

            $this->encoder = $encoder;

            $this->map = self::getMap($mode, $custom);

            if($jwt)
                $this->adapter->appendHeaders(['Authorization: Bearer '.$jwt]);
        }

        /**
         * @param string $mode
         * @param bool $custom
         * @return array
         */
        protected function getMap(string $mode, bool $custom): array
        {
            $api = 'https://api-omni.x5.ru';
            $test = 'https://api-preprod-omni.x5.ru';

            $arMap = [
                'jwtGenerate' => [
                    'API' => $api.'/jwt-generate-claims/rs256/1',
                    'TEST' => $test.'/jwt-generate-claims/rs256/1',
                    'REQUEST_TYPE' => 'FORM'
                ],
                'pickupPoints' => [
                    'API' => $api.'/api/v1/pickuppoints/query',
                    'TEST' => $test.'/api/v1/pickuppoints/query',
                    'REQUEST_TYPE' => 'POST'
                ],
                'warehouse' => [
                    'API' => $api.'/api/v1/warehouse',
                    'TEST' => $test.'/api/v1/warehouse',
                    'REQUEST_TYPE' => 'POST'
                ],
                'createOrder' => [
                    'API' => $api.'/api/v1/createOrder',
                    'TEST' => $test.'/api/v1/createOrder',
                    'REQUEST_TYPE' => 'POST'
                ],
                'cancelOrderById' => [
                    'API' => $api.'/api/v2/cancelOrder/byOrderId/',
                    'TEST' => $test.'/api/v2/cancelOrder/byOrderId/',
                    'REQUEST_TYPE' => 'DELETE'
                ],
                'cancelOrderByNumber' => [
                    'API' => $api.'/api/v2/cancelOrder/bySenderOrderId/',
                    'TEST' => $test.'/api/v2/cancelOrder/bySenderOrderId/',
                    'REQUEST_TYPE' => 'DELETE'
                ],
                'getOrderStatus' => [
                    'API' => $api.'/api/v1/getOrderStatus',
                    'TEST' => $test.'/api/v1/getOrderStatus',
                    'REQUEST_TYPE' => 'POST'
                ],
                'getOrderHistory' => [
                    'API' => $api.'/api/v1/getOrderHistory',
                    'TEST' => $test.'/api/v1/getOrderHistory',
                    'REQUEST_TYPE' => 'POST'
                ],

            ];

            if(defined('IPOL_FIVEPOST_CUSTOM_MAP') && is_array(IPOL_FIVEPOST_CUSTOM_MAP))
                foreach(IPOL_FIVEPOST_CUSTOM_MAP as $method => $url)
                {
                    $arMap[$method]['CUSTOM'] = $url;
                }

            if($mode != 'TEST' && $mode != 'API')
                throw new Error('Unknown Api-map configuring mode');

            $arReturn = array();
            foreach($arMap as $method => $arData)
            {
                if($custom && isset($arMap[$method]['CUSTOM']))
                    $url = $arMap[$method]['CUSTOM'];
                else
                    $url = $arData[$mode];

                $arReturn[$method] = array(
                    'URL' => $url,
                    'REQUEST_TYPE' => $arData['REQUEST_TYPE']
                );
            }
            return $arReturn;
        }

        /**
         * @param string $method name of method in api-map
         * @return bool
         */
        protected function configureRequest(string $method): bool
        {
            if(array_key_exists($method, $this->map))
            {
                $url = $this->map[$method]['URL'];
                $type = $this->map[$method]['REQUEST_TYPE'];
            }
            else
            {
                throw new Error('Requested method "'.$method.'" not found in module map!');
            }

            $this->adapter->setUrl($url);
            $this->adapter->setRequestType($type);
            return true;
        }

        /**
         * @param Entity\Request\JwtGenerate $data
         * @return JwtGenerate
         * @throws BadResponseException
         */
        public function jwtGenerate($data): JwtGenerate
        {
            $this->configureRequest(__FUNCTION__);
            return new JwtGenerate($data, $this->adapter, $this->encoder);
        }

        /**
         * @param Entity\Request\PickupPoints $data
         * @return PickupPoints
         * @throws BadResponseException
         */
        public function pickupPoints($data): PickupPoints
        {
            $this->configureRequest(__FUNCTION__);
            return new PickupPoints($data, $this->adapter, $this->encoder);
        }
        /**
         * @param Entity\Request\Warehouse $data
         * @return Warehouse
         * @throws BadResponseException
         */
        public function warehouse($data): Warehouse
        {
            $this->configureRequest(__FUNCTION__);
            return new Warehouse($data, $this->adapter, $this->encoder);
        }

        /**
         * @param Entity\Request\CreateOrder $data
         * @return CreateOrder
         * @throws BadResponseException
         */
        public function createOrder($data): CreateOrder
        {
            $this->configureRequest(__FUNCTION__);
            return new CreateOrder($data, $this->adapter, $this->encoder);
        }

        /**
         * @param Entity\Request\CancelOrderById $data
         * @return CancelOrderById
         * @throws BadResponseException
         */
        public function cancelOrderById($data): CancelOrderById
        {
            $this->configureRequest(__FUNCTION__);
            return new CancelOrderById($data, $this->adapter, $this->encoder);
        }

        /**
         * @param Entity\Request\CancelOrderByNumber $data
         * @return CancelOrderByNumber
         * @throws BadResponseException
         */
        public function cancelOrderByNumber($data): CancelOrderByNumber
        {
            $this->configureRequest(__FUNCTION__);
            return new CancelOrderByNumber($data, $this->adapter, $this->encoder);
        }

        /**
         * @param Entity\Request\GetOrderStatus $data
         * @return GetOrderStatus
         * @throws BadResponseException
         */
        public function getOrderStatus($data): GetOrderStatus
        {
            $this->configureRequest(__FUNCTION__);
            return new GetOrderStatus($data, $this->adapter, $this->encoder);
        }

        /**
         * @param Entity\Request\GetOrderHistory $data
         * @return GetOrderHistory
         * @throws BadResponseException
         */
        public function getOrderHistory($data): GetOrderHistory
        {
            $this->configureRequest(__FUNCTION__);
            return new GetOrderHistory($data, $this->adapter, $this->encoder);
        }
    }