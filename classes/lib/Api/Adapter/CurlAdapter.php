<?
namespace Ipol\Fivepost\Api\Adapter;

use Exception;
use InvalidArgumentException;
use Ipol\Fivepost\Api\ApiLevelException;
use Ipol\Fivepost\Api\BadResponseException;
use Ipol\Fivepost\Api\Client\curl;


/**
 * Class CurlAdapter
 * @package Ipol\Fivepost\Api\Adapter
 */
class CurlAdapter extends AbstractAdapter implements AdapterInterface
{
    /**
     * @var curl
     */
    protected $curl;
    /**
     * @var array
     */
    private $allowedCodeArr;
    /**
     * @var array
     */
    private $validErrorCodeArr;
    /**
     * @var string
     */
    protected $url = "";
    /**
     * @var string
     */
    protected $requestType;
    /**
     * @var array
     */
    protected $headers = [];
    /**
     * @var string
     */
    protected $contentType = 'Content-Type: application/json; charset=utf-8';

    /**
     * CurlAdapter constructor.
     * @param int $timeout
     * @throws Exception
     */
    public function __construct($timeout = 15)
    {
        parent::__construct();
        $this->allowedCodeArr = ['200', '202'];
        $this->validErrorCodeArr = ['302', '400', '401', '500'];
        $this->curl = new curl(false,
            [CURLOPT_TIMEOUT_MS => $timeout * 1000]
        );

        return $this;
    }

    /**
     * @param array $headers
     *
     * @return CurlAdapter
     */
    public function appendHeaders(array $headers): CurlAdapter
    {
        $this->headers = array_merge($this->headers, $headers);
        return $this;
    }

    /**
     * @return $this
     */
    protected function applyHeaders(): CurlAdapter
    {
        $this->appendHeaders([$this->contentType]);
        $this->curl->config([CURLOPT_HTTPHEADER => $this->headers]);
        return $this;

    }

    /**
     * @param string $method
     * @param array $dataPost
     * @param string $urlImplement
     * @param false | array $dataGet
     * @return mixed
     * @throws BadResponseException
     * @throws ApiLevelException
     */
    public function post(string $method, $dataPost = [], $urlImplement = "", array $dataGet = [])
    {
        $this->logging($method, 'sending post', $method);

        $this->curl->setOpt(CURLOPT_RETURNTRANSFER, TRUE);

        $getStr = (!empty($dataGet))? "?" . http_build_query($dataGet) : "";

        $this->curl->setUrl($this->getUrl() . $urlImplement . $getStr);

        $this->logging([
            'URL' => $this->getUrl() . $urlImplement . $getStr,
            'DATA' => $dataPost,
            'JSON' => json_encode($dataPost, JSON_UNESCAPED_UNICODE)],
            'REQUEST', $method
        );

        $this->applyHeaders()->curl->post(json_encode($dataPost,JSON_UNESCAPED_UNICODE));

        $this->logging([
            'CODE' => $this->curl->getCode(),
            'BODY' => $this->curl->getAnswer()],
            'RESPONSE', $method
        );

        if(!in_array($this->curl->getCode(),  $this->allowedCodeArr))
        {
            if(in_array($this->curl->getCode(),  $this->validErrorCodeArr))
            {
                throw new ApiLevelException('Request error', $this->curl->getCode(), $this->curl->getUrl(), $dataPost, $this->curl->getAnswer());
            }
            else
            {
                throw new BadResponseException('Bad server answer: ' . $this->curl->getAnswer(), $this->curl->getCode());
            }
        }

        return $this->curl->getAnswer();
    }

    /**
     * @param string $method
     * @param bool | array $dataPost
     * @param string $urlImplement
     * @param false | string $dataGet
     * @return mixed
     * @throws BadResponseException
     * @throws ApiLevelException
     */
    public function form(string $method, $dataPost = [], $urlImplement = "", array $dataGet = [])
    {
        $this->logging($method, 'sending form', $method);

        $this->curl->setOpt(CURLOPT_RETURNTRANSFER, TRUE);

        if (!$this->checkField($method)) {
            throw new InvalidArgumentException(get_class() . ": call to no method");
        }

        $getStr = (!empty($dataGet))? "?" . http_build_query($dataGet) : "";

        $this->curl->setUrl($this->getUrl() . $urlImplement . $getStr);

        $this->logging([
            'URL' => $this->getUrl() . $urlImplement . $getStr,
            'DATA' => $dataPost,
            'FORM' => http_build_query($dataPost, JSON_UNESCAPED_UNICODE)],
            'REQUEST', $method
        );

        $this->applyHeaders()->curl->post(http_build_query($dataPost));

        $this->logging([
                'CODE' => $this->curl->getCode(),
                'BODY' => $this->curl->getAnswer()],
            'RESPONSE', $method
        );

        if(!in_array($this->curl->getCode(),  $this->allowedCodeArr))
        {
            if(in_array($this->curl->getCode(),  $this->validErrorCodeArr))
            {
                throw new ApiLevelException('Request error', $this->curl->getCode(), $this->curl->getUrl(), $dataPost, $this->curl->getAnswer());
            }
            else
            {
                throw new BadResponseException('Bad server answer: ' . $this->curl->getAnswer(), $this->curl->getCode());
            }
        }

        return $this->curl->getAnswer();
    }

    /**
     * @param string $method
     * @param string $urlImplement
     * @param false | array $dataGet
     * @return mixed
     * @throws BadResponseException
     * @throws ApiLevelException
     */
    public function get(string $method, $urlImplement = "", array $dataGet = [])
    {
        $this->logging($method, 'sending get', $method);

        $this->curl->setOpt(CURLOPT_RETURNTRANSFER, TRUE);

        $this->curl->setUrl($this->getUrl() . $urlImplement);

        $this->logging([
            'URL' => $this->getUrl() . $urlImplement . http_build_query($dataGet)],
            'REQUEST', $method
        );

        $this->applyHeaders()->curl->get($dataGet);

        $this->logging([
            'CODE' => $this->curl->getCode(),
            'BODY' => $this->curl->getAnswer()],
            'RESPONSE', $method
        );

        if(!in_array($this->curl->getCode(),  $this->allowedCodeArr))
        {
            if(in_array($this->curl->getCode(),  $this->validErrorCodeArr))
            {
                throw new ApiLevelException('Request error', $this->curl->getCode(), $this->curl->getUrl(), 'get request', $this->curl->getAnswer());
            }
            else
            {
                throw new BadResponseException('Bad server answer: ' . $this->curl->getAnswer(), $this->curl->getCode());
            }
        }

        return $this->curl->getAnswer();
    }

    /**
     * @param string $method
     * @param string $urlImplement
     * @return mixed
     * @throws BadResponseException
     * @throws ApiLevelException
     */
    public function delete(string $method, $urlImplement = "")
    {
        $this->logging($method, 'sending delete', $method);

        $this->applyHeaders()->curl->setUrl($this->getUrl() . $urlImplement);
        $this->logging([
            'URL' => $this->getUrl() . $urlImplement,],
            'REQUEST', $method
        );

        $this->curl->delete();

        $this->logging([
            'CODE' => $this->curl->getCode(),
            'BODY' => $this->curl->getAnswer()],
            'RESPONSE', $method
        );

        if(!in_array($this->curl->getCode(),  $this->allowedCodeArr))
        {
            if(in_array($this->curl->getCode(),  $this->validErrorCodeArr))
            {
                throw new ApiLevelException('Request error', $this->curl->getCode(), $this->curl->getUrl(), 'delete request', $this->curl->getAnswer());
            }
            else
            {
                throw new BadResponseException('Bad server answer: ' . $this->curl->getAnswer(), $this->curl->getCode());
            }
        }

        return $this->curl->getAnswer();
    }

    /**
     * @param mixed $wat
     * @param string $label
     * @param bool $method
     */
    protected function logging($wat, string $label, $method = false)
    {
        //print_r([$wat, $label, $method]);
        if($this->log){
            $this->log::toLog(print_r([$label=>$wat], true), 'main_log', $method);
        }
    }

    /**
     * @return string
     */
    public function getUrl(): string
    {
        return $this->url;
    }

    /**
     * @param string $url
     * @return CurlAdapter
     */
    public function setUrl(string $url): CurlAdapter
    {
        $this->url = $url;
        return $this;
    }

    /**
     * @return string
     */
    public function getRequestType(): ?string
    {
        return $this->requestType;
    }

    /**
     * @param string $requestType
     * @return CurlAdapter
     */
    public function setRequestType(string $requestType): CurlAdapter
    {
        $this->requestType = $requestType;
        return $this;
    }

    /**
     * @param string $contentType
     * @return $this
     */
    public function setContentType(string $contentType): CurlAdapter
    {
        $this->contentType = $contentType;
        return $this;
    }

    /**
     * @return curl
     */
    public function getCurl(): curl
    {
        return $this->curl;
    }

}