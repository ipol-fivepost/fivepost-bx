<?
    namespace Ipol\Fivepost\Api\Adapter;

    use Ipol\Fivepost\Api\Entity\LoggerInterface;

    abstract class AbstractAdapter
    {
        /**
         * @var bool|LoggerInterface
         */
        protected $log = false;

        public function __construct()
        {
            return $this;
        }

        /**
         * @return bool|LoggerInterface
         */
        public function getLog()
        {
            return $this->log;
        }

        /**
         * @param LoggerInterface $log
         * @return $this;
         */
        public function setLog($log)
        {
            $this->log = $log;

            return $this;
        }

        protected function checkField($wat){
            return (is_string($wat) && !empty($wat));
        }


    }
?>