<?php


namespace Ipol\Fivepost\Api\Entity\Request\Part\CreateOrder;

use Ipol\Fivepost\Api\Entity\AbstractEntity;

/**
 * Class PartnerOrder
 * @package Ipol\Fivepost\Api\Entity\Request\Part\CreateOrder
 */
class PartnerOrder extends AbstractEntity
{
    /**
     * @var string
     */
    protected $clientEmail;
    /**
     * @var \DateTime ("2019-05-07T14:02:14Z" Z - UTC 0)
     */
    protected $plannedReceiveDate;
    /**
     * @var \DateTime
     */
    protected $senderCreateDate;
    /**
     * @var \DateTime
     */
    protected $shipmentDate;
    /**
     * @var string
     */
    protected $senderOrderId;
    /**
     * @var string
     */
    protected $brandName;
    /**
     * @var string
     */
    protected $clientOrderId;
    /**
     * @var string
     */
    protected $clientName;
    /**
     * @var string  (+79XXXXXXXXX, 79XXXXXXXXX, 89XXXXXXXXX or 9XXXXXXXXX)
     */
    protected $clientPhone;
    /**
     * @var string (uuid)
     */
    protected $receiverLocation;
    /**
     * @var string
     */
    protected $senderLocation;
    /**
     * @var string (RETURN | UTILIZATION)
     */
    protected $undeliverableOption;
    /**
     * @var Cost
     */
    protected $cost;
    /**
     * @var CargoList
     */
    protected $cargoes;

    /**
     * @return string
     */
    public function getClientEmail()
    {
        return $this->clientEmail;
    }

    /**
     * @param string $clientEmail
     * @return PartnerOrder
     */
    public function setClientEmail($clientEmail)
    {
        $this->clientEmail = $clientEmail;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getPlannedReceiveDate()
    {
        return $this->plannedReceiveDate;
    }

    /**
     * @param \DateTime $plannedReceiveDate
     * @return PartnerOrder
     */
    public function setPlannedReceiveDate($plannedReceiveDate)
    {
        $this->plannedReceiveDate = $plannedReceiveDate;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getSenderCreateDate()
    {
        return $this->senderCreateDate;
    }

    /**
     * @param \DateTime $senderCreateDate
     * @return PartnerOrder
     */
    public function setSenderCreateDate($senderCreateDate)
    {
        $this->senderCreateDate = $senderCreateDate;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getShipmentDate()
    {
        return $this->shipmentDate;
    }

    /**
     * @param \DateTime $shipmentDate
     * @return PartnerOrder
     */
    public function setShipmentDate($shipmentDate)
    {
        $this->shipmentDate = $shipmentDate;
        return $this;
    }

    /**
     * @return string
     */
    public function getSenderOrderId()
    {
        return $this->senderOrderId;
    }

    /**
     * @param string $senderOrderId
     * @return PartnerOrder
     */
    public function setSenderOrderId($senderOrderId)
    {
        $this->senderOrderId = $senderOrderId;
        return $this;
    }

    /**
     * @return string
     */
    public function getBrandName()
    {
        return $this->brandName;
    }

    /**
     * @param string $brandName
     * @return PartnerOrder
     */
    public function setBrandName($brandName)
    {
        $this->brandName = $brandName;
        return $this;
    }

    /**
     * @return string
     */
    public function getClientOrderId()
    {
        return $this->clientOrderId;
    }

    /**
     * @param string $clientOrderId
     * @return PartnerOrder
     */
    public function setClientOrderId($clientOrderId)
    {
        $this->clientOrderId = $clientOrderId;
        return $this;
    }

    /**
     * @return string
     */
    public function getClientName()
    {
        return $this->clientName;
    }

    /**
     * @param string $clientName
     * @return PartnerOrder
     */
    public function setClientName($clientName)
    {
        $this->clientName = $clientName;
        return $this;
    }

    /**
     * @return string
     */
    public function getClientPhone()
    {
        return $this->clientPhone;
    }

    /**
     * @param string $clientPhone
     * @return PartnerOrder
     */
    public function setClientPhone($clientPhone)
    {
        $this->clientPhone = $clientPhone;
        return $this;
    }

    /**
     * @return string
     */
    public function getReceiverLocation()
    {
        return $this->receiverLocation;
    }

    /**
     * @param string $receiverLocation
     * @return PartnerOrder
     */
    public function setReceiverLocation($receiverLocation)
    {
        $this->receiverLocation = $receiverLocation;
        return $this;
    }

    /**
     * @return string
     */
    public function getSenderLocation()
    {
        return $this->senderLocation;
    }

    /**
     * @param string $senderLocation
     * @return PartnerOrder
     */
    public function setSenderLocation($senderLocation)
    {
        $this->senderLocation = $senderLocation;
        return $this;
    }

    /**
     * @return string
     */
    public function getUndeliverableOption()
    {
        return $this->undeliverableOption;
    }

    /**
     * @param string $undeliverableOption
     * @return PartnerOrder
     */
    public function setUndeliverableOption($undeliverableOption)
    {
        $this->undeliverableOption = $undeliverableOption;
        return $this;
    }

    /**
     * @return Cost
     */
    public function getCost()
    {
        return $this->cost;
    }

    /**
     * @param Cost $cost
     * @return PartnerOrder
     */
    public function setCost($cost)
    {
        $this->cost = $cost;
        return $this;
    }

    /**
     * @return CargoList
     */
    public function getCargoes()
    {
        return $this->cargoes;
    }

    /**
     * @param CargoList $cargoes
     * @return PartnerOrder
     */
    public function setCargoes($cargoes)
    {
        $this->cargoes = $cargoes;
        return $this;
    }

}