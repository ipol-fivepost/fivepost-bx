<?php


namespace Ipol\Fivepost\Api\Entity\Request\Part\CreateOrder;


use Ipol\Fivepost\Api\Entity\AbstractEntity;

/**
 * Class ProductValue
 * @package Ipol\Fivepost\Api\Entity\Response\Part\CreateOrder
 */
class ProductValue extends AbstractEntity
{
    /**
     * @var string
     */
    protected $barcode;
    /**
     * @var string
     */
    protected $codeGTD;
    /**
     * @var string
     */
    protected $codeTNVED;
    /**
     * @var string (Alpha-3)
     */
    protected $currency;
    /**
     * @var string
     */
    protected $originCountry;
    /**
     * @var string
     */
    protected $vendorCode;
    /**
     * @var string
     */
    protected $name;
    /**
     * @var float
     */
    protected $price;
    /**
     * @var int
     */
    protected $value;
    /**
     * @var float
     */
    protected $vat;

    /**
     * @return string
     */
    public function getBarcode()
    {
        return $this->barcode;
    }

    /**
     * @param string $barcode
     * @return ProductValue
     */
    public function setBarcode($barcode)
    {
        $this->barcode = $barcode;
        return $this;
    }

    /**
     * @return string
     */
    public function getCodeGTD()
    {
        return $this->codeGTD;
    }

    /**
     * @param string $codeGTD
     * @return ProductValue
     */
    public function setCodeGTD($codeGTD)
    {
        $this->codeGTD = $codeGTD;
        return $this;
    }

    /**
     * @return string
     */
    public function getCodeTNVED()
    {
        return $this->codeTNVED;
    }

    /**
     * @param string $codeTNVED
     * @return ProductValue
     */
    public function setCodeTNVED($codeTNVED)
    {
        $this->codeTNVED = $codeTNVED;
        return $this;
    }

    /**
     * @return string
     */
    public function getCurrency()
    {
        return $this->currency;
    }

    /**
     * @param string $currency
     * @return ProductValue
     */
    public function setCurrency($currency)
    {
        $this->currency = $currency;
        return $this;
    }

    /**
     * @return string
     */
    public function getOriginCountry()
    {
        return $this->originCountry;
    }

    /**
     * @param string $originCountry
     * @return ProductValue
     */
    public function setOriginCountry($originCountry)
    {
        $this->originCountry = $originCountry;
        return $this;
    }

    /**
     * @return string
     */
    public function getVendorCode()
    {
        return $this->vendorCode;
    }

    /**
     * @param string $vendorCode
     * @return ProductValue
     */
    public function setVendorCode($vendorCode)
    {
        $this->vendorCode = $vendorCode;
        return $this;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return ProductValue
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return float
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @param float $price
     * @return ProductValue
     */
    public function setPrice($price)
    {
        $this->price = $price;
        return $this;
    }

    /**
     * @return int
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @param int $value
     * @return ProductValue
     */
    public function setValue($value)
    {
        $this->value = $value;
        return $this;
    }

    /**
     * @return float
     */
    public function getVat()
    {
        return $this->vat;
    }

    /**
     * @param float $vat
     * @return ProductValue
     */
    public function setVat($vat)
    {
        $this->vat = $vat;
        return $this;
    }

}