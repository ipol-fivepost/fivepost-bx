<?php


namespace Ipol\Fivepost\Api\Entity\Request\Part\CreateOrder;


use Ipol\Fivepost\Api\Entity\AbstractEntity;

/**
 * Class Cargo
 * @package Ipol\Fivepost\Api\Entity\Request\Part\CreateOrder
 */
class Cargo  extends AbstractEntity
{
    /**
     * @var BarcodeList
     */
    protected $barcodes;
    /**
     * @var ProductValueList
     */
    protected $productValues;
    /**
     * @var int
     */
    protected $vat;
    /**
     * @var string
     */
    protected $senderCargoId;
    /**
     * @var string (Alpha-3)
     */
    protected $currency;
    /**
     * @var float
     */
    protected $price;
    /**
     * @var float
     */
    protected $height;
    /**
     * @var float
     */
    protected $length;
    /**
     * @var float
     */
    protected $width;
    /**
     * @var float
     */
    protected $weight;

    /**
     * @return BarcodeList
     */
    public function getBarcodes()
    {
        return $this->barcodes;
    }

    /**
     * @param BarcodeList $barcodes
     * @return Cargo
     */
    public function setBarcodes($barcodes)
    {
        $this->barcodes = $barcodes;
        return $this;
    }

    /**
     * @return ProductValueList
     */
    public function getProductValues()
    {
        return $this->productValues;
    }

    /**
     * @param ProductValueList $productValues
     * @return Cargo
     */
    public function setProductValues($productValues)
    {
        $this->productValues = $productValues;
        return $this;
    }

    /**
     * @return int
     */
    public function getVat()
    {
        return $this->vat;
    }

    /**
     * @param int $vat
     * @return Cargo
     */
    public function setVat($vat)
    {
        $this->vat = $vat;
        return $this;
    }

    /**
     * @return string
     */
    public function getSenderCargoId()
    {
        return $this->senderCargoId;
    }

    /**
     * @param string $senderCargoId
     * @return Cargo
     */
    public function setSenderCargoId($senderCargoId)
    {
        $this->senderCargoId = $senderCargoId;
        return $this;
    }

    /**
     * @return string
     */
    public function getCurrency()
    {
        return $this->currency;
    }

    /**
     * @param string $currency
     * @return Cargo
     */
    public function setCurrency($currency)
    {
        $this->currency = $currency;
        return $this;
    }

    /**
     * @return float
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @param float $price
     * @return Cargo
     */
    public function setPrice($price)
    {
        $this->price = $price;
        return $this;
    }

    /**
     * @return float
     */
    public function getHeight()
    {
        return $this->height;
    }

    /**
     * @param float $height
     * @return Cargo
     */
    public function setHeight($height)
    {
        $this->height = $height;
        return $this;
    }

    /**
     * @return float
     */
    public function getLength()
    {
        return $this->length;
    }

    /**
     * @param float $length
     * @return Cargo
     */
    public function setLength($length)
    {
        $this->length = $length;
        return $this;
    }

    /**
     * @return float
     */
    public function getWidth()
    {
        return $this->width;
    }

    /**
     * @param float $width
     * @return Cargo
     */
    public function setWidth($width)
    {
        $this->width = $width;
        return $this;
    }

    /**
     * @return float
     */
    public function getWeight()
    {
        return $this->weight;
    }

    /**
     * @param float $weight
     * @return Cargo
     */
    public function setWeight($weight)
    {
        $this->weight = $weight;
        return $this;
    }

}