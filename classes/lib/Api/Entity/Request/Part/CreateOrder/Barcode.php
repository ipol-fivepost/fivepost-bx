<?php


namespace Ipol\Fivepost\Api\Entity\Request\Part\CreateOrder;


use Ipol\Fivepost\Api\Entity\AbstractEntity;

/**
 * Class Barcode
 * @package Ipol\Fivepost\Api\Entity\Response\Part\CreateOrder
 */
class Barcode extends AbstractEntity
{
    /**
     * @var string
     */
    protected $value;

    /**
     * @return string
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @param string $value
     * @return Barcode
     */
    public function setValue($value)
    {
        $this->value = $value;
        return $this;
    }
}