<?php


namespace Ipol\Fivepost\Api\Methods;

use Ipol\Fivepost\Api\Adapter\CurlAdapter;
use Ipol\Fivepost\Api\ApiLevelException;
use Ipol\Fivepost\Api\BadResponseException;
use Ipol\Fivepost\Api\Entity\EncoderInterface;
use Ipol\Fivepost\Api\Entity\Response\ErrorResponse;
use Ipol\Fivepost\Api\Entity\Response\JwtGenerate as ObjResponse;
use Ipol\Fivepost\Api\Entity\Request\JwtGenerate as ObjRequest;


/**
 * Class JwtGenerate
 * @package Ipol\Fivepost\Api\Methods
 */
class JwtGenerate extends AbstractMethod
{
    /**
     * JwtGenerate constructor.
     * @param ObjRequest $data
     * @param CurlAdapter $adapter
     * @param false|EncoderInterface $encoder
     * @throws BadResponseException
     */
    public function __construct(ObjRequest $data, CurlAdapter $adapter, $encoder = false)
    {
        parent::__construct($adapter, $encoder);

        $this->setDataGet($this->encodeFieldToAPI(['apikey' => $data->getApikey()]));
        $this->setDataPost($this->encodeFieldToAPI([
            'subject' => $data->getSubject(),
            'audience' => $data->getAudience(),
        ]));

        try
        {
            $response = new ObjResponse($this->request());
            $response->setRequestSuccess(true);
        } catch (ApiLevelException $e)
        {
            $response = new ErrorResponse($e->getAnswer());
            //Crunch
            $response->setErrorCode($e->getCode());
            $response->setMessage($e->getAnswer());
            //And cringe
            $response->setRequestSuccess(false);
        }

        $this->setResponse($this->reEncodeResponse($response));

        $this->setFields();

        return $this;
    }


    /**
     * @return ObjResponse|ErrorResponse
     */
    public function getResponse()
    {
        return parent::getResponse();
    }
}