<?php

namespace Ipol\Fivepost\Bitrix\Controller;


use Ipol\Fivepost\Admin\Logger;
use Ipol\Fivepost\Bitrix\Entity\BasicResponse;
use Ipol\Fivepost\Core\Order\OrderCollection;

class Order extends AbstractController
{

    /**
     * @var \Ipol\Fivepost\Core\Order\Order
     */
    protected $order;
    protected $result;

    public function __construct($module_id, $module_lbl,$order=false)
    {
        parent::__construct($module_id, $module_lbl);

        $this->logger  = ($this->options->fetchDebug() === 'Y' && $this->options->fetchOption('debug_order') === 'Y') ? new Logger() : false;

        $this->application->setLogger($this->logger);

        if($order){
            $this->order = $order;
        }
    }

    /**
     * @return BasicResponse
     */
    public function send()
    {
        $orderCollection = new OrderCollection();
        $orderCollection->add($this->order);

        $obReturn = new BasicResponse();

        if($this->options::fetchOption('debug_order') === 'Y')
            $this->application->setLogger(new Logger());

        $obResponse = $this->application->sendOrders($orderCollection);
        if($obResponse && $obResponse->isSuccess()){
            $obResponse->setSuccess(true);
        } else {
            $obReturn->setSuccess(false)
                     ->setErrorText($this->application->getLastError());
        }

        return $obReturn;
    }

    public function delete()
    {
        $obReturn = new BasicResponse();

        try {
            ob_start();
            $obResponse = $this->application->cancelOrderByNumber($this->order->getNumber());
            ob_get_clean();
            if($obResponse && $obResponse->isSuccess()){
                $obResponse->setSuccess(true);
            } else {
                $obReturn->setSuccess(false)
                    ->setErrorText($this->application->getLastError());
            }
        }catch(\Exception $e){
            $obReturn->setSuccess(false)
                     ->setErrorText($e->getMessage());
        }

        return $obReturn;
    }

    public function generateBarcode(){
        $obReturn = $this->application->generateBarcode();

        if($obReturn && $obReturn->isSuccess()){
            return $obReturn->getBarcode();
        }

        return false;
    }

    public function getResult()
    {
        return $this->result;
    }

    public function setResult($result)
    {
        $this->result = $result;
    }

    /**
     * @return \Ipol\Fivepost\Core\Order\Order
     */
    public function getOrder()
    {
        return $this->order;
    }

    /**
     * @param mixed $order
     * @return $this
     */
    public function setOrder(\Ipol\Fivepost\Core\Order\Order $order)
    {
        $this->order = $order;

        return $this;
    }
}