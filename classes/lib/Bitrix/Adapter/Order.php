<?
namespace Ipol\Fivepost\Bitrix\Adapter;


use Ipol\Fivepost\Bitrix\Entity\Options;
use Ipol\Fivepost\OrderHandler;
use Ipol\Fivepost\OrderPropsHandler;
use Ipol\Fivepost\OrdersTable;

class Order
{
    protected $bitrixId;
    protected $orderNumber;
    /**
     * @var Options
     */
    protected $options;
    protected $baseOrder;
    /**
     * @var Receiver
     */
    protected $receiver;
    /**
     * @var AddressTo
     */
    protected $addressTo;
    protected $payment;
    protected $goods;
    /**
     * @var orderItems
     */
    protected $items;

    protected $moduleLbl;

    public function __construct(Options $options)
    {
        $this->options     = $options;
        $this->baseOrder   = new \Ipol\Fivepost\Core\Order\Order();
        $this->receiver    = new Receiver($options);
        $this->addressTo   = new AddressTo($options);
        $this->payment     = new Payment($options);
        $this->goods       = new OrderGoods($options);
        $this->items       = new OrderItems($options);

        $this->moduleLbl   = IPOL_FIVEPOST_LBL;

        return $this;
    }

    /**
     * @param $bitrixId
     * @return $this
     * Заполняет заказ данными по заказу в Битрисе и настройкам по умолчанию.
     */
    public function newOrder($bitrixId)
    {
        $this->bitrixId    = $bitrixId;
        $this->orderNumber = \Ipol\Fivepost\Bitrix\Handler\Order::getOrderNumber($bitrixId);

        $this->getBaseOrder()->setStatus('new');

        $this->getReceiver()->fromOrder($bitrixId);
        $this->getAddressTo()->fromOrder($bitrixId);

        $this->getPayment()->fromOrder($bitrixId);
        $this->getGoods()->fromOrder($bitrixId);
        $this->getItems()->fromOrder($bitrixId);

        $this->compileOrder();

        $this->setDefaultFields();


        $order = \Ipol\Fivepost\Bitrix\Handler\Order::getOrderById($this->bitrixId);

        if($order) {
            $arProps = $order->getPropertyCollection()->getArray();
            foreach ($arProps['properties'] as $property) {
                if (
                    $property['CODE'] == $this->moduleLbl . OrderPropsHandler::getPVZprop() &&
                    $value = array_pop($property['VALUE'])
                ) {
                    $this->getBaseOrder()->setField('receiverLocation', $value);
                }
            }
        }

        return $this;
    }

    /**
     * Привязывает базовые сущности заказа (адрес, отправитель, итп) к базовому заказу
     */
    protected function compileOrder()
    {
        $this->getBaseOrder()
            ->addReciever($this->getReceiver()->getCoreReceiver())
            ->setAddressTo($this->getAddressTo()->getCoreAddress())
            ->setPayment($this->getPayment()->getCorePayment())
            ->setNumber($this->getOrderNumber())
            ->setGoods($this->getGoods()->getCoreGoods())
            ->setItems($this->getItems()->getCoreItems());
    }

    /**
     * Устанавливает поля по умолчанию для заказа с учетом настроек модуля и данных в самом заказе.
     * ! только для newOrder
     */
    protected function setDefaultFields()
    {
        $this->getBaseOrder()
            ->setField('brandName',$this->options->fetchBrandName())
            ->setField('undeliverableOption',$this->options->fetchUndeliverableOption())
            ->setField('receiverLocation',false)
            ->setField('senderCreateDate',\Ipol\Fivepost\Bitrix\Handler\Order::getOrderDate($this->bitrixId,true))
            ->setField('barcode',OrderHandler::getOrderBarcode())
        ;
    }

    /**
     * @return $this
     * Устанавливает поля из запроса (по сути - из формы отправления заявки)
     */
    public function requestOrder()
    {
        $this->bitrixId    = $_REQUEST['orderId'];
        $this->orderNumber = $_REQUEST['number'];

        $request = self::fromRequest();

        $this->getBaseOrder()->setNumber($this->orderNumber);

        $this->setArrayFields($request['order']);

        $this->getReceiver()->fromArray($request['receiver']);
        $this->getAddressTo()->fromArray($request['addressTo']);

        $this->getPayment()->fromArray($request['payment']);
        $this->getGoods()->fromArray($request['goods']);
        $this->getItems()->fromArray($request['items']);

        $this->getBaseOrder()->setField('barcodes',array($_REQUEST['barcode']));

        $arDateCreate  = \Ipol\Fivepost\Bitrix\Handler\Order::getOrderDate($this->bitrixId,true);

        $this->getBaseOrder()->setField('createDate',$arDateCreate['timestamp']);
        $this->getBaseOrder()->setField('createDateFP',$this::makeFivepostTimeFromTimestamp($arDateCreate['timestamp']));
        if(array_key_exists('plannedReceiveDate',$_REQUEST) && $_REQUEST['plannedReceiveDate']){
            $this->getBaseOrder()->setField('plannedReceiveDate',$_REQUEST['plannedReceiveDate']);
            $this->getBaseOrder()->setField('plannedReceiveDateFP',$this::makeFivepostTimeFromTimestamp($_REQUEST['plannedReceiveDate']));
            $this->getBaseOrder()->setField('plannedReceiveDateDB',$this::makeDBTime($_REQUEST['plannedReceiveDate']));
        }
        if(array_key_exists('shipmentDate',$_REQUEST) && $_REQUEST['shipmentDate']){
            $this->getBaseOrder()->setField('shipmentDate',$_REQUEST['shipmentDate']);
            $this->getBaseOrder()->setField('shipmentDateFP',$this::makeFivepostTimeFromTimestamp($_REQUEST['shipmentDate']));
            $this->getBaseOrder()->setField('shipmentDateDB',$this::makeDBTime($_REQUEST['shipmentDate']));
        }

        $this->compileOrder();

        return $this;
    }

    /**
     * @return array
     * Связка данных из массива запроса с полями заказа
     */
    protected static function fromRequest()
    {
        return array(
            'receiver'    => array(
                'name'  => $_REQUEST['clientName'],
                'phone' => $_REQUEST['clientPhone'],
                'email' => $_REQUEST['clientEmail']
            ),
            'addressTo'   => array(
                'city'    => $_REQUEST['Recipient_City'],
            ),

            'payment'     => array(
                'goods'      => $_REQUEST['payment_sum'],
                'estimated'  => $_REQUEST['price'],
//                'isBeznal'   => $_REQUEST['payment_isBeznal'],
                'delivery'   => $_REQUEST['deliveryCost'],
//                'payed'      => $_REQUEST['payment_prepayment'],
                'type'       => $_REQUEST['paymentType'],
//                'ndsDelivery' => $_REQUEST['payment_ndsDelivery'],
                'ndsDefault'  => $_REQUEST['payment_ndsDefault'],
            ),

            'goods' => array(
                'length'    => $_REQUEST['length'],
                'width'     => $_REQUEST['width'],
                'height'    => $_REQUEST['height'],
                'weight'    => $_REQUEST['weight']
            ),

            'order' => array(
                'barcode'     => $_REQUEST['barcode'],
                'track'       => $_REQUEST['barcode'],
                'senderCargoId' => $_REQUEST['barcode'],

                'receiverLocation'      => $_REQUEST['receiverLocation'],
                'senderCreateDate'     => $_REQUEST['senderCreateDate'],
                'senderLocation'    => $_REQUEST['senderLocation'],
                'undeliverableOption'    => $_REQUEST['undeliverableOption'],

                'currency'             => $_REQUEST['currency'],
                'deliveryCostCurrency' => $_REQUEST['deliveryCostCurrency'],
                'paymentCurrency'      => $_REQUEST['paymentCurrency'],
                'priceCurrency'        => $_REQUEST['priceCurrency'],

                'brandName'            => $_REQUEST['brandName'],
                'orderId'              => $_REQUEST['orderId']


//                'cargoList'          => ($_REQUEST['cargoList']) ? json_encode($_REQUEST['cargoList']) : false,
//                'lotList'            => ($_REQUEST['lotList'])   ? json_encode($_REQUEST['lotList']) : false,
//                'addList'            => ($_REQUEST['addList'])   ? json_encode($_REQUEST['addList']) : false,
            ),

            'items' => $_REQUEST['items']
        );
    }

    /**
     * @param $array
     * Устанавливаем поля из массива в базовый заказ
     */
    protected function setArrayFields($array)
    {
        foreach($array as $key => $val)
        {
            $this->getBaseOrder()->setField(lcfirst($key),(string)$val);
        }
    }

    /**
     * @param $bitrixId - ID заказа в Битриксе
     * @param $mode - всегда 1 (2 - для отгрузок, не подключено)
     * @return $this
     * Заполняет заказ данными по сведениям, хранящимся в таблице модуля (то есть - по отосланному заказу)
     */
    public function uploadedOrder($bitrixId)
    {
        $this->bitrixId = $bitrixId;

        $arOrder = OrdersTable::getByBitrixId($bitrixId);

        $this->setDefaultFields();

        $arDateCreate  = \Ipol\Fivepost\Bitrix\Handler\Order::getOrderDate($this->bitrixId,true);

        $dbFields = $this->fromDB($arOrder);
        if($arOrder) {
            $this->getBaseOrder()->setStatus($arOrder['STATUS']);
            $this->getBaseOrder()->setLink($arOrder['FIVEPOST_ID']);

            $this->setArrayFields($dbFields['order']);
            $this->getReceiver()->fromArray($dbFields['receiver']);
            $this->getPayment()->fromArray($dbFields['payment']);

            $this->getBaseOrder()
                ->addReciever($this->getReceiver()->getCoreReceiver())
                ->setPayment($this->getPayment()->getCorePayment())
                ->setNumber(($this->getOrderNumber()) ? $this->getOrderNumber() : \Ipol\Fivepost\Bitrix\Handler\Order::getOrderNumber($bitrixId))
                ->setGoods($dbFields['goods'])
                ->setItems($dbFields['items'])
                ->setField('barcode',$arOrder['FIVEPOST_ID'])
                ->setField('createDate',$this::makeFivepostTimeFromTimestamp($arDateCreate['timestamp']))
                ->setField('message',$arOrder['MESSAGE']);

            $this->getAddressTo()->fromOrder($this->bitrixId);
            $this->getBaseOrder()->setAddressTo($this->getAddressTo()->getCoreAddress());

            if(array_key_exists('PLANNED_RECEIVE_DATE',$arOrder) && $arOrder['PLANNED_RECEIVE_DATE']){
                $this->getBaseOrder()->setField('plannedReceiveDate',$arOrder['PLANNED_RECEIVE_DATE']->toString());
            }
            if(array_key_exists('SHIPMENT_DATE',$arOrder) && $arOrder['SHIPMENT_DATE']){
                $this->getBaseOrder()->setField('shipmentDate',$arOrder['SHIPMENT_DATE']->toString());
            }
        }

        return $this;
    }

    protected function fromDB($arDB)
    {
        $arStuff = unserialize($arDB['CARGOES']);
        return array(
            'receiver'    => array(
                'name'  => $arDB['CLIENT_NAME'],
                'phone' => $arDB['CLIENT_PHONE'],
                'email' => $arDB['CLIENT_EMAIL']
            ),

            'payment'     => array(
                'goods'       => $arDB['PAYMENT_VALUE'],
                'estimated'   => $arDB['PRICE'],
                //                'isBeznal'   => $_REQUEST['payment_isBeznal'],
                'delivery'    => $arDB['DELIVERY_COST'],
                //                'payed'      => $_REQUEST['payment_prepayment'],
                'type'        => $arDB['PAYMENT_TYPE'],
                //                'ndsDelivery' => $_REQUEST['payment_ndsDelivery'],
                'ndsDefault'  => $arDB['payment_ndsDefault'],
            ),

            'goods' => $arStuff['goods'],

            'order' => array(
                'barcode'                => $arDB['FIVEPOST_ID'],
                'track'                  => $arDB['FIVEPOST_ID'],
                'senderCargoId'          => $arDB['FIVEPOST_ID'],
                'receiverLocation'       => $arDB['RECEIVER_LOCATION'],
                'senderLocation'         => $arDB['SENDER_LOCATION'],
                'undeliverableOption'    => $arDB['UNDELIVERABLE_OPTION'],

                'currency'             => $arDB['CURRENCY'],
                'deliveryCostCurrency' => $arDB['DELIVERY_COST_CURRENCY'],
                'paymentCurrency'      => $arDB['PAYMENT_CURRENCY'],
                'priceCurrency'        => $arDB['PRICE_CURRENCY'],

                'brandName'            => $arDB['BRAND_NAME'],
                'orderId'              => $arDB['BITRIX_ID'],

                'fivepost_status'      => $arDB['FIVEPOST_STATUS'],
                'fivepost_execution_status' => $arDB['FIVEPOST_EXECUTION_STATUS'],


                //                'cargoList'          => ($_REQUEST['cargoList']) ? json_encode($_REQUEST['cargoList']) : false,
                //                'lotList'            => ($_REQUEST['lotList'])   ? json_encode($_REQUEST['lotList']) : false,
                //                'addList'            => ($_REQUEST['addList'])   ? json_encode($_REQUEST['addList']) : false,
            ),

            'items' => $arStuff['items']
        );
    }

    /**
     * @param $PI
     * @return order
     * @throws \Exception
     * Заполняет заказ данными по таблице модуля (отправленный), выборка - по ID поней.
     */
    public function uploadedOrderBy5I($fivePostId)
    {
        $obOrder = OrdersTable::getByFivepostId($fivePostId);
        if($obOrder)
        {
            return $this->uploadedOrder($obOrder['BITRIX_ID']);
        }else
        {
            throw new \Exception('No order with 5Post_Id '.$fivePostId);
        }
    }


    /**
     * @param $code
     * @return bool
     * Конгениальная проверка чекбоксов
     */
    protected function checkBoolOption($code)
    {
        $method = 'get'.ucfirst($code);
        return ($this->options->$method() === 'Y');
    }

    protected static function makeFivepostTimeFromTimestamp($timeStamp){
        $strDateCreate = new \DateTime( 'now', new \DateTimeZone('UTC'));
        $strDateCreate->setTimestamp($timeStamp);
        return str_replace('+00:00', '.000Z', $strDateCreate->format('c'));
    }

    protected static function makeDBTime($timeStamp){
        $obDate = \Bitrix\Main\Type\DateTime::createFromTimestamp($timeStamp);
        return $obDate;
    }

    /**
     * @return mixed
     */
    public function getBitrixId()
    {
        return $this->bitrixId;
    }

    /**
     * @return orderItems
     */
    public function getItems()
    {
        return $this->items;
    }

    /**
     * @param mixed $items
     * @return $this
     */
    public function setItems($items)
    {
        $this->items = $items;

        return $this;
    }

    /**
     * @return \Ipol\Fivepost\Core\Order\Order
     */
    public function getBaseOrder()
    {
        return $this->baseOrder;
    }

    /**
     * @return Receiver
     */
    public function getReceiver()
    {
        return $this->receiver;
    }

    /**
     * @param Receiver $receiver
     * @return $this
     */
    public function setReceiver(Receiver $receiver)
    {
        $this->receiver = $receiver;

        return $this;
    }

    // Getters/setters
    /**
     * @return addressTo
     */
    public function getAddressTo()
    {
        return $this->addressTo;
    }

    /**
     * @param mixed $addressTo
     * @return $this
     */
    public function setAddressTo(addressTo $addressTo)
    {
        $this->addressTo = $addressTo;

        return $this;
    }

    /**
     * @return payment
     */
    public function getPayment()
    {
        return $this->payment;
    }

    /**
     * @param mixed $payment
     * @return $this
     */
    public function setPayment(payment $payment)
    {
        $this->payment = $payment;

        return $this;
    }

    /**
     * @return OrderGoods
     */
    public function getGoods()
    {
        return $this->goods;
    }

    /**
     * @param mixed $obGoods
     * @return $this
     */
    public function setGoods($obGoods)
    {
        $this->goods = $obGoods;

        return $this;
    }


    /**
     * @return mixed
     */
    public function getOrderNumber()
    {
        return $this->orderNumber;
    }
}