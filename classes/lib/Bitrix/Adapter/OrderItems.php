<?php

namespace Ipol\Fivepost\Bitrix\Adapter;


use Ipol\Fivepost\Bitrix\Entity\DefaultGabarites;
use Ipol\Fivepost\Bitrix\Entity\Options;
use Ipol\Fivepost\Bitrix\Handler\GoodsPicker;
use Ipol\Fivepost\Core\Order\Item;
use Ipol\Fivepost\Core\Order\ItemCollection;

class OrderItems
{
    protected $coreItems;
    protected $options;

    public function __construct(Options $options)
    {
        $this->options   = $options;
        $this->coreItems = new ItemCollection();
        return $this;
    }

    public function fromOrder($bitrixId)
    {
        $arGoods = GoodsPicker::fromOrder($bitrixId);

        $articul = $this->options->fetchArticul();
        $barcode = $this->options->fetchBarcode();

        GoodsPicker::addBasketGoodProperties($arGoods,array($articul,$barcode));
        GoodsPicker::addGoodsQRs($arGoods,$bitrixId);

        $defGabarites = new DefaultGabarites();

        foreach($arGoods as $arGood)
        {
            $arDimensions = array(
                'WEIGHT' => ($defGabarites->getMode() == 'G' && !floatval($arGood['WEIGHT'])) ? $defGabarites->getWeight() : $arGood['WEIGHT'],
                'HEIGHT' => ($defGabarites->getMode() == 'G' && !floatval($arGood['HEIGHT'])) ? $defGabarites->getWeight() : $arGood['HEIGHT'],
                'WIDTH'  => ($defGabarites->getMode() == 'G' && !floatval($arGood['WIDTH']))  ? $defGabarites->getWeight() : $arGood['WIDTH'],
                'LENGTH' => ($defGabarites->getMode() == 'G' && !floatval($arGood['LENGTH'])) ? $defGabarites->getWeight() : $arGood['LENGTH']
            );
            $obItem = new Item();
            $obItem->setName($arGood['NAME'])
                ->setPrice($arGood['PRICE'])
                ->setCost($arGood['PRICE'])
                ->setQuantity($arGood['QUANTITY'])
                ->setId($arGood['PRODUCT_ID'])
                ->setVatRate(intval($arGood['VAT_RATE'] * 100))
                ->setWeight($arDimensions['WEIGHT'])
                ->setHeight($arDimensions['HEIGHT'])
                ->setWidth($arDimensions['WIDTH'])
                ->setLength($arDimensions['LENGTH']);

            if($articul){
                $obItem->setArticul($arGood['PROPERTIES'][$articul]);
            }
            if($barcode){
                $obItem->setBarcode($arGood['PROPERTIES'][$barcode]);
            }
            if($arGood['QR']){
                $obItem->setProperty('QR',$arGood['QR']);
            }

            $this->getCoreItems()->add($obItem);
        }

        return $this;
    }

    public function fromArray($arItems)
    {
        foreach ($arItems as $item) {
            $obItem = new Item();
            $obItem->setName($item['name'])
                ->setPrice($item['price'])
                ->setCost($item['cost'])
                ->setQuantity($item['quantity'])
                ->setId($item['id'])
                ->setWeight($item['weight'])
                ->setHeight($item['height'])
                ->setWidth($item['width'])
                ->setLength($item['length'])
                ->setVatRate($item['vatRate'])
                ->setArticul($item['articul'])
                ->setBarcode($item['barcode']);

            foreach(array('oc','ccd','tnved') as $property){
                $obItem->setProperty($property,$item[$property]);
            }

            $this->getCoreItems()->add($obItem);
        }

        return $this;
    }

    /**
     * @return ItemCollection
     */
    public function getCoreItems()
    {
        return $this->coreItems;
    }

    /**
     * @param mixed $coreItems
     * @return $this
     */
    public function setCoreItems($coreItems)
    {
        $this->coreItems = $coreItems;

        return $this;
    }
}